<!-- TITLE: Witness -->
<!-- SUBTITLE: A not-too-technical description of what a witness does; why we need non-default witnesses, and who's available -->
# What is a witness?
A witness is a highly reputable user with a real-world identity, who stamps each transaction seen. There are 12 witnesses involved in every transaction. In exchange for the work involved, a witness collects part of the transaction fee (the payload fee). This list varies very little from transaction to transaction. There cannot be more than one change in the witnesses list. The witnesses majority (6+1) show the path to the main chain. Some witnesses may even be down for a period of time without affecting the network. The security of the network would only be threatened if 7 witnesses colluded together, which is almost unthinkable. 

Ref [Byteball white paper PDF](https://byteball.org/Byteball.pdf)

# How to replace a witness
Wallet menu > Settings > Witnesses > then   either
* Turn on "Auto-update the witness list from the hub"; or
* Turn it off. Click the witness (just one) you want to change, then paste in the new ID and click Save. Note that if your hub suggests a different witness list, it will notice the difference and prompt you to change it back.

Note that currently, the only one possible to change is JED....
# Non-default witnesses
## Why change?
The platform was set up with 12 witnesses all being the founder, Anton Churyumov (Tony). He is, of course, "a highly reputable user with a real-world identity", and totally trustworthy. Byteball is his baby, after all. But this couldn't get more centralized, unwelcome in a going-to-become decentralized system. So we need to diversify, to start trusting others willing to stand up publicly and put their reputation on the line.
## @portabella, the first independent witness
No real-world identity posted. Known in the Byteball community as a veteran Byteball developer and contributor, who started and maintains this very wiki. Has also operated the first independent Byteball hubs (see [hub](https://byteroll.com/hub) list) since January 2017.

> Based in Stockholm, Sweden, a software developer by day, Byteballer by night. Replace the default witness JED... with **7ULGTPFB72TOYA67YNGMX2Y445FSTL7O**. You can do so in the Global settings menu - Witnesses, or by changing hub to byteroll.com/bb and accepting the witness-list it suggests, which is the above JED... to 7ULGT... change. Operational security is of highest importance, as such it will never run in a cloud or somebody else server, it is currently in a physically secure location with auto-destruct if tampered with. There are no backups for high-availability, as any restart requires the manual entering of passphrase. The passphrase and keys have been generated and not ever copied anywhere else.

Contact info: @portabella at our Slack.

## @seb486, the cashback witness
No real-world identity posted. Known in the Byteball community as seb486 on slack and Bytefan on BitcoinTalk. With a partner operates 4 hubs globally (see [hub](https://byteroll.com/hub) list). Main website is https://byteball.fr.

In his own words:

> We are based in France and operate the first Byteball redistributive witness, also know as "the cashback witness". Each time you post a unit to the byteball network you pay a Payload fee which is proportional to the weight (in bytes) of your unit. The 12 witnesses your unit is authored on receive each 1/12th of the Payload fee. Use the Cashback Witness as one of your witnesses and **50% of the payload income it earned from you is given back to you**. The process is fully automated and 100% anonymous, no registration is needed.
**You earn back bytes for free**. It is as simple as that!

Ref [The Cashback Witness](https://byteball-cashback-witness.com)  
Contact info: #cashback_witness channel at our Slack 

# Witness monitoring service
This is a useful page, created and maintained by @seb486

Ref [Witness monitoring service](https://byteball.fr/stats.html)

# Witness list
| Witness ID                       | Owner           | Slack ID    | Started    |
|----------------------------------|-----------------|-------------|------------|
| BVVJ2K7ENPZZ3VYZFWQWK7ISPCATFIW3 | Anton Churyumov | @tonych     | 2016-12-25 |
| DJMMI5JYA5BWQYSXDPRZJVLW3UGL3GJS | Anton Churyumov | @tonych     | 2016-12-25 |
| FOPUBEUPBC6YLIQDLKL6EW775BMV7YOH | Anton Churyumov | @tonych     | 2016-12-25 |
| GFK3RDAPQLLNCMQEVGGD2KCPZTLSG3HN | Anton Churyumov | @tonych     | 2016-12-25 |
| H5EZTQE7ABFH27AUDTQFMZIALANK6RBG | Anton Churyumov | @tonych     | 2016-12-25 |
| I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT | Anton Churyumov | @tonych     | 2016-12-25 |
| JEDZYC2HMGDBIDQKG3XSTXUSHMCBK725 | Anton Churyumov | @tonych     | 2016-12-25 |
| JPQKPRI5FMTQRJF4ZZMYZYDQVRD55OTC | Anton Churyumov | @tonych     | 2016-12-25 |
| OYW2XTDKSNKGSEZ27LMGNOPJSYIXHBHC | Anton Churyumov | @tonych     | 2016-12-25 |
| S7N5FE42F6ONPNDQLCF64E2MGFYKQR2I | Anton Churyumov | @tonych     | 2016-12-25 |
| TKT4UESIKTTRALRRLWS4SENSTJX6ODCW | Anton Churyumov | @tonych     | 2016-12-25 |
| UENJPVZ7HVHM6QGVGT6MWOJGGRTUTJXQ | Anton Churyumov | @tonych     | 2016-12-25 |
| 7ULGTPFB72TOYA67YNGMX2Y445FSTL7O | anon            | @portabella | 2017-02-14 |
| MEJGDND55XNON7UU3ZKERJIZMMXJTVCV | anon            | @seb486     | 2017-05-25 |

-----

## Additional comments
[Slack 11:00 PM 2017-06-22]  
**markcross:** and it's those witnesses that wallet has choosen that will try to determine the validity of the transaction/message?  

**tonych:** To make sure there is no misunderstanding, actually, No.
Witnesses are not sole validators.  
All full nodes perform validation, witnesses are a small subset of full nodes.  They are no[t] special in regards to validation.  
Witnesses play other role.  Remember we are on a DAG, there is no strict order between units.   All full nodes look at witnesses in the recent history to establish the path of the Main Chain.  The Main Chain is chosen such that it goes through as many witness-authored units as possible.  Total order is then determined relative to the Main Chain, and total order resolves conflicts caused by double-spends -- the earlier version wins.  That's the role of the witnesses - to draw the Main Chain through them.

-----

[Tony on bitcointalk 2017-02-06:](https://bitcointalk.org/index.php?topic=1608859.msg17755806#msg17755806)
> When changing your witness list you remove one old witness and replace it with a new one.  If the removed witness is the same on all nodes (which is more likely in practice, e.g. if negative information about a witness is released), all nodes stay compatible: only one mutation relative to the old list and relative to each other.  The nodes can perform more changes as long as their new lists stay compatible.

-----
 **This "Witness" page main authors: @portabella and @slackjore at the** [Byteball slack](http://slack.byteball.org/)
