<!-- TITLE: Social Tokens -->
<!-- SUBTITLE: In a local community, most people have regular money in varying amounts, and it works fine apart from one generally not having enough of it. But there are other resources, personal to the individual, that can be "monetised", with these tokens then being passed around like cash. -->

# Backing
To be more than a fun-coin, useful for practising textcoins or smart contracts but with zero monetary value, a token must be backed by something. That means the bearer can exchange the token with reasonable ease for something the bearer considers valuable.

## Historical
In the distant past, "I promise to pay the bearer on demand the sum of 1 pound" on a one-pound English note meant on presentation at the bank the bearer could receive in exchange one pound weight of sterling silver. With fiat money -- where it's still valuable -- the backing is only the confidence that other people will accept it in exchange for usual goods and services.

## Present day
How does this apply today, in 2018, and especially with Byteball? It's not generally practical to create a token exchangeable one-for-one with the national fiat currency, although local currencies complementary to the national currency do exist. They are accepted at participating merchants to encourage residents to shop locally.[[1](#references)]

However, ICOs that collect money in exchange for tokens -- especially on the Ethereum platform -- are very popular, despite threats of government regulatory action.

# Counterfeiting
The big thing about non-crypto tokens is many ideas seem great until you get to the practicalities and realise that any physical tokens one will make (hand-made or computer-printed or even hammered rounds of metal) can easily be counterfeited. It's why national banknotes and passports are so incredibly intricate and beyond creation at home. And creating a crypto token on the Ethereum platform, say, is relatively difficult and costly.

But Byteball tokens change all that because:

* the cost and effort of creation is negligible, and  
* despite this ease one's token is instantly recognisable and can't be forged.

This is a **big** deal!

# The Social Tokens system
[video](https://vimeo.com/255579961){.vimeo}

## Time
The default resource, that most anyone can monetise to some extent, is their spare time. So Bobby Smart creates a token called "Minutes by Bobby Smart" that he issues very carefully. Because the agreement is that **anyone** holding his tokens can "cash them in" with Bobby on demand in exchange for that many minutes of his time and attention. Obviously one is not expected to drop whatever they are doing to honour the new demand, but a reasonable accommodation, satisfactory to both parties, should be made.

## Good faith
This is a good faith system, where "I promise to pay the bearer on demand ..." for once means what it says. Both token issuers and token redeemers are expected to act reasonably and if they don't the value of their "money" will drop accordingly along with their social reputation.

## Skills
The time of a professional, whether a plumber or a tax accountant, will have to be specified as social or professional, i.e. whether an hour with the plumber is just chat or includes unblocking your sink.

"Minutes by Gary Watson", a local plumber, would be a confusing token. Does this time include plumbing labour, or only chat time? "Plumbing Minutes by Gary Watson" or "Casual Minutes by Gary Watson" would be clearer. But "Professional Landscape Gardening Minutes by Sotiris Papadopoulos" isn't going to fit in the space available.

## Exchange rates
Just as 1 Zimbabwean Dollar is not worth the same as 1 US Dollar, "Kisses by Sally Snodgrass" might not be as prized -- or as rare -- as "Kisses by Sarah Sweetbrush". The relative values of various similar tokens in a community will become clearer as time goes by.

## Fiat or token?
Some items can readily be exchanged for fiat cash, but others could be exchanged for social tokens that don't feel right to sell for cash. For example, spending an hour fixing a good friend's computer would probably be done for free; doing it for a complete stranger might involve a normal cash payment; but how about the awkward area in the middle where neither seems appropriate?

## Examples
These are things friends with skills might do for each other, not full-blown commercial offerings.

* Lambo rides (from a rich family)  
* DJ sessions  
* Haircuts  
* Microbrews
* Backrubs  
* Horserides (family business)  
* Makeovers (make-up expert)  
* Portraits (from someone who draws well)  
* Guitar lessons  
* Language lessons (bilingual person)  
* Listening minutes (sympathetic person)  
* Babysitting hours

## Textcoins
Deb is perfect to illustrate all this as she both likes Byteball and loves to give away her kisses with wild abandon. If an intended target does not have a Byteball wallet yet, no problem. She sends some “Kisses by Deb” as a [textcoin](https://byteroll.com/textcoin) in a text message on WhatsApp or whatever is in vogue at the time.

[video](https://vimeo.com/254898717){.vimeo}

## Creating tokens
How tokens are **created** is covered in the [Asset](https://byteroll.com/asset) article. To have a token **display** in English, "Tingos by Jore Bohne" instead of "O1DbJWbZJfKhjZQYH5RrdRQ2ojMuo2WiaKbjIWSWd4E=", requires registration[[2](#references)] too.

## See also

## References
1. https://en.wikipedia.org/wiki/Local_currency
2. https://docs.google.com/document/d/e/2PACX-1vQnpiwTipnBgrhSJcELOYYAOa3mTLZbmLmOebbtHFJFrfgHtlsNNZ9MPEGafvtuTnVAyfWukwu_hYSB/pub


-----
**This "Social tokens" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)