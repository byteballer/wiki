<!-- TITLE: Invest -->
<!-- SUBTITLE: Thinking of investing in a cryptocurrency? -->

# Crypto investment questions
Posted in reddit.com/r/dashpay by **forgoodnessshakes** 2017-06-13
and copied with permission.

-----
 
All crypto-currencies are high-risk investments that can produce a high rate of return or large losses. Is this the type of investment you are looking for?

The best thing is to get to know what you are buying by asking the questions (these are mine, add your own):

* Is it useful?  
* Do other tokens fulfill the same need?  
* Is it a natural monopoly (likely to be only one token in this market or many)?  
* Is it scarce/limited supply?  
* Is it actively under development?  
* Do you admire the 'cleverness' of the project?  
* Is the development reliant on volunteers?  
* Does it have a large community?  
* Does it have a network of users?  
* Does it have first-mover advantage?  
* Is it liquid enough to get out in small amounts or in total?  
* Is there a large market?  
* Can it scale?  
* Is demand likely to exceed supply?  
* Can you hold it long enough to iron out any bumps in growth? 
 
Review the competitor's criticisms of it - are they at all valid?

Lastly, does the thought of buying it get your Spidey-senses tingling? Do you 'sense' it is undervalued? Then swoop.

