<!-- TITLE: Error -->
<!-- SUBTITLE: Error messages and (maybe) how to fix them -->

# huh?
Table information collection in progress. Please contact @slackjore with any info.

| Error message | Notes |
|-----------|-----------|
| could not send payment: bad signature at path r | look under the table |
| could not send payment: precommit callback failed | smart wallet containing committed funds that only the counter-party can now spend |
| could not send payment: authentifier verification failed | smart contract conditions not fulfilled |
| could not send payment: known bad | if you try a bad send repeatedly |
| could not create payment proposal: No bytes to pay fees | so send bytes to wallet |
| connect to light vendor failed [socket closed] | firewall? turn off Tor? (light vendor = hub) | 
| ??? | incoming payment not confirmed yet, so wait another 5 minutes or so |
| [internal] connection closed | maybe non-functioning hub: change to another hub |
| unable to verify the first certificate | ??? |
| too many messages, try sending a smaller amount [of blackbytes] | send the total bit by bit in smaller amounts |
| on login “Uncaught exception: Error: message encrypted to unknown key, device" (+others) | dont click ok; right click >  "inspect" > delete the window in the html code ; restart|
| unable to find parents; failed to find compatible parents: no deep units | witness error, such as (user) swapping out two at once |
| **Others** |  **Any action necessary?** |
| Syncing -- 24 private payments left (Android) | try clearing out smart wallets; related to private assets? |
| blah blah | blah blah | 

## comments
**tonych [10:43 AM 2017-06-14 #helpdesk]:** 

> **Bad signature at path r** is a mysterious bug that affected a few users in the past but there is no clarity about its nature. Are you on android, did you send any payments from this wallet before? any other unusual actions with this wallet?