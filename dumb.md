<!-- TITLE: Dumb -->
<!-- SUBTITLE: Notes on dumbing down too-technical bits -->

# Notes for the rest of us
## Backups
|  Original, just split up | Notes  |
|---|---|
| Byteball uses a single extended private key for all wallets.  | look up "hierarchical deterministic" re crypto wallets|
| **BIP44** is used for wallet address derivation.   | Ref [Bitcoin Improvement Proposal 44](https://en.bitcoin.it/wiki/BIP_0044) |
| There is a **BIP39** mnemonic for backing up the wallet key, but it is not enough.  | Ref [Bitcoin Improvement Proposal 39](https://en.bitcoin.it/wiki/BIP_0039)  |
| Private payments and co-signers of multisig wallets are stored only in the app's data directory  | 4 notes |
| so better use the built-in backup function! Go to **hamburger menu** > settings > full backup,   | three parallel horizontal lines (displayed as ☰), suggestive of a list, and named for its resemblance to the layers in hamburgers 
| this will create an encrypted and optionally compressed backup file.  | 6 notes |

**This "Dumb" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)