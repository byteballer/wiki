<!-- TITLE: Byteball Community Fund -->
<!-- SUBTITLE: The Fund's mission is to support the Byteball crypto platform and encourage its wider use -->
# The Byteball Community Fund

## Byteball Community Announcement
The Byteball crypto project has done great so far. Tony, the chief developer, laid the foundation for this with his great invention and continuous hard work. But this project would be nothing without you - the users. Great ideas, testing, translations, marketing, so much was achieved by the community and it's invaluable! To reward those users who play an active role in the community and therefore accelerate the progress of the Byteball crypto project, a fund was created:


## Purpose
The Byteball Community Fund's goal is to support and encourage the Byteball crypto platform. Fundraising is done by voluntary donations from investors, stakeholders and enthusiasts. Donation payments are accepted in bytes, blackbytes and bitcoins. The fund will pay out bounties for byteball-related work such as reporting security vulnerabilities, software bugs, user support, marketing campaigns etc.

## Security
As a security measure, both byteball and bitcoin wallets of the fund use multi-signature transaction confirmation. The trustees of the Byteball Community Fund are: 
* Tony (Anton Churyumoff, Byteball's chief developer)
* CryptKeeper (Byteball's community leader)
* Travin Keith (Managing Director of Agavon)  
* Durerus (Byteball investor and adviser)
  
Any outgoing transaction needs to be confirmed by at least 3 trustees (3-of-4 multi-sig).

## Project funding
Besides bounty payment for smaller achievements, the fund can also decide to support byteball-related projects. For most projects, the project leaders should seek funding by investors or organize a crowdfunding. Because the community fund supply is scarce, funding of such projects should be the last option sought and will only be considered if the project is important for the platform.

## Decision-making procedure
As this fund is a community fund, byteball community members will be actively involved in the decision-making for the most important issues and community feedback will be taken into account by the trustees.
Like the multi-sig setup implies, trustees come to a decision in an informal meeting with a simple majority vote.

## Fund management
The goal of the community fund is not to gain profits by trading with the donated currencies. Quite the contrary, the fund must be managed in the most conservative way to avoid any risks! However it may be necessary to exchange one currency to another to maintain the fund's liquidity and to be able to pay expenses if another currency is required. The decision of whether a share of the fund should be exchanged will be decided by the trustees with a simple majority vote.

To maximize the effectiveness of the donations, the fund's wallet addresses will take part in the distributions of free bytes and blackbytes.

## Accounting
Fund accounting will be as transparent as possible with [detailed reports published regularly](https://docs.google.com/spreadsheets/d/1foYdri-vadnq8Kbr51QVhBlXiswHtiDpaShVaWO060s/edit?usp=sharing), though donors and bounty recipients can choose to stay anonymous.

Current (January 2018) fund balances, rounded off: 1660 GB / 55,000 GBB / 0 BTC

## Donation accounts
If you want to donate, please use one of the following addresses:  
* For Bytes: KREOV5Y57FRWQKDHJ7BD3SFQBEVVPAG3  
![Byteballcommunityfundbytedonations](/uploads/cryptkeeper/byteballcommunityfundbytedonations.png "BYTE Donation Address")
* For BTC: 39zeGpu4nG6BqB9ARxd9xM9XThX9JCmx8t  
![Byteballcommunityfundbtcdonations](/uploads/cryptkeeper/newbyteballcommunityfundbtcdonations.png "BTC Donation Address")
* You can also donate Blackbytes! In this case, please contact Tony or CryptKeeper, who will guide you through the process.

-----

Ref [Reddit: Byteball Community Announcement](https://www.reddit.com/r/ByteBall/comments/5yau5j/byteball_community_announcement)

Byteball Community Announcement (self.ByteBall)  
submitted [March 2017] * by TheRealCryptKeeper



-----
**This Byteball Community Fund page main author: @cryptkeeper at the** [Byteball slack](http://slack.byteball.org/)
