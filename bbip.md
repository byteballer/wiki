<!-- TITLE: BBIP -->
<!-- SUBTITLE: Byteball Improvement Protocol -->

# BBIPs
https://github.com/byteball/BBIPs/blob/master/bbip-0001.md

A Byteball Improvement Proposal (BBIP) is a design document providing information to the Byteball community, or describing a new feature for Byteball or its processes or environment. The BBIP should provide a concise technical specification of the feature and a rationale for the feature.

We intend BBIPs to be the primary mechanisms for proposing new features, for collecting community input on an issue, and for documenting the design decisions that have gone into Byteball. The BBIP author is responsible for building consensus within the community and documenting dissenting opinions.

Because the BBIPs are maintained as text files in a versioned repository, their revision history is the historical record of the feature proposal.

## BBIP 1
 BBIP: 1  
  Title: BBIP Purpose and Guidelines  
  Author: Peter Miklos @pmiklos  
	Comments-Summary: No comments yet.  
	Comments-URI: https://github.com/byteball/bbips/wiki/Comments:BBIP-0001  
	Status: Draft  
	Type: Process  
	Created: 2017-07-20  
	License: BSD-2-Clause, OPL
	
	

-----
**This (copy/paste) page main author: @pmiklos at the Byteball slack**

