<!-- TITLE: Textcoin -->
<!-- SUBTITLE: Sending cryptocurrency through text even if the recipient doesn’t have a wallet yet -->

[video](https://vimeo.com/254898717){.vimeo}

The article text is excerpted (with minor edits) from [Medium article 2017-12-09 Sending cryptocurrency to email](https://medium.com/byteball/sending-cryptocurrency-to-email-5c9bce22b8a9)

# Wallet version 2.0+
## Sending Bytes by text

With the release of Byteball wallet version 2.0, Byteball users can send Bytes by email. Even if the recipient is not in Byteball yet. The sender just writes an email address where he would normally write a Byteball address. When he hits “Send”, his email app is opened with prefilled text for the recipient. The sender can edit it before sending.
The recipient receives an email with a link. Example:

> Here is your link to receive 0.001 GB. https://byteball.org/openapp.html#textcoin?pact-volume-lazy-midnight-mix-cool-fiction-symbol-tag-fiction-coral-sibling

He clicks the link and the money is added to his balance. If he doesn’t have a wallet yet, he is prompted to install one in order to receive the money.

There are more options. When the sender is on a mobile phone and switches to the wallet “Share via message” tab, he is able to use all the options available through the phone’s standard “Share” menu: text message, Telegram, WhatsApp, WeChat, Signal, Viber, Slack, Facebook Messenger, Skype, etc. The recipient receives the same link, but through a chat app.

### Refill
You can "refill" a textcoin address you have created/emptied. Simply find the interim address in your  wallet history, top it up, and when the new transaction is confirmed the original link will work again.

### All public assets
With version 2.1, you can send any public asset by textcoin, not only Bytes.

### Android
Another small but important update: when a textcoin link is clicked on Android which doesn't have Byteball app installed yet, the user will be automatically redirected to our app on Google Play and the textcoin will be passed on to Google Play, then to the newly installed app, and claimed immediately after the wallet is created.  Which is a very smooth process for new users. [Reference](https://bitcointalk.org/index.php?topic=1608859.msg27855312#msg27855312)

## Technical

On the technical part, the link which is delivered contains a BIP39 mnemonic (we use dashes instead of spaces to avoid confusion with a wallet seed). Only one address, with path m/44'/0'/0'/0/0, is generated from the mnemonic, and the coins are actually sent to this address. When the recipient claims the funds his wallet restores the private key from the mnemonic (everything happens in the background and looks instantaneous), and sweeps the entire balance from this address to one of his own addresses.

Two fees have to be paid, first to fill the temporary address where the money is stored in transit, then to sweep its balance. The sender adds a small amount to cover the fees spent when sweeping. So the amount displayed in the email or text message is the exact net amount. This is to make it clearer for the recipient, especially when he is new to crypto.

## Security

The textcoins might be delivered through insecure media, such as emails or unencrypted chats. That’s why it is recommended to use them only for small amounts or through end-to-end encrypted chats, such as Signal, WhatsApp, and iMessage.

## Human-friendly

If the sender makes a mistake in the email address or chooses a wrong recipient, the money is not lost. The sender can claim the money back using the same mnemonic. It is saved in the wallet for such occasions, and a “claim back” link is available on the transaction details page.

## No third party

This is like PayPal but with one important difference: the money is not kept in the custody of PayPal or any other third party. The money is sent directly from sender to recipient, peer-to-peer, in the spirit of crypto.

# Server-side
From https://bitcointalk.org/index.php?topic=1608859.msg26332250#msg26332250:

Now textcoins are enabled in server-side (headless) wallets and are available to developers.

Here is the doc about sending them: https://github.com/byteball/byteballcore/wiki/Sending-textcoins

Now, any service that has email addresses of its users (that is, almost any service) can easily mass-pay them using just email addresses.

## Chatbots

Textcoins are also easy to integrate with chatbots.  For example, a telegram bot could reward its users for certain actions, e.g. for taking surveys or writing reviews.

## Email lists
You can automatically send a different textcoin to everyone on an email list you have. Then claim back the ones unclaimed after 7 days, or whatever time you choose.

See the [Medium article: Using MailChimp to mass send payments as textcoins](https://medium.com/byteball-help/using-mailchimp-to-mass-send-payments-as-textcoins-5c1db06342e3)

# Cashback

Lastly, our own [cashback](https://byteroll.com/cashback) program now supports textcoins: merchants don't have to update their order forms to ask users about their Byteball address, instead they can give us the user's email address and we send a textcoin.

Updated API description: https://github.com/byteball/byteball/wiki/Cashback-API

-----
**This (copy/paste) page main author: @tonych at the** [Byteball slack](http://slack.byteball.org/)