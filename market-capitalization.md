<!-- TITLE: Market capitalization -->
<!-- SUBTITLE: What the term normally means and how it is applied to cryptocurrencies -->
# What is *market capitalization*?

The well-known site [Coinmarketcap](https://coinmarketcap.com) lists hundreds of altcoins next to their “Market Cap”, along with a large amount of timely data on each. But what exactly is a cryptocurrency’s “Market Cap”?

## Normal usage
> Market capitalization is the aggregate valuation of the company based on its current share price and the total number of outstanding stocks. It is calculated by multiplying the current market price of the company's share with the total outstanding shares of the company.

Ref [The Economic Times (India)](http://economictimes.indiatimes.com/definition/market-capitalization)

"Outstanding" here means not inside the company's own Treasury. Some, even all, of these outstanding shares may have legal restrictions preventing the owners dumping them on the market at any time.

### But unicorns!

Decades ago the term was taken to mean something. It started to look wild in 2000 when TimeWarner, a long-established company with huge real-world holdings of studios, magazines and newspapers, a vast movie back-catalog, and much more ~~got taken over by~~ merged with AOL (America OnLine), an internet upstart with lots of people in offices, billions of distributed CDs to get people onto this new-fangled Internet thingy -- most now in landfills or hanging up in gardens to scare away birds -- and little else. 

Today, Uber, the ride-share ~~company~~ app, again has lots of people in offices and owns little else, and somehow is considered -- by virtue of its insane market cap -- to be worth more than Exxon-Mobil with all its heavyduty oil drilling platforms, pipelines, refineries, 70,000 employees getting their hands dirty out in the real world, and so on. 

### Conclusion
A company's market cap does give some indication of the company's size, but don't read too much into it.

## Crypto usage
It is generally taken to be the aggregate valuation of a cryptocurrency based on its current unit price multiplied by the number of units in circulation. So if you look it up at Coinmarketcap, the market cap for Bitcoin is the current price multiplied by the number of coins created so far (about 16 million out of the total of 21 million that will have been created by 2140). And the market cap for Byteball is the current market price multiplied by the number of units so far distributed (about 645,000) out of the total 1,000,000 already created and due for full distribution maybe in 2018.

# No problem, right?
Well, not exactly. It's fine if you don't look too closely. But look at these examples:

**A new scamcoin:** Some lazy scammer instamines 50% (21 million) of the intended final total volume (42 million) of his new coins, *buttclones*. He sells one *buttclone* to a friend for $5. So the market cap of the *Buttclones* platform is $105 million?

## (more examples from real coins should be added)
# Conclusion
Like a commercial company, a crypto's market cap does give some indication of its size, but don't read too much into it.


-----
**This "Market capitalization" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
