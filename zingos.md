<!-- TITLE: Zingos -->
<!-- SUBTITLE: A quick summary of Zingos -->

Zingos are very much like [Tangos](https://byteroll.com/tangos), except...
# Zingos have two decimal places
Four dollars can be written as $4 or $4.00, and we can have $4.57 too. Similarly, a hundred Zingos (Z for Zingos here) can be Z100 or Z100.00, and we can have Z100.57 too. How this works in Byteball is:

* Z40,000.00 total in your wallet shows on the home screen as 40,000 Zingos by Johre etc
* Z39,164.81 total in your wallet shows on the home screen as 39,164.81 Zingos by Johre etc
* Z100 entered in the Send screen sends Z100.00, which shows in Explorer as "10,000"
* Z100.00 entered in the Send screen sends Z100.00, which shows in Explorer as "10,000".

Be careful not to send 100x the amount you wish to. With Zingos, having zero monetary value, it doesn't matter too much. But you are practising here for when you will be working with an asset of value having two decimal places.

## Read the Tangos article!
All the general dos and don'ts in the Tangos article apply equally to Zingos, so make sure you understand what is written there.

# Definition
[Source](https://explorer.byteball.org/#FPboi/y+Vo008aCHgPsEpSDQe1dogkKH0e1Z/iCcH84=)  
cap: 1000000000000000  
is_private: false  
is_transferrable: true  
auto_destroy: true  
fixed_denominations: false  
issued_by_definer_only: true  
cosigned_by_definer: false  
spender_attested: false

# Registry publication
[Source](https://explorer.byteball.org/#iOsJ4a+OWyvcFbk6NdLWnl/v+QxIk5VaxqwA9Hl7pfk=)  
asset: FPboi/y+Vo008aCHgPsEpSDQe1dogkKH0e1Z/iCcH84=  
decimals: 2  
name: Zingos by Jore Bohne  
shortName: Zingos  
issuer: Jore Bohne  
ticker: ZINGO  
description: A token of no monetary value to practise with, to be spread around, not worth hoarding because there are so many of them.


-----
**This "Zingos" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

