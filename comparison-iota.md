<!-- TITLE: Comparison of Byteball and Iota -->
<!-- SUBTITLE: Quick bullet-point comparison -->

# Byteball
* First cryptocurrency with DAG-structure to trade on exchanges,  2016 late December.

* Does not require proof-of-work

* Decentralized, not fully trustless. Has 12 witnesses, trust required that majority will not conspire together, users select and can replace witnesses see [Witness](Witness) 

* Fair, open initial distribution by proving BTC ownership, spanning a year

* Has fee, the amount of data to store in the network is the amount of currency, bytes, to pay. To store a transactions costs about 520 bytes, full nodes and witnesses colllect/split the fee.

* To pay for goods/service of value 52 000 bytes, the fee would be 1%. Bytes are traded in 1 000 000 000 (1GB) as of (June 2017), total supply is 1 000 000 000 000 000 or *1 petabyte*. For todays price of $700 for 1GB, the fee of 600bytes (high estimate) is $0.0004. For fee to be $0.4, the price of 1GB would be $700 000.

* Declarative smart contracts, oracles, user-defined (private) assets, immutable data-storage. 

* Community effort, no organized businesses, foundations.

* Uncensored, unmoderated communication channels.

# Iota
* Began as idea/ICO in 2015, trades on exchange 2017 July.
 
* Requires Proof-of-Work

* Zero-fee, the transaction is payed for by expending compute-resources doing Proof-of-Work.

* The data-structure DAG also sometimes called Tangle

* Total supply is 2 779 530 283 277 761, traded in (1 000 000, 1Mi). *2.779 petaiota*.

* ICO, over-the-counter trade before exchange.

* Developer releases mark milestones as checkpoints in network.

* Has foundation, which has wide business connections and signed partnerships.

* Censored/moderated communication channels.

* Not yet (2017 July) decentralized, has Coordinator/Proof-of-Authority.


A few useful technical discussion links,

https://www.reddit.com/r/ethereum/comments/6h2jut/have_ethereum_devs_considered_tangle_dag_instead/

https://www.reddit.com/r/Iota/comments/6h3sc8/what_are_the_cons_of_iota/

-----

Some points of comparison by Tony from [Altcoin Spekulant Oct 6, 2016](https://altcoinspekulant.com/2016/10/06/byteball-exclusive-interview-with-developer-tonych-in-english/) interview: 

| Byteball | IOTA |
|-----------|-----------|
| the ordering of transactions is based on main chain | IOTA uses PoW |
| there is no chance of orphaning | parts of the DAG can be orphaned |
| there are deterministic criteria when a transaction becomes final | there are no exact criteria, it is still probabilistic, based on intuition |
| has multiple assets and smart contracts | is a single token currency with only plain payments | 
| there are assets that are transferred privately | all payments are public |

# Raiblocks?
See this [Reddit thread](https://www.reddit.com/r/CryptoCurrency/comments/7iv20r/dag_coin_comparison_byteball_iota_raiblocks_etc/) for a DAG coin comparison (Byteball, IOTA, RaiBlocks).

-----

**This "comparison" page main author: @portabella at the** [Byteball slack](http://slack.byteball.org/)
