<!-- TITLE: Chatbot -->
<!-- SUBTITLE: You can pair your wallet with a bot, which emulates the experience of chatting with a live person. The bot will have very limited responses, but enough to get the job done. A merchant bot could allow you to select from a range of pizzas, for example, then pay for your order with two clicks while still in that chat session. -->
# Botstore bots
Byteball v.1.9.0+ has a "bot store". Your hub provides the list. The default hub has 19 currently:
* Real name attestation bot
* Flight delay insurance
* Transition bot  
* Byte-BTC exchange  
* Flight delays oracle  
* Sports oracle  
* BTC oracle 
* Rosie bot
* Byteball Asset Manager
* Zork | game  
* Poll bot
* Blackbytes Exchange BEEB (Trustful^)
* Blackbyte Exchange [freebe] (Semi-trustless^)
* Buy blackbytes (trustless)  
* Slice&Dice MUD  
* Betting bot (Semi-trustless)  
* Luckybytes Lottery (provably fair)  
* TitanCoin ICO  
* Byteball-Altcoin Exchange Bot
* Fun-coins faucet


^ Note that in the cryptosphere, "trustless" is generally preferable to "trustful" -- the reverse of what one might expect. And neither word equates to "trusted" or "untrusted".

# Descriptions
Links are not needed in this section. Access the bots via the Bot Store in your wallet Chat.

## Real name attestation bot
Verify your real name to get access to services that require KYC. Attestation that proves your verification is saved on the public database, but no personal data is published without your request. Your data is saved in your wallet and you can easily disclose it to the service that needs the data. After first successful verification, you are rewarded with $20.00 worth of Bytes from the distribution fund.

## Flight delay insurance
This allows one to take out insurance on a possible flight delay one day to three months in the future. See the wiki article [Oracle](https://byteroll.com/oracle) for details.

## Transition bot  
See the wiki article [Airdrop](https://byteroll.com/airdrop) for details.

## Byte-BTC exchange 
This provides a book for users. See the wiki article [Trading](https://byteroll.com/trading) for details.

## Flight delays oracle  
Used in conjunction with flight delay insurance.

## Sports oracle
See the wiki article [Sports betting](https://byteroll.com/trading/sports-betting) for details.

## BTC oracle 
This oracle posts Merkle Roots of all Bitcoin transactions in a block every time a new Bitcoin block is mined. You can use its data to p2p trade bytes and bitcoins. If you are receiving bytes (sending btc), chat with the oracle after sending your bitcoins to get the Merkle Proof of your Bitcoin transaction and unlock your bytes from the smart contract.

## Rosie bot
This is an open-source conversational AI.

## Byteball Asset Manager
See the wiki article [Asset](https://byteroll.com/asset) for details.

## Zork | game  
Play one of the earliest interactive fiction computer games developed between 1977 and 1979. The game unfolds in a maze-like dungeon, where the user must battle trolls and solve puzzles in order to find twenty trophies to bring back to the house outside which the game begins.

Developer: @Hyena

## Poll bot
Now we can have referendums.  This bot assists in making a vote.  A vote is a small transaction that pays to yourself and is signed by your most funded addresses; the **weight** of the vote is the combined balance of the signing addresses.  Note that there is a privacy concern because by making a vote you associate a number of your addresses. 

## Blackbytes bots
See the wiki section [Blackbytes#bots](https://byteroll.com/blackbytes#bots)

## Buy blackbytes (trustless)
Instantly buy blackbytes for bytes. The sale is done via a conditional payment smart contract, so the seller can't scam you.

Source code: https://github.com/byteball/conditional-token-sale. 

## Slice&Dice MUD [Multi-User Dungeon]
Combining the elements of provably fair online gambling and role-playing games, Slice&Dice Dungeon delivers a unique gaming experience for everyone. The player takes a role of a dungeon inhabitant who has come across a violent underground casino. The main goal in this game is to become a powerful and respected member of the community. Doing so requires one to gamble on the Byteball tokens and to get into fights with other players as part of the endless struggle for power.

Developer: CoinGaming.io

## Betting bot (Semi-trustless)
Bet on sport fixtures by taking the trustless offers immediately available. Or be the bookmaker and develop a business by proposing competitive odds for popular events.

Developer: papabyte.com

## Luckybtes Lottery
An in-app lottery in which you can play using your Byteball Bytes. There are three different game modes to participate in. Win large amounts of Bytes depending on the number of players on the principle of "the winner takes all". All games are provably fair. Each lottey comes with a game and proof hash which lets a player validate and prove the results against manipulation.

Developer: pxrunes, https://lucky.byte-ball.com

## TitanCoin ICO
Invest in Titan Coin -- a new token pegged to the price of 1 kg of ilmenite concentrate. Ilmenite concentrate is the main raw material used for production of titanium dioxide.

Project page and investor information: https://titan-coin.com

### ICO bot

This is the first use of our ICO bot which was published on github just recently https://github.com/byteball/ico-bot.  The bot is designed to help entrepreneurs raise funds and distribute their tokens, easily and securely.  It now (2018-01-07) accepts Bytes, BTC and ETH. The tokens are issued immediately after the payment is confirmed.

## Byteball-Altcoin Exchange Bot
Exchange over 60 altcoins to Bytes, Bytes to altcoins, or altcoins to altcoins. Receive your coins as fast as the network confirms your transaction. The fee is only 0.75%. Powered by Changelly.

Developer: Robert Huber, @robbterr, http://byteball-exchange-bot.com

## Fun-coins faucet

This new (Jan 19) bot gives out free Tangos, Tingos, Zangos and Zingos. These tokens have zero monetary value so you can practise textcoins and smart contracts with zero risk.

* NOTE: To send any of these fun-coins from your wallet, you will need Bytes to cover the transaction fees, usually less than 1000 bytes per transaction.

If you wish, you can donate small amounts of bytes for transaction fees to the sending address, CARJFJ6SKDC2XGLX2XSNMIITAVRDEW2R, but it is not necessary.

Developer: papabyte.com (@neversaynever)

# Security
[Slack comment from @vakar 2018-01-10:]
> A bot has no way to connect to the private part of a paired wallet. It can only send text messages to the chat. As long as we only have text (and no html) all bots are safe and cannot "steal coins".

-----
**This "Chatbot" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

