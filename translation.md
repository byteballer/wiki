<!-- TITLE: Translation -->
<!-- SUBTITLE: Info on translations of Byteball writings -->

# Links
[Byteball webpage](https://crowdin.com/project/byteball-web)

[Wallet translation](https://crowdin.com/project/byteball)

After all translations are complete there can be votes on the best version. When Tonych considers the process complete the translation becomes available. Thank you for the community efforts.

