<!-- TITLE: License -->
<!-- SUBTITLE: Creative Commons Attribution-ShareAlike 3.0 Unported License -->
# License and copyright

All the content of this wiki is copyrighted by the wiki editors and contributors and is formally licensed to the public under the [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/)


References:  
[External Wikipedia link: Text of license]( https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License)  
[External link: Wikipedia Copyrights](https://en.wikipedia.org/wiki/Wikipedia:Copyrights)


# Human-readable summary of the full license
![Cc Deed](/uploads/cc-deed.png "Cc Deed"){.align-center}


-----
**This "License" page main authors: @portabella and @slackjore at the** [Byteball slack](http://slack.byteball.org/)

