<!-- TITLE: Identity Verification -->
<!-- SUBTITLE: This attestation allows the user to prove to anybody that his Byteball address is linked to a verified person, without disclosing any personal information -->

# Introduction
Starting 18 January, every Byteball user can link his Byteball address to his real world identity. The user’s personal data is verified by Jumio, the leading provider of identity verification services, and stored in the user’s Byteball wallet. At the same time, a hash of the personal data is stored on the public DAG and signed by a trusted attestor. The attestor also serves as a witness, so it is already trusted.

This attestation allows the user to prove to anybody that his Byteball address is linked to a verified person, without disclosing any personal information. It also allows to reveal the private information to individual service providers on demand, and the service provider can easily verify authenticity of this information using the hash stored on the public DAG.[[1](#references)]
# How to use
Install the *Real name attestation bot* from the Bot Store in a **Single-Address Wallet**, and follow ALL the instructions. After first successful verification, you are rewarded with $20.00 worth of Bytes
from the distribution fund.

## Exact amount
The $8 fee will get converted into bytes at the time you apply. Pay this **exact** amount promptly. Don't pay too much, and don't pay in two or more transactions. If you delay for too long, the exchange rate will have changed, so don't come back after an hour and pay the same amount assuming it is still valid.

## Glare
You’ll need to show your ID in front of the camera and make a selfie. After the photos are taken, Jumio will process them to ensure that the ID was not tampered with and it matches the selfie. Make sure there is no glare from reflected light, especially if your ID has a glossy surface. Otherwise Jumio can't read it.
## Webcams
Use your Android camera if possible. Sometimes it is tricky to get webcam ID images big enough and in focus. 

## Latin characters
Note the warning about Latin characters. If your government-issued ID only has Chinese characters, for example, your application will be rejected and you will lose the $8.

## Time limit
There is a 30-minute time limit in doing this, so don't go and have lunch in the middle, or you will lose the $8 again.

## More than one verified address
There is no way to remove an attestation (maybe the device was lost) but users can attest their new Byteball address. Then there will be (at least) 2 attestations of the same user. Tony would expect service providers to check if a later attestation of the same user exists, and if it does, not to trust the old one.[[2](#references)]

## No refunds
There are no refunds for any reason.

# Numbers
See how many Byteball verification are done [here](https://byteball.fr/heartbeat.php) (last two lines of "12 hour snapshot").

# External links
* https://www.jumio.com/about/press-releases/byteball-ico-identity-layer/
* http://finovate.com/jumio-partners-byteball-bring-kyc-ico-issuers/

# References
1. https://medium.com/byteball/bringing-identity-to-crypto-b35964feee8e  
2. https://www.reddit.com/r/ByteBall/comments/7rpsmn/jumio_verification_and_comprised_private_keys


-----
**This "Identity Verification" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)