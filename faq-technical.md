<!-- TITLE: FAQ-technical -->
<!-- SUBTITLE: Technical questions/answers about the Byteball system -->

# Technical FAQ
### What is difference between Full and Light wallet?

> When first starting the Byteball app on a Desktop PC, it asks for Full or Light mode. Full mode means the software will syncronize with the rest of the network and act as a full-validating node on the Byteball network, helping offload the bandwidth and processing needs and securing each others transactions. Light wallet participates only as a client, connected to a Byteball hub. The default hub is byteball.org/bb and this setting can be changed in the Global settings menu. Full wallet receives fees from the headers-commision, on average 100-200 bytes, for each unit the full wallet sees first on the network. Light wallet does not receive these fees.

### What is a witness?

> A witness is a trusted entity on the network and a trusted person/organization with honesty and integrity is required to run it, the witness fullfills an important duty on the Byteball network. Witness stamps each transaction it sees, it witnesses the transactions. There are 12 witnesses. Bad witnesses could collude with other witnesses to stall the network or censor some transactions, or attempt double-spends.

## Backups!

To backup, use the options in the Global settings menu.
If you must do it manually, whether full or light wallet, zip or copy ~/.config/byteball (Linux/MacOsX) or %LocalAppData%\Byteball (Windows) folders.

[Backups](https://byteroll.com/backups) - for more details information and scripts

### Cold storage

USB-memory sticks can store the full database as of 2017 May, which is around 3GB. Use several for backups, as usb-sticks can fail.

## Currency

### What is difference between bytes and blackbytes?

Bytes are native currency, they pay transaction fee. Blackbytes are the first defined asset, a private asset on the Byteball platform.

Blackbytes, when used over Tor, are anonymous, untraceable private currency.  
There exists 2.1111 more blackbytes than bytes.  
Blackbytes are not in any public database, they are only transferred from peer to peer as encrypted messages through the Byteball network.  
Only the spend-proof is published on the public Byteball database, this does not reveal the amounts or even which private asset was transferred, or by the from or to.  

### How can I trade blackbytes?

Find a peer, anywhere, but on byteball.slack.com you have #trading_blackbyte.  
Then pair your devices by exchanging pairing-code which you find in the Chat interface. Click "Invite the other device" and "Accept other device".  
Click the "..." icon and "Insert my address" now you can click "Offer a contract" and here you can specify  
"When I pay bytes and peer payes blackbytes, within 4 hours, the bytes are released and blackbytes are released to me", ie a simple smart contract.

Trades of blackbytes are decentralized, distributed. A centralized exchange would find out too much about its users and the purpose of blackbytes defeated - that is concealment of amounts and senders/receivers.

### Developer guide for issuing new assets
Developer guide for issuing new assets on Byteball:
https://github.com/byteball/byteballcore/wiki/Issuing-assets-on-Byteball

Posted by tonych on Bitcointalk thread 2017-05-30.


-----
**This Technical FAQ page main authors: @portabella? @markcross? at the** [Byteball slack](http://slack.byteball.org/)

