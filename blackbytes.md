<!-- TITLE: Blackbytes -->
<!-- SUBTITLE: One of the two Byteball currencies. When you want complete privacy, pay in blackbytes, a cash-like untraceable currency whose transactions are not visible on the public database. They are sent peer-to-peer instead. -->
# Blackbytes

Blackbytes is the name of one, the first, defined private asset in Byteball. Byteball supports user-defined assets, the assets has properties such as private or public, amount of coins, coin-denominations and if any authentifiers is required for transferring, or if the asset is transferrable.

Blackbytes are defined as a private asset, meaning its never posted to the public Byteball database, it is ever only transferred when Byteball wallets have paired, private assets are sent encrypted through the Byteball network from device address to device address. When used over Tor, blackbytes become anonymous, and only the device-address can be known to the Byteball network.

To verify that a peer has not sent the same blackbyte to another peer before attempting to trick you into accepting it again, a spend-proof is posted to the public Byteball database. Upon receiving the blackbyte your full-wallet/light-wallet-hub provider, checks the spend-proof. The spend-proof does not contain amounts or sender/receiver information, it is merely a signature.

## Blackbyte Exchange

If blackbytes are traded on a centralized exchange, the exchange would have enough information to reveal the origin and destination of blackbytes, and amounts traded, which defeats their purpose.  
Blackbytes hence can only be traded on decentralized exchanges, with conditional payments in the Byteball wallet. Peer discovery has to happen outside the Byteball wallet. Such an exchange is yet to appear (2017-May).

## Recommended usage

1. Start a light wallet on one of your computers/smartphones, do not use revealing device name for the wallet, pick something like localhost or byteball.  
2. Enable Tor in the Global settings menu, this requires that you have tor installed or Orbot on Android, a tor-proxy for any Android app.  
3. Send your blackbytes to this wallet, and use it to trade with and pay for goods and services.  


## Good links

Technical discussions, https://bitcointalk.org/index.php?topic=1574508.msg15806728#msg15806728

[@loyce's bitcointalk Blackbytes FAQ](https://bitcointalk.org/index.php?topic=2020882)

## Sending
To receive blackbytes (BB/GBB) if you do not have any yet, try doing it this way: start a chat with paired device, click INSERT ADDRESS in chat menu, now the owner of the blackbytes can click your address and he will see an option to send blackbytes to you. You will need some bytes in your wallet to pay the fee for the blackbyte transfer.

## Byte-BTC exchange
It is also possible to trade BTC and GBB as described in this [trading page section](https://byteroll.com/trading#peer-to-peer-byte-btc-exchange), only using GBB instead of GB.

# Bots
## Buy blackbytes (trustless)
Instantly buy blackbytes for bytes. The sale is done via a conditional payment smart contract, so the seller can't scam you. This bot only sells blackbytes; it will not buy blackbytes.

### Pairing code
Access the bot from your wallet's bot store.

## Freebe
Low-fee (0 for trade makers, 0.5% for trade takers) blackbytes exchange: trading bot by @pxrunes. Slack channel #freebe.

* instant blackbyte exchange
* trustless: all funds are held by the user
* huge chat room: chat with many other byteball users
* See [Reddit post](https://www.reddit.com/r/ByteBall/comments/6rm27u/announcing_trusless_blackbyte_exchange_fully/) for **very** helpful detailed instructions  
* See [freebe website](http://freebe.byte-ball.com)

### Pairing code (or add from wallet bot store)
* [Click this link](byteball:ApSicldzuDl675iiUyWdmO7pLl8MPgeuNg4qOr13EkNJ@byteball.org/bb#globalchat), or 

* copy/paste same code:  
`ApSicldzuDl675iiUyWdmO7pLl8MPgeuNg4qOr13EkNJ@byteball.org/bb#globalchat`

## BEEB
Blackbytes Escrow Exchange Bot, a commercial bot from @seb486. Instant buy and sell batches of blackbytes 24/7. Slack channel #beeb.

### Pairing code (or add from wallet bot store) 
* [Click this link](byteball:AxV6ohKFORqIGfGqCZgjK1HlL8vBiNltcWWaI0Rc9yN+@byteball.fr/bb#0000), or 

* copy/paste same code:  
`AxV6ohKFORqIGfGqCZgjK1HlL8vBiNltcWWaI0Rc9yN+@byteball.fr/bb#0000`

### Web front end

* Price and volume graphical history  
* 24hr volume  
* Current best ask  
* Detailed help menu  
* API interface to blackbyte prices.

http://www.beeb-bot.com

-----
**This "Blackbytes" page main author: @portabella at the** [Byteball slack](http://slack.byteball.org/)  
**Bots section added by @slackjore at the** [Byteball slack](http://slack.byteball.org/)
