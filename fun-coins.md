<!-- TITLE: Fun-coins -->
<!-- SUBTITLE: Coins with no monetary value readily available for use as practice tokens/assets -->
[video](https://vimeo.com/253273900){.vimeo}
# Introduction
Fun-coins are tokens on the Byteball platform that:

* exist in vast quantity, like 10^15 units  
* are readily available to anyone for free  
* are free of as many restrictions as possible  
* are designed for practice with various platform features  
* have zero current and future monetary value because they are not scarce, so  
* no-one needs to hoard them!

# Some available fun-coins
| Name   | Start date | Quantity      | Issuer     | Documents        | Available from    |
|--------|------------|---------------|------------|------------------|-------------------|
| Tangos | 14/01/18   | 10^15         | Jore Bohne | Def[[1](#references)] Reg[[2](#references)]  | Fun-coins faucet |
| Zingos | 14/01/18   | 10^13[[3](#references)]     | Jore Bohne | Def[[4](#references)] Reg[[5](#references)]  |  Fun-coins faucet  |
| Tingos | 21/01/18   | 9 x 10^15     | Jore Bohne | Def[[6](#references)] Reg[[7](#references)]  |  Fun-coins faucet  |
| Zangos | 21/01/18   | 9 x 10^13[[8](#references)] | Jore Bohne | Def[[9](#references)] Reg[[10](#references)] |  Fun-coins faucet  |
| Credits | 7/02/18   | 5 x 10^15 | Jore Bohne | Def[[11](#references)] Reg[[12](#references)] |  Fun-coins faucet  |

# Getting ready
## Two devices
If you have separate (not multisig) wallets on two or more devices, maybe a laptop and a cellphone, you can pair those and execute smart contracts between them, which is excellent practice. You don't need to pair with someone else.

## You'll need some bytes
To send any fun-coin from your wallet, you will need Bytes to cover the transaction fees -- as with any Byteball transaction -- usually less than 1000 bytes per transaction. If 1 GB = $1000, 1 MB = $1, 1 KB (1000 bytes) = 1/10 cent.

## Change addresses
Be aware of [change addresses](https://byteroll.com/change-addresses) and how they work. You might first notice this when you send some of your nice new fun-coins to a friend and then find you cannot send any more anywhere for 5-10 minutes.

# Zingos/Zangos have two decimal places
Four dollars can be written as $4 or $4.00, and we can have $4.57 too. Similarly, a hundred Zingos/Zangos (Z here) can be Z100 or Z100.00, and we can have Z100.57 too. How this works in Byteball is:

* Z40,000.00 total in your wallet shows on the home screen as 40,000 Zingos by Jore etc
* Z39,164.81 total in your wallet shows on the home screen as 39,164.81 Zingos by Jore etc
* Z100 entered in the Send screen sends Z100.00, which shows in Explorer as "10,000"
* Z100.00 entered in the Send screen sends Z100.00, which shows in Explorer as "10,000".

Be careful not to send 1/100 or 100x the amount you wish to. With Zingos/Zangos, having zero monetary value, it doesn't matter too much. But you are practising here for when you will be working with an asset of value having two decimal places.

# Fun-coins faucet
You can get Tangos, Tingos, Zangos, Zingos and Credits from the fun-coins faucet in the [BOT STORE](https://byteroll.com/glossary).

If you wish, you can donate small amounts of bytes for transaction fees to the sending address, CARJFJ6SKDC2XGLX2XSNMIITAVRDEW2R, but it is not necessary.

Developer[[13](#references)]

# Smart contract
Here's how to use fun-coins in a [smart contract](https://byteroll.com/smart-contract). Let's say you are exchanging 10,000 Tangos for 75 X's (KB, Zingos, whatever) with a peer. First, pair your devices.

## Pairing
One party gives the other a pairing code, either written out in full or via QR code. Generate this in Byteball wallet chat by clicking on "+ Add a new device" then "Invite the other device". It will look something like `AhHPXCpCSTzD1CF53ELGTAsZ6MCA8Ogvk+koyibfPt/2v@byteball.org/bb#GxVBizZ2DSEf`. The other party in Byteball wallet chat clicks on "+ Add a new device" then "Accept invitation from the other device" and pastes the pairing code into the almost-invisible line above the "pair" button.

## After pairing
1. In chat, peer sends you a receive address (click on "..." at the left of the chat window).  
2. You left-click the address, and click "Pay to this address".  
3. From the drop-down menu, select Tangos. Click "bind this payment to a condition". Use "I receive another payment" as the condition. Adjust the "take-back" time if you wish. Fill in the amount of 75 X's. Click "Bind payment".  
4. Fill in your payment amount of 10,000 Tangos and click Send. Or click Cancel if you were just trying it out.  
5. Peer reads the contract and if it's what was agreed, clicks Send. Done deal, both payments occur at once.

## Notes
* See the new smart wallet by selecting it right at the top of the screen to the right of the main wallet's name.  
* If you just received Tangos, then the smart wallet won’t have any (white)bytes in it yet to cover the sending fee. Click the Receive tab, copy the address, then paste it into the Send screen of your regular bytes wallet. Send maybe 5000 bytes to overpay any possible transaction fee, then after you've moved the Tangos you can send the remaining bytes back to your main wallet.  
* You can see the details of the smart (wallet) contract by clicking the little eyeball to the right of the smart contract home screen.  
* Be careful to make payments from the correct wallet -- maybe you were looking in a smart wallet and forgot to get back into your main wallet.  
* You may get an error message if the funds you are trying to send haven't confirmed yet.  
* It is best not to send other payments in the 5-20 minutes while funding and waiting for execution of a smart contract. Additional payment notifications in the chat, and pending confirmations in the wallet home screen, can be confusing.

# New tokens in your wallet
(Copied from [Asset](https://byteroll.com/asset))

You started off with 0 bytes in your wallet. Later, you probably got some bytes, and maybe some blackbytes. At present, maybe some Tangos and/or something else. If you click the Send tab, you'll see you a drop-down menu, and you can spend from any of your assets/tokens listed.

Note that these listed asset/token names remain, even if the balance of one particular asset/token is zero. So every time someone newly receives some Bongos, the Bongo asset/token name gets added to their wallet asset/token list, indelibly so for now. After a few of these, if you don't really want them, they act as spam.

So, please don't send your new assets/tokens to someone without asking them first if they want them.

# External links
[Byteball Asset Manager](https://byteball.market/#!/asset/order), where you can create your own token in 5 minutes

# References
1. https://explorer.byteball.org/#0Qki2BWSJ80dMN58Dq1rrJugaYyFndxkZloLJby+olU=  
2. https://explorer.byteball.org/#ammQoP5B+hpm5wX2EVjro6WGLVDePxUoxXAMhIKq5yE=  
3. plus 2 decimal places makes 10^15  
4. https://explorer.byteball.org/#FPboi/y+Vo008aCHgPsEpSDQe1dogkKH0e1Z/iCcH84=  
5. https://explorer.byteball.org/#iOsJ4a+OWyvcFbk6NdLWnl/v+QxIk5VaxqwA9Hl7pfk=  
6. https://explorer.byteball.org/#O1DbJWbZJfKhjZQYH5RrdRQ2ojMuo2WiaKbjIWSWd4E=  
7. https://explorer.byteball.org/#ZWKx0ghzXxKkaSLH3jAAaqogCmmLIq7B7YXnocxjW7U=  
8. plus 2 decimal places makes 9 x 10^15  
9. https://explorer.byteball.org/#3qacTCGOkrzYRyekq8yRIT/Pc30Pb/BcmTEHDY/aTz0=  
10. https://explorer.byteball.org/#9OPMHyhSR+ddntJlGlSSFHj9etFj4vEB7FE/7DJxR4s= 
11. https://explorer.byteball.org/#o/VVwWXXGpb/XZ1fHh0m/SSgyNWydcYKPk6ScsJ4WRM=
12. https://explorer.byteball.org/#jkv+5KPlnpn6uuJlXuMt/yudWFo8kK7GgyooFbAaOUU=
13. https://papabyte.com (@neversaynever)


-----
**This "Fun-coins" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
