<!-- TITLE: FAQ-general -->
<!-- SUBTITLE: General questions/answers about the Byteball platform -->
# General FAQ
## What is money?
Simply stated, money is any item or verifiable record that is generally accepted as payment for goods and services and repayment of debts in a particular country or context. The main functions of money are distinguished as **a medium of exchange**, **a unit of account**, and **a store of value**. 

See the wiki [Money](https://byteroll.com/money) article for further explanation.

## What is a cryptocurrency, and how well do cryptos satisfy the three main functions of money?
A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. Generally, cryptos do a lousy job at satisfying these three formal functions.

See the wiki [Money](https://byteroll.com/money) article for further explanation.

## What makes the Byteball platform different?
It's the first crypto with smart (conditional) payments. Its conditional-payments system allows you to write (or agree to) simple human-readable contracts. You set a condition for how the payee receives the money. If the condition is not met, you get your money back. This substitutes for trust between strangers because neither is able to scam the other.

The features that set the Byteball platform apart from other cryptos are explained in more detail in the wiki [features](https://byteroll.com/features) article.

### But I thought electronic payments were just a small part of the platform?
Quite right. Byteball is a decentralized system that allows tamper-proof storage of arbitrary data, including data that represents transferrable value such as currencies, property titles, debt, shares, etc. Storage units are linked to each other cryptographically such that the whole data structure becomes rapidly unchangeable.  
Ref [Byteball white paper, p1](https://byteball.org/Byteball.pdf)

However, promises of future features, no matter how ground-breaking and useful, have a very short shelf-life. It's usually far better to go with what's available right now.

## How do I get Byteball stuff?
### Wallet
Download a wallet from the main Byteball site at [byteball.org](https://byteball.org).

### (White)bytes and blackbytes
**From other Byteball users**  
Pair up with another user you meet face-to-face or in an online forum like our [Slack channel](https://byteball.slack.com).

**From an online exchange**  
Register with an online exchange, send them your other cryptocurrencies or fiat, and then trade. There is a list of such exchanges in this wiki [Trading](https://byteball.org/trading) article. 

**From an airdrop**  
See the wiki [Airdrop](https://byteroll.com/airdrop) article for details.

### fair
## Is the airdrop (distribution system) unfair?  

The ideal way to distribute new money tokens would be to give everyone who wants to use them an equal amount at the time of creation. But who wants to use them? How would we know? And how would we know that one person would not fake being a hundred persons and take more than his fair share?

To determine such interest one must use an indicator that cannot easily be faked. Owning bitcoins fulfils that requirement. All the money tokens, (white)bytes and blackbytes, came into existence at the first moment of creation of the Byteball master record. They are being distributed in maybe 10-15 "airdrops", currently one every full moon. 

The distribution could be considered to be unfair, but unfair distribution of money exists in real life: some people are billionaires and some are beggars. And it is not at all unreasonable to assume that someone who owns many bitcoins -- 8 years into its lifecycle -- is more likely to contribute to the use and expansion of another crypto than someone who owns only a few, or none.

So here we are.

## Is there a block explorer to find transactions?
No blocks, but the Byteball equivalent is the [DAG (Directed Acyclic Graph) Explorer](https://explorer.byteball.org/)

## How often should I back up my wallet? 
Every time your blackbyte balance changes

## I think the unit should be MB instead of GB
You can change it in your wallet. On an exchange, then instead of 1 GB = .13 BTC you would see 1 MB = .00013 BTC. Hard to work with.

## How much did it cost to store a transaction on byteball's DAG? (Directed Acyclic Graph)
The fee you pay is identical to the size of the data you want stored. So a storage unit that takes up 18,000 bytes in the distributed Byteball database will cost 18,000 (white)bytes to send there. Currently a usual transaction fee is maybe 500 bytes, with a blackbytes fee being maybe 1000 bytes or so. If 1GB = $750, then 1MB = $0.75, and 1KB (1000 bytes) = $0.00075. So that's around 1/10 cent US.

## How about Tor?
Desktop: Switch it on in Settings.

Android: Install Orbot app for Android. Then start Orbot, enter the left menu, and enable Apps VPN Mode. You will be presented the list of apps, tick the checkbox next to Byteball.

Ref [Byteball at medium.com](https://medium.com/byteball-help/how-to-enable-tor-in-byteball-android-app-38cb1605a51d)

-----
**This General FAQ page main authors: ? and @slackjore at the** [Byteball slack](http://slack.byteball.org/)
