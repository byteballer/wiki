<!-- TITLE: Antifragility -->
<!-- SUBTITLE: A quick summary of Antifragility -->

# Why stress tests?
Antifragility is a property of systems that increase in capability, resilience, or robustness as a result of stressors, shocks, volatility, noise, mistakes, faults, attacks, or failures. It is a concept developed by Professor Nassim Nicholas Taleb in his book, *Antifragile* and in technical papers.[Wikipedia: Antifragility](https://en.wikipedia.org/wiki/Antifragility)

It applies to any cryptocurrency in that a coin's main platform must be frequently stress-tested as it matures, otherwise the live production will break unexpectedly, maybe with disastrous results. Sometimes, like an exchange being severely hacked, it can mean the end of that platform.

[video](https://www.youtube.com/watch?v=810aKcfM__Q){.youtube}

# Byteball stress tests
These are community-produced tests, with no "team" input (at present). They are experiments.

## Jan 2018
@portabella has agreed to do one "next week", i.e. Jan 7-13. He provided the address 3JI5LUOH755Y3XV2IYF25M2YG2MNHZST, but it is not clear what for.

The idea is to see if the platform can cope with a relatively large volume of transactions: 10 tps (transactions per second)? 50tps? 100tps? 500tps? 1000tps for 5 minutes?

What seems to be needed is an automated way of producing transactions on the platform, plus a huge number of confirmed inputs to draw from. Because of [change address](https://byteroll.com/change-address) mechanics one cannot just pump transactions out of one single address.

### Sharing the script
Maybe once @portabella has the script working, it could be shared with the 5-10 other "non-team" devs, and then all could fire away at the set time.

# Comment 
[Tony on bitcointalk 2017-02-09:](https://bitcointalk.org/index.php?topic=1608859.msg17789094#msg17789094)
> You know, there is no architectural limit in the DAGs. Regarding the practical limits, I don't buy into this race to Visa tps.  The most pressing issue of crypto is not tps, it is adoption (which we address in the first place).  Tps will come second after the first is solved.


-----
**This "Change Address" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)