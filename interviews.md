<!-- TITLE: Interviews -->
<!-- SUBTITLE: Important Byteball interviews -->

# Video interviews with Tony
### Date: 2016-10-14 Length: 58:16 
**Coin Interview Episode 27: Anton Churyumov of Byteball.org**

[video](https://www.youtube.com/watch?v=zjT7wQNg_s4){.youtube}

### Date: 2018-01-21 Length: 31:05
**Exclusive Interview with the Byteball Founder Tony Churyumoff (Part 1) 🔒 Legends of Crypto**

[video](https://www.youtube.com/watch?v=tB0AUpiF9B4){.youtube}

### Date: 2018-01-28 Length: 31:06
**Exclusive Interview with the Byteball Founder Tony Churyumoff (Part 2) 🔒 Legends of Crypto**

[video](https://www.youtube.com/watch?v=7Fau1bg9-jg){.youtube}

### Date: 2018-01-28 Length: 24:18
**Exclusive Interview with the Byteball Founder Tony Churyumoff (Part 3) 🔒 Legends of Crypto**

[video](https://www.youtube.com/watch?v=ocB5oJdxuuY){.youtube}


# Audio interviews with Tony
**Crypto Talk "made in Germany" - Blockchain, Crypto Themen, ICO/Token Sales**  
[#009 Tony Churyumoff - #Byteball](http://traffic.libsyn.com/force-cdn/highwinds/cryptotalk/Crypto_Talk_Folge_009_mit_Tony_Churyumoff.mp3?dest-id=591992)

# Written interviews with Tony (brief summary with link only)
*Byteball: Exclusive Interview with developer tonych in English*  
[Altcoin Spekulant 2016-10-06](https://altcoinspekulant.com/2016/10/06/byteball-exclusive-interview-with-developer-tonych-in-english/)

*Interview with Anton Churyumov and how blockchain enables trust*  
[HuffPost -- Saeed Ashif Ahmed, Contributor 2017-11-08](https://www.huffingtonpost.com/entry/interview-with-anton-churyumov-and-how-blockchain-enables_us_5a0384eee4b0204d0c171407?ncid=engmodushpmg00000004)

*Interview with Mr. Churyumov and how he thinks about witness and distribution of Byteball*  
[Byteball Japan 2017-11-24](http://byteballjp.info/2017/11/24/interview-with-mr-churyumov-and-how-he-thinks-about-witness-and-distribution-of-byteball)
# Other interviews (brief summary with link only)
*Byteball Interview (Part 1): Crypto of the 3rd generation, Witnesses, IOTA vs. BYTEBALL*

[Techburst -- Interview with @CryptKeeper Part 1](https://techburst.io/byteball-interview-part-1-crypto-of-the-3rd-generation-witnesses-iota-vs-byteball-900b629bff)

*Byteball Interview (Part 2): Technical Strengths, Blackbytes, Distribution Rounds and Cashback System*

[Techburst -- Interview with @CryptKeeper Part 2](https://techburst.io/byteball-interview-part-2-technical-strengths-blackbytes-distribution-rounds-and-cashback-8721b24841ad)