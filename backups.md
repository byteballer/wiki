<!-- TITLE: Backups -->
<!-- SUBTITLE: A quick summary of Backups -->

[Byteball Backups and Recovery](https://github.com/byteball/byteball#byteball-backups-and-recovery) -  github original advice

Byteball uses a single extended private key for all wallets, BIP44 is used for wallet address derivation. There is a BIP39 mnemonic for backing up the wallet key, but it is not enough. Private payments and co-signers of multisig wallets are stored only in the app's data directory, so better use the built-in backup function! Go to hamburger menu > settings > full backup, this will create an encrypted and optionally compressed backup file.

![Backupfunction](/uploads/cryptkeeper/backupfunction.png "Backupfunction")  


**In case you have to backup an old wallet version without backup function, you have to do the backup manually:**

**NB**
1. Stop and exit your ByteBall wallet before copying the directory contents over
2. Store the backup on an external disconnected drive or USB stick
3. Disconnect the backup from your PC afterwards


```text
macOS: 
~/Library/Application Support/byteball

Linux:
~/.config/byteball

Windows:
%LOCALAPPDATA%\byteball
```

### Linux backup script

```text
cd ~
vi byteball_backup.sh
chmod a+x byteball_backup.sh

foldername=$(date +%Y-%m-%d)
mkdir -p  "/more/02 byteball_backups/$foldername"
cp -R /home/mark/.config/byteball/. "/more/02 byteball_backups/$foldername/."
```

### Linux backup script usage

Create a backup target destination folder for backups on *another* drive to your primary drive

EG In my example "/more/02 byteball_backups/"

* Exit your ByteBall wallet
* Drop to your command line terminal
* **./byteball_backup.sh**
* Done
* Restart your ByteBall wallet as required

-----
**This "Backups" page main author: @markcross at the** [Byteball slack](http://slack.byteball.org/)
