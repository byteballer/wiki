<!-- TITLE: Referral -->
<!-- SUBTITLE: Rewards possible from using the *Real name attestation bot* -->
# Real name attestation bot
This [chatbot](https://byteroll.com/chatbot#real-name-attestation-bot) performs simple [identity verification](https://byteroll.com/identity-verification) for use with ICO-issuers etc.

# Attestation reward
Every new user who successfully verifies himself for the first time will receive $20 in Bytes from the distribution fund. With $8 they have to spend for verification, it is net $12. The amount seems large enough to care about but not too large to bother about instadumping.

Attestation is available only for single-address wallets, and to avoid confusion, starting with version 2.1, the default ("Small expenses") wallet of new users is created as single-address (nothing changes for old users). Users can still easily add another wallet and make it multi-address for better privacy. The second wallet is not linked to the user’s verified identity and can be used anonymously. [[Ref 1](#references)]

The attestation reward usually takes 15-30 minutes to be paid.

# Referral reward
By referring a new user, the referrer also receives $20 after the referred verifies himself. The referrer must be also verified at this moment. The referral reward takes a few hours to come through.

# External links
# References
1. https://medium.com/byteball/distribution-to-verified-users-and-referrals-ed00b9b2a30e


-----
**This "Asset" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)