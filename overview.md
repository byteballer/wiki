<!-- TITLE: Overview -->
<!-- SUBTITLE: A one-page summary of Byteball goodies -->

**Byteball**, listed on Coinmarketcap as ''Byteball Bytes'', is a DAG-based cryptocurrency developed by Anton Churyumov.

# Videos
[video](https://www.youtube.com/watch?v=2ZuYZOitKfQ){.youtube}

[video](https://www.youtube.com/watch?v=zOayZ2_6cN4){.youtube}

[video](https://www.youtube.com/watch?v=RT-pA_KHz4U){.youtube}

[video](https://www.youtube.com/watch?v=SnXuylxUKqo){.youtube}

# DAG
Byteball data is stored and ordered using a [directed acyclic graph](https://byteroll.com/directed-acyclic-graph)[[1](references)] (DAG) rather than a blockchain. This allows all users to secure each other's data by referencing earlier data units created by other users, and also removes scalability limits common to blockchains such as the blocksize issue.

There are no blocks: there are only transactions. You just add your transaction to the end of the DAG yourself, without waiting for the miners to (hopefully) include it in some future block.

# Features
## Smart/conditional payments
The killer feature is the Smart/Conditional Payment. You set a condition for how the payee receives the money. If the condition is not met, you get your money back. This substitutes for trust between strangers because neither is able to scam the other. A simple use-case is exchanging one (wallet) currency for another.

### Oracles
An oracle is a trusted third party that monitors specific external events and imports them into the Byteball database as a data feed. An example is a list of cryptocurrency exchange rates updated every ten minutes. Data-feed items from relevant oracles are then called upon when writing smart contracts. For example, making a bet with someone on a chosen sporting event.

## Textcoin
One can send Bytes (Byteball funds) by email or WhatsApp etc, even if the recipient is not in Byteball yet. For email, the sender just writes an email address where he would normally write a Byteball address. When he hits "Send", his email app is opened with pre-filled text for the recipient. The sender can edit it before sending. The recipient receives an email with a link. Example:

> Here is your link to receive 0.001 GB: https://byteball.org/openapp.html#textcoin?pact-volume-lazy-midnight-mix-cool-fiction-symbol-tag-fiction-coral-sibling

## Identity verification for cryptos
Starting 18 January, every Byteball user can link his Byteball address to his real world identity. The user’s personal data is verified by Jumio, the leading provider of identity verification services, and stored in the user’s Byteball wallet. At the same time, a hash of the personal data is stored on the public DAG and signed by a trusted attestor. The attestor also serves as a witness, so it is already trusted.

This attestation allows the user to prove to anybody that his Byteball address is linked to a verified person, without disclosing any personal information. It also allows to reveal the private information to individual service providers on demand, and the service provider can easily verify authenticity of this information using the hash stored on the public DAG.[[2](references)]

## Blackbytes
Private payments can be made using blackbytes, a cash-like untraceable currency. Its transactions are not visible on the public database that shows all payments made with (white)bytes. Blackbytes are sent peer-to-peer instead in an encrypted chat session.

## Chatbots
Chatbots are fun and facilitate real-world transactions, including shopping with a merchant and paying with two clicks. Current chatbots in the wallet Bot Store are: Flight delay insurance, Transition bot, Byte-BTC exchange, Flight delays oracle, Sports oracle, BTC oracle, Rosie bot, Byteball Asset Manager, Zork | game, Poll bot, Blackbytes Exchange BEEB (Trustful^), Blackbyte Exchange [freebe] (Semi-trustless^), Buy blackbytes (trustless), Slice&Dice MUD, Betting bot (Semi-trustless), Luckybytes Lottery (provably fair), TitanCoin ICO, Byteball-Altcoin Exchange Bot, CFD Trading (Trustful).

^Note that in the cryptosphere, "trustless" is generally preferable to "trustful" -- the reverse of what one might expect. And neither word equates to "trusted" or "untrusted".

## Wallet
### Multi-signature, multi-device
Whatever you want, for example 2of4 out of desktop, laptop, cellphone, spouse's cellphone. Set up as many wallets on one device as you wish.

### Encrypted chat
End-to-end encryption between paired wallets, for any chat as well as payments.

### Single-address wallet
You can use single-address wallets to run a manual oracle right from your wallet, without having to run a node on a server. This allows you to run a prediction market for a future event, enable users to make contracts (bets) referencing your address as an oracle, and when the outcome of the event is known you post its result from your wallet.

### Multiple screens
Each asset (Bytes, Blackbytes, Zwibs, Zingos etc) has its own screen showing the balance available and any incoming transactions in that currency. See [Platform section](overview#platform) below.

### Preferences
Change local wallet name anytime, pick a color out of 12, a language out of 18.

### Address info
All wallet addresses are listed, along with their balances in all currencies.

### Seed/Backup
The addresses are generated from an extended private seed, like any HD wallet. Record the seed somewhere safe, and all the non-private addresses and transactions can be recreated in a new wallet. Encrypted backups are easily done, and should be after private (say blackbyte) transactions.

# Platform
Byteball has its native currencies, Bytes and Blackbytes. It is also a **platform** for new [assets](https://wiki.byteball.org/asset) (coins/tokens) you can create yourself at minimal cost in five minutes. You can simply send your asset to anyone with a Byteball wallet, or you can use your asset in many smart contracts.
## ICO
Create your own ICO if you wish. Creating your own asset takes five minutes. An ICO bot is available right now in Github, and a user-friendly one should be available soon.

# Basic info
## Native currencies
* **Bytes:** Total supply = 10^15 bytes. Unit on exchanges is the GBYTE. 1 GB = 1,000 MB = 1,000,000 KB = 1,000,000,000 Bytes.
* **Blackbytes:** Total supply = 2.1111 x 10^15. 1 GBB = 1,000 MBB = 1,000,000 KBB = 1,000,000,000 Blackbytes.

All Bytes and Blackbytes were created at the genesis unit. So far approx 65% have been issued for use.

## Date introduced
* First announced 5 September 2016. [Source](https://bitcointalk.org/index.php?topic=1608859.msg16156239#msg16156239)
* Platform went live on 25 December 2016.

# Zurich presentation
A presentation given by Tony on 2 February 2018, that gives an excellent overview of Byteball.[[3](#references)]

# External links
* https://byteball.org Main website
* https://byteball.org/Byteball.pdf White paper
* https://explorer.byteball.org Transactions explorer
* https://medium.com/@Byteball Medium articles
* https://bitcointalk.org/index.php?topic=1608859.0 Bitcointalk thread
* https://byteball.market/#!/asset/order Byteball Asset Manager (for you to create your own coin on the Byteball platform)
* https://wiki.byteball.org Byteball Wiki
* https://www.reddit.com/r/ByteBall Community Subreddit
* https://byteball.slack.com Community Slack

# References
1. https://en.wikipedia.org/wiki/Directed_acyclic_graph
2. https://medium.com/byteball/bringing-identity-to-crypto-b35964feee8e
3. https://docs.google.com/presentation/d/1dpbE1l4Aj8Te2_i9wjVsW48igZMhdyjKJVcWY-45qd8/edit#slide=id.p5


-----
**This "Asset" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

