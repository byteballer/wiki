<!-- TITLE: Ed -->
<!-- SUBTITLE: This is a page for wiki editors or wiki editing. The system won't allow a regular wiki page with the name "Editors" or "Editing" though. -->
# Editor/editing info
If you look OK to an admin, you can get write access to the wiki. Register an account here then ask in the Slack #helpdesk channel. It helps to have some posting history on Slack.

## Article guidelines
See the Byteroll wiki page [Article guidelines](https://byteroll.com/ed/article-guidelines)

## Talk
Use the #byteball_wiki channel at the external [Byteball Slack](https://byteball.slack.com). That works fine for our purposes.

## Sandbox
This is for experimenting with things rather than using a regular page. The idea is to clean it (apart from the top "sticky" note) at the end of each session so someone else gets a clean sandbox.

See the Byteroll wiki page [Sandbox](https://byteroll.com/ed/sandbox)

## Personal pages
These are for your own notes of things to do, half-written sections/articles not yet ready for a regular page, etc. Create your own page if you want one, but get the path right, namely "/ed/yourname". Maybe use your Slack name, and list it here so other editors can find you.

[Ed/Slackjore](https://byteroll.com/ed/slackjore)

[Ed/markcross](https://byteroll.com/ed/markcross)


# Wiki/article history
As of now, there is no user-friendly page history to see who wrote what when. But there is an available [Bitbucket repo](https://bitbucket.org/byteballer/wiki) for the technically-minded.



-----
**This "Ed" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)


