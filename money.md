<!-- TITLE: Money -->
<!-- SUBTITLE: Simplified basics of money with regard to use in cryptocurrencies -->
# What is money?
![Zim Note](/uploads/slackjore/zim-note.jpg "Zim Note"){.align-center}

Simply stated, money is any item or verifiable record that is generally accepted as payment for goods and services and repayment of debts in a particular country or context. The main functions of money are distinguished as: 

**A medium of exchange**   
An intermediary used in trade to avoid the inconveniences of a pure barter system.

**A unit of account**  
"I'll give you US $50 for that" makes sense because the value of the US dollar is relatively stable. However, when the price of lunch in a restaurant is twice as much at the end of the meal than at the start, that currency is next to worthless as money. All fiat currencies issued by national governments become worthless over time, because they cannot resist the lure of free money made possible by inflating the supply, which results in the devaluation of the currency. $100 was worth a lot more in 1917 than 2017. 

**A store of value**  
Gold and silver have been used as money for literally thousands of years. Gold bullion in a bank vault or an Indian woman's gold jewelry is a familiar store of value, although it makes lousy money for buying a cup of coffee. 

Main ref [Wikipedia: Money](https://en.wikipedia.org/wiki/Money)

## Counterparties
Properly speaking, real money has some intrinsic worth, like gold and silver. All national currencies (Dollar, Euro, Yen etc) are fiat currencies based on debt. All usual paper "assets" -- bonds, shares, pension funds, bank accounts etc -- have counterparties, a debtor on one side and a creditor on the other. If push comes to shove, and the debtor is unable to pay, one's "asset" becomes painfully worthless. 

Does a cryptocurrency have any intrinsic value? One could argue both yes and no, but what a cryptocurrency platform does not have is the risk of a counterparty going belly-up reducing the value of the cryptotokens in your wallet to zero. 
# What is a cryptocurrency?
A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency.

Bitcoin became the first decentralized cryptocurrency in 2009. Since then, numerous cryptocurrencies have been created. These are frequently called *altcoins*, as a blend of *bitcoin alternative*. Bitcoin and its derivatives use decentralized control as opposed to centralized electronic money/centralized banking systems. The decentralized control is related to the use of Bitcoin's blockchain transaction database in the role of a distributed ledger.

Main ref [Wikipedia: Cryptocurrency](https://en.wikipedia.org/wiki/Cryptocurrency)

Crypto payments generally:  
• are sent peer-to-peer, with no intermediary like a bank or credit-card company  
• require no permission from a central authority  
• are irrevocable: once the payment is sent it's sent and that's it.

## How well do cryptos satisfy these three main functions of money?

**A medium of exchange**  
Very few merchants, and very few customers, have adopted *any* cryptocurrency. We are barely into the early-adoption phase.

**A unit of account**  
Generally a crypto's price is very volatile on a day-to-day -- even minute-to-minute -- basis. It is not uncommon for one to go up 100% in a week, then down 20% in an hour. Crypto veterans grow accustomed to this behavior and shrug it off, but it can be unnerving to the new user.

**A store of value**  
Bitcoin and many altcoins have appreciated hundreds or thousands of times in a few years. Some have gone to zero. But it is an asymmetric investment. You invest, say, $1000 in a crypto that looks promising, and there is a distinct chance you may make $100,000; while (usually) the most you can lose is the $1000.

So overall, cryptos currently make lousy money for everyday use but have HUGE potential as speculative asymmetric investments.

# Additional properties of a good currency
**Fungibility**  
The units of the currency are interchangeable. Generally speaking, one $20 bill is as valuable as any other $20 bill; one ounce of gold is as valuable as any other ounce of gold.

**Divisibility**  
The currency exists in sufficient denominations to be useful in everyday trade. This applies to almost all national currencies that aren't hyperinflating. You can buy a pack of chewing gum or a cheap used car in US $ cash.  Gold is useless for buying gum, unless the gum-seller will accept a tiny pinch of gold dust. Well-known US gold coins like the 1-ounce Eagle and 1/10-ounce Eagle have (approximate) values of $1200 and $120 each, so unless the car price is some multiple of those, it won't work. And even if the car's asking price is $1200, the seller probably wouldn't accept an Eagle in exchange.

**Portability**  
You can easily move it around. This varies depending on the amounts involved. It is easy to carry US $100 in your wallet. It is harder to quickly wire $10 million abroad, and very hard to both get hold of and safely transport $10 million in cash. Anytime you cross the Canadian border, you must declare any currency or monetary instruments you have valued at Can $10,000 or more. Carrying $12,000-worth of gold coins in your pocket is easy, as they would weigh about 10 ounces. But $12,000-worth of silver coins would weigh about 500 ounces. 

**Resistance to attack**  
Nation states attack other national currencies all the time. If you devalue your own currency your exports become cheaper, giving you an advantage in international trade. A competitor may be forced to follow suit. But more expensive imports accompany the cheaper exports, driving up the costs of producers and citizens generally. Many countries do it. It’s a race to the bottom.

A local currency is a currency that can be spent in a particular geographical locality at participating organisations. A local currency acts as a complementary currency to a national currency, rather than replacing it, and aims to encourage spending within a local community, especially with locally-owned businesses. Ref [Wikipedia](https://en.wikipedia.org/wiki/Local_currency). These are basically no threat to a national currency, so get largely ignored by the State.

**Easy to recognise**  
Is what you are being offered for your service or good the real thing? Is it a counterfeit that won't be accepted when you try to buy something with it later on? National currencies do pretty well at this, especially ones that have been in use for some time. There may well be counterfeit notes or coins in circulation, but if the fake was good enough to fool you it will probably also fool whoever you pass it on to. Gold and silver coins are much harder to use, because they are very unfamiliar generally and hardly anyone can validate one except for a professional coin dealer.


## How well do cryptos satisfy these additional properties?
**Fungibility**  
Generally, not well. The entire history of the coins making up a bitcoin address is readily available online, at least in terms of earlier associated addresses. What if, unknown to you, the bitcoins you just sent to your grandmother for her birthday were stolen from an orphanage's main (crypto) savings fund last month? This is known as "taint", and it means some coins may be considered less desirable than other coins.

It's all very well to declare that a coin's history doesn't matter -- and in practice, it's impossible to trace the entire history of an address's coins unless close to their creation -- but what if you run an exchange and you discover someone trying to offload $10 million of cryptocoins you *know* were stolen last week?

What if some government decides to mandate that a merchant may only accept a crypto payment from a registered (i.e., linked to a known identity) address? 

Very few cryptos are free of this problem. Byteball's blackbytes are, its (white)bytes aren't.

**Divisibility**  
Bingo! Cryptos tend to excel at this, better than any non-crypto currency. It is as easy to send a crypto payment equivalent of $10 million as $.001.

**Portability**  
Cryptos excel at this too. A paper wallet could hold your entire life savings. Or even a brain wallet.

**Resistance to attack**  
A decentralised cryptocurrency is hard for a State to destroy, especially since it is usually global. Look what happened when the centralised file-sharing innovator Napster got stomped out of existence: peer-to-peer file-sharing software got developed, and now how successful are governments or big media corporations at stamping out the use of bittorrent? Maybe in the future some country will make it illegal for a merchant to accept crypto payments, and the value of the currency will plummet.  Maybe. Hard to say what the future will bring. In the meantime, enjoy the ride. :)

**Easy to recognise**  
Cryptos suffer from the same problem as gold and silver coins: general lack of familiarity. You couldn't pass off a fake bitcoin to someone familiar with how it all works, but you could show a noob one of those Chinese novelty metal "bitcoins" that cost $2 each and he wouldn't know the difference from the real thing.

# Any other advantages to a cryptocurrency?

* Crypto markets seem to be free of State manipulation.  
* Its issuance rate is generally known from the beginning.
* It is not subject to whims of the State such as making worthless 86% of the cash in use (India, 2016) or stealing maybe 20% of bank deposits (Cyprus, 2013). So if you are concerned about your government confiscating your pension fund or bank account savings, or your stock-market investments tanking, cryptos **may** provide a way out.
* Cryptos are programmable money, some more so than others.

# Um, a cryptocurrency is a currency, right?

## Currency vs commodity
The traditional definition of a currency is that it is a medium of exchange and a store of value. The traditional definition of a commodity is that it is a nearly-perfectly fungible good. A commodity could be used as a currency if it is convenient to do so. Likewise, a currency can become a commodity under certain conditions.

## Because taxes
Should cryptos be treated in law as currencies or commodities, each with their own established rules and regulations? Unfortunately it is generally impossible to neatly pigeonhole them as one or the other.

Currently different States are struggling with how to treat cryptocurrencies, with no consensus among them. The IRS treats cryptos as regular commodities subject to capital gains taxes, meaning that you should somehow keep minute track of every satoshi acquired and spent, with the $-equivalent noted at the times of acquisition and disposal.

> For Bitcoins, however, if there is a path to full currency status it is likely in future generations of the technology that address version 1.0’s weaknesses, including anticipating how the government will surely intervene to block it. Until you can whip out your phone and pay in Bitcoins without issuing a 1099 [IRS tax form] and calculating the capital gains tax on the transaction (or maybe there will be an app that will do both?) the potential is limited to largely the specific forms of commerce currently open to them (more so black market or darknet).

Ref [Zerohedge, author unstated](http://www.zerohedge.com/news/2017-05-26/bitcoin-rebounds-2600-greshams-law-looms)

While this is clearly unreal except in broad strokes and will make the average crypto-user guilty of tax evasion even if they are **trying** to do it right, Governments tend to want their pound of flesh wherever they can get it and aren't too likely to merely roll over and give cryptos a free pass.
# Comparison of market sizes
Figures are approximate, and given in US$ millions (equivalent value)

| Market |  Daily global trading volume      |
|-----------------------------|-------------:|
| Bonds      |100,000,000 | 
| Forex      | 5,000,000 | 
| Equities   | 200,000 |
| Gold       | 30,000 |
| Cryptos    | 5,000   |
| Silver     | 2,000   |
| **Byteball**   | 0.2       |


# Finally, food for thought
> *Paper money eventually returns to its intrisic value: zero.* -- Voltaire

![Zim Note 2](/uploads/slackjore/zim-note-2.jpg "Zim Note 2"){.align-right}According to an interesting study of the 775 fiat currencies that have existed 599 are no longer in circulation. The median life expectancy for the defunct currencies? Fifteen years.  Perhaps the author was being unfair by focusing solely on the failures. **Sadly no, the average life expectancy of all fiat currency is running at a truly underwhelming 27 years.** Only a select few have managed anything approaching old age. The British pound sterling is one such example at over 300 years and counting. Before we get too excited by this apparent example of longevity, at inception the pound was defined as 12 ounces of silver.  The pound is now worth less than 0.5% of this original value and of course there is no silver involved anywhere. In other words, **the most successful currency in existence in terms of life-span has lost more than 99% of its value.**

Ref [(Stephen Johnson)](http://historysquared.com/2012/06/26/fiat-currencies-trend-towards-their-intrinsic-value-often-rather-quickly/)


-----
**This "Money" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)



