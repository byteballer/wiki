<!-- TITLE: Wallet -->
<!-- SUBTITLE: Details of the main (top left) menu and the cogwheel (halfway down on the right) menu; smart wallets -->

# Byteball wallets
To start, download and install a wallet from those listed at byteball.org or [Github: releases](https://github.com/byteball/byteball/releases) (includes win32, but not for XP). The wiki [installation](https://byteroll.com/installation) page may help here.

The installation comes with a default wallet, called "Small Expenses Wallet". You can create additional main wallets. A main wallet can also have [smart wallets](https://byteroll.com/wallet#smart-wallets) inside it, created by conditional-payment smart contracts.

## Multiple devices
If you want a connected Byteball wallet on your pc and another one on your Android smartphone, choose the multi-device option [as shown below](https://byteroll.com/wallet#add-wallet). Don't try and build the second one from the seed of the first: it will probably not function as expected, especially regarding blackbytes.

### Independent or connected?
**Independent:** Useful for experimenting, seeing what happens when you interact with another Byteball user in chat. Use (plain) default Small Expenses Wallet on each device.

**Connected:** Useful for security. Will show the same balance and history across all devices.

### Pairing
Pairing is between devices, independent of whatever wallets are on those devices. So if you have a 1of2 wallet shared by your pc and your smartphone, and you pair the pc and chat with a friend, the chat will not appear on your smartphone, although the funds will.

## Light/easy or full/hard wallet?
Always select the light option unless you REALLY need the full version. If you have funds sent to an address in a full wallet you will be unable to spend them until it finishes synchronizing, and this can take a very long time. The bottleneck seems to be the read/write speed to the hard drive, with an SSD being preferred by far. Ref [bitcointalk thread post #10018](https://bitcointalk.org/index.php?topic=1608859.10000) (scroll down)

## Single-address wallet
A single-address wallet (version 1.11.0 on) you create will not generate the usual new [change address](https://byteroll.com/change-address) when needed. Change will always go to the one-and-only address the wallet contains.

You can use single-address wallets to run a manual oracle right from your wallet, without having to run a node on a server.  This allows you to run a [prediction market](https://byteroll.com/trading/trading-prediction-markets) for a future event, enable users to make contracts (bets) referencing your address as an oracle, and when the outcome of the event is known you post its result from your wallet.

In this version you can also manually attest other users and post arbitrary data into the Byteball DAG.

# Multiple screens
The default wallet is named "Small Expenses Wallet", and has two screens: one for bytes, one for blackbytes. Every [new asset](https://byteroll.com/asset) you receive (Zwibs, Scamcups etc) will create an additional screen just for that asset. If you add a wallet to the default set-up -- maybe "Savings" -- it will start out with two screens again.
# Update
When doing a routine update, first do a full back-up using the Byteball menu option (see below) "just in case", then simply download the latest version and install it over the top of the existing version. So when you get a prompt saying "you already have [this], do you want to replace it?" click yes. 
 # Main menu options
 ## Add wallet
Choose between PLAIN WALLET and MULTI-DEVICE WALLET, and give it a name. If multi-device (m of n) wallet, choose the total number of co-signers (n), and the required number of signatures (m). For each co-signer (not you), you need to select from the list of paired devices in your library. If the device is not there yet, share a pairing code with it and then select it.

A multi-device wallet is duplicated across each of the paired devices, showing the same transactions, addresses, balances etc.

### Signatures
* A 1-of-2 set-up means two devices and either one is sufficient to send a transaction.  
* A 2-of-2 set-up means two devices and both signatures are needed to send a transaction.  
* A 2-of-4 set-up means four devices and two signatures are needed to send a transaction.
 ## Paired devices
 ### Contacts
This shows a list of all your paired devices, both the user-chosen name (*myputer* etc) 
and the permanent device number (0VC...ARM etc). Some you will be able to remove, some you won't. However, if you enter the chat window for that device, there's an Edit button at the top right. One of the options is *rename*, so at least you can rename "AnnoyingDevice" to "zzAnnoyingDevice" and it will move to the end of your alphabetical list and be out of the way.

### Bot store
Currently shows 15 bots: Flight Delay Insurance, Transition Bot, Byte-BTC exchange, Flight delays oracle, Sports oracle, BTC oracle, Bounce bot, Rosie bot, Byteball Asset Manager, Zork | game, Poll bot, three blackbyte trading bots, and Slice&Dice MUD (game). See wiki article [Chatbot](https://byteroll.com/chatbot) for details.

## Settings = Global preferences
 **Device name:** Change it if you wish. If you chat to someone, they will see this name
 
 **Hub:** Default is byteball.org/bb. You can change it
 
 **TOR:** This option is desktop/laptop only. For Android, use the Orbot app
 
 **Language:** Select from 18 or so
 
 **Unit for bytes:** Select from kB, MB or GB
 
 **Unit for blackbytes:** Select from kBB, MBB or GBB
 
 **Enable push notifications:** (Android) toggle on or off
 
 **Witnesses:** Easiest to select "Auto-update the witness list from the hub". See wiki [Witness](https://byteroll.com/witness) article for harder options
 
 **DEVICE ADDRESS:** 0VCPO8MYDRN2E3N5JDRHRSTVEVZSZYARM for example. This doesn't change, and will be visible to anyone you pair with for chat
 
**SPENDING RESTRICTIONS**  
**Request password:** Lets you set up a password that will encrypt your wallet. Don't forget it! There is no password recovery option

**Backup wallet seed:** The seed alone will back up your bytes but not your blackbytes

**Recovery from seed:** Recovery is available in single-sig (i.e., not multi-sig) light wallets from version 1.10.1. 

**Full backup:** Lets you set a password (don't forget it!) and export a full backup file

**Restore from full backup:** Note this will permanently delete all your existing wallets and replace them with whatever is in your backup file. If you created this backup on another device, you should have stopped using the original device wallet immediately after creating the backup. Never clone wallets. If you must access from several devices, use multisig.

**About Byteball:** Version number, commit hash (developer thingy), terms of use, translators credits, session log
 # Cogwheel preferences
 **Wallet alias:** change local name
 
 **Color:** 12 to choose from
 
 **Advanced:** (under next heading)
 ## Wallet information
 **Wallet name (at creation):** whatever name you gave it first
 
 **Wallet ID:** note this does not change, and is different to all the wallet addresses. If this is a shared multi-sig wallet, the ID will be the same on each of the paired devices
 
 **Wallet configuration (m-n):** multiple-signature configuration, like 1 of 1, 1 of 2, 2 of 2, or 2 of 3 etc
 
 **Derivation strategy:** BIP44: standard method of deriving new keys for addresses, ref [Bitcoin Improvement Proposal 44](https://en.bitcoin.it/wiki/BIP_0044)
 
 **Account (BIP44):** BIP44 allows the creation of more than one account. If you create a second multi-sig account with a paired device it will show as #2
 
 **CO-SIGNERS**
 
 Shows the co-signers (if any) for this wallet configuration, and which wallet is currently in use. Note the original wallet names are used, not what they may have been changed to
 
 **EXTENDED PUBLIC KEYS:** 
 * **Me** xpub... (this wallet's xpub)  
 * (Other wallet's name, if multi-sig) xpub... (that wallet's xpub)
 
 **ALL WALLET ADDRESSES:** (examples shown)
 
65C...AW6  
m/44'/0'/1'/0/0 · April 19th 2017, 3:00 pm

7SV...MMM  
m/44'/0'/1'/0/1 · April 22nd 2017, 5:47 pm

MR3...APD  
m/44'/0'/1'/1/0 · April 26th 2017, 1:34 pm

The m/44'/0'/1'/... designation is part of the BIP44 protocol. Here, the 1' means the second wallet -- the first (small expenses) wallet is designated 0'. The final 0/0 (0/1, 0/2 etc) means the 1st (2nd, 3rd etc) visibly-generated receive addresses; and the final 1/0 (1/1, 1/2 etc) means the 1st (2nd, 3rd etc) automatically-generated [change addresses](https://byteroll.com/change-address).

**BALANCE BY ADDRESS:** (examples shown)
 
7SV...MMM  
0.04 GBB

O3J...YL5  
0.024402 MB

RAZ...LIM  
0.05966 MB  
55 Zwib by Jore Bohne (new asset, registered)  
576 of 3kc7H8A2oiWr8mv7AcWJeLCA0Cp8c3BLK04kYQ+5pfU= (new asset, unregistered)  
0.085248 GBB

UQX...EC6  
0.117889 MB
 
 ## Sweep paper wallet
 * This gives an option to scan QR code of paper wallet private key  
 * If anyone sees a byteball paper wallet generator, please tell me (Slackjore)
 ## Delete wallet
 What it says, big red button on the next screen
 
 # Smart wallets
 When you transact using a smart contract, the funds may go into a smart wallet. One or both parties will be able to unlock this and spend the funds.
 
 ## Confirmed funds
 You need transactions to confirm before  
 * A smart wallet first becomes visible  
 * You can spend funds from it
 
 ## Transaction fees are paid in bytes
 If you are in a new smart wallet and trying to send blackbytes, or some [new asset](https://byteroll.com/asset) like Zwibs, it won't have any regular bytes in it to cover the transaction fee. So open the Receive tab, copy the address, paste it into your regular wallet and send some bytes, maybe .1 MB. You can easily recover any unspent bytes from this.
 
 ### Zero out the smart wallet
 Lots of little smart wallets can get confusing. So send all funds, in all currencies, to your regular wallet(s). Send the (white)bytes last, or you won't be able to pay the transaction fees.
 
 When all balances in a wallet are zero, it will disappear (hooray!).
 
 If you cannot spend the funds, maybe they are locked because the other party needs to spend them. 

# Backup
Full backup is WAY more important than a seed. Full backup keeps your blackbytes, and even better, it means a simple and quick full restore, whereas a restore from seed requires a full node.

# Convert
There's no simple way to change from a full wallet to a light one. Usually the need becomes apparent when one has sent GBytes from an exchange to a full wallet and the funds don't show in the wallet because it hasn't sync'd up to the date/time of the transaction. 

In this case, follow these steps to get access to your bytes (not blackbytes):

1. On a separate device, download and install a light wallet
2. Find someone you trust that has a fully-sync'd full wallet that has zero funds in it. Send that person your seed. They will then restore your wallet on their computer
3. That person then sends your funds to your new light wallet
4. Continue to use the light wallet

To get access to your blackbytes, either  
* Wait until the full wallet syncs, then send the blackbytes to the light wallet (after pairing); or
* Send that nice person you trusted with your seed the "User data" data directory (see below), and he can then send you the blackbytes (after pairing).

User data directory in Windows, for example, is found at C:\Users\Alice\AppData\Local\byteball\User Data

# Reinstall wallet
To start from scratch, you have to rename or delete the Byteball user data folder before reinstalling. **Be aware that deleting the user data folder results in any existing coins getting lost**. After you've renamed or deleted this folder, you can do a re-install and choose the wallet type (light/full) again.

## Rename or delete this folder before reinstall

* Windows: %LOCALAPPDATA%\byteball

* MacOS: ~/Library/Application Support/byteball

* Linux: ~/.config/byteball



## Changing operating systems
It is apparently possible to make a full backup on Windows, and then restore it on Linux. Everything reportedly gets copied including the device address.

### no show
# Wallet and explorer differ
Does explorer.byteball.org show some new bytes have arrived, but they don't show in your wallet? 

**Wallet not connected properly?** Your internet connection needs to allow communications to get to and from the wallet: an easy check is do the chatbots work? If the chatbots don't work, maybe showing a Socket Closed error, this lack of proper connection must get fixed.  
* Firewall problem?  
* Tor turned on by mistake? (**Check** this at Settings > TOR, don't just assume it isn't)
* Something else wrong?

**Wallet not sync'd?** If have a full wallet it needs to sync up to the date of a transaction to show it. Then, transfer your coins to a light wallet and when you're sure the coins are in the light wallet dump the full wallet.

**Weird wallet?** If you have done something weird like "copied" a wallet from one device to another, or are running more than one instance on a single computer, then you may or may not be able to access these coins. This includes trying to use a testnet wallet instead of a regular one.

**Latest version?** You **are** running the latest version, right?

# Sync problems
## Android
If your phone is taking too long to sync, even with a light wallet, it could be due to blackbytes, and because when the screen fades out it often stops syncing. Activate Developer Mode on Android to get under Settings a new option to keep the screen awake/on when plugged in, and this allows you to sync overnight.


## Full wallet may be better 
If you want to do many blackbyte transactions, a light wallet is less efficient than a full wallet to sync blackbytes. So in this case you are better off with a full wallet and thus must have an SSD drive on your laptop/desktop.


-----

 **This "Wallet" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)