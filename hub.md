<!-- TITLE: Hub -->
<!-- SUBTITLE: A general summary, not too technical -->
# Hub for Byteball network
This is a [node](https://byteroll.com/node) for the Byteball network that serves as a relay, plus it facilitates the exchange of messages among devices connected to the Byteball network. Since all messages are encrypted with the recipient's key, the hub cannot read them. The hub does not hold any private keys and cannot send payments itself. 

The messages are used for the following purposes:

## Private payment information
Conveying private payment information from payer to payee.

## Multisig adddress
Exchanging partially-signed transactions when sending from a multisig address. One of the devices initiates a transaction and signs it with its private key, then it sends the partially-signed transaction to the other devices that participate on the multisig address. The user(s) confirm the transaction on the other devices, they sign and return the signatures to the initiator.

## Multilateral signing
Multilateral signing, when several addresses sign the same unit, e.g. when exchanging one asset for another, or when signing a contract. The exchange of messages is similar to the multisig scenario above.

## Chat between users
Plain text chat between users; in particular, users can send each other the newly generated addresses to receive payments to.

## Chat with bots
Plain text chat with bots that offer a service and can receive or send payments. *Faucet* is an example of such a bot. The hub helps deliver such messages when the recipient is temporarily offline or is behind NAT. If the recipient is connected, the message is delivered immediately, otherwise it is stored and delivered as soon as the recipient connects to the hub. As soon as delivered, the message is deleted from the hub.

# Hub address
Users set their hub address in their wallet settings. The default hub is wss://byteball.org/bb but users can change it.

## List of hubs
Wallet user: to use one of these hubs in the wallet, change it in main menu > settings > hub.

| Hub | Notes |
|-----------|-----------|
| ```byteball.org/bb``` | default hub, operated by @tonych |
| ```byteroll.com/bb``` | for EU, operated by @portabella |
| ```e.byteroll.com/bb``` | for North America, operated by @portabella |
| ```s.byteroll.com/bb``` | for Singapore/South Asia, operated by @portabella  |
| ```byteball.me/bb```| in S. Europe, operated by @zuqka |
| ```blackbytes.me/bb```| in S. Europe, operated by @zuqka |
| ```byteball.zuqka.info/bb```| in S. Europe, operated by @zuqka |
| ```byteball.fr/bb``` | operated with a partner by @seb486 |
| ```east.byteball.us/bb``` | operated with a partner by @seb486 |
| ```west.byteball.us/bb``` | operated with a partner by @seb486 |
| ```byteball.wang/bb``` | operated with a partner by @seb486 |
| ```bb.coinium.ch/bb``` | based in Switzerland, operated by @rootlogin |

Also ref [world map](https://byteball.fr/byteballworldmap.php) showing hubs 

-----
**This (copy/paste) page main author: @tonych at the** [Byteball slack](http://slack.byteball.org/)  
**List of hubs added by @slackjore**
