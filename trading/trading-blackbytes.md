<!-- TITLE: Trading blackbytes -->
<!-- SUBTITLE: How to buy or sell blackbytes in the wallet using conditional payments -->
# Definitions
See the [glossary](https://byteroll.com/glossary) if needed for definitions of "blackbytes" etc.

# Bots
See https://byteroll.com/blackbytes#bots for info on three blackbytes trading bots.
# Peer-to-peer actions to do
## The Slack #trading_blackbyte channel
Post "otc". This will automatically show the available bot commands. Post "asks" and "bids" to show the available offers and bids. Let's say you want to buy 40 GBB for 1GB. Post "bid 1 40". Assuming you used the correct format, the bookbot will respond "OK". Then post "bids" to check that your bid is actually what you wanted to post. When you want to remove your bid, post "remove bid" and if you did that correctly the bot will comply and post "OK". Similarly for asks.

You need to find someone willing to sell/buy at your price. You might need to come down a bit. You might need to haggle. You can try DMing possible traders listed in the book. Some of the entries are outdated.

0. Buyer and seller agree on amounts using some messaging channel such as Slack, email, even face to face.

## After agreeing the terms

1.  One party gives the other a pairing code, either written out in full or via QR code. Generate this in byteball wallet chat by clicking on "+ Add a new device" then "Invite the other device". It will look something like 
AhHPXCpCSTzD1CF53ELGTAsZ6MCA8Ogvk+koyibfPt/2v@byteball.org/bb#GxVBizZ2DSEf. The other party in byteball wallet chat clicks on "+ Add a new device" then "Accept invitation from the other device" and pastes the pairing code into the almost-invisible line above the 'pair' button.

2. Say hi etc to establish communication with the other party in the byteball chat window. If needed, change the units in Settings to be close to your trade quantities. In other words, if you're going to send 40 MB, don't input it as .04 GB or 40000 KB. In the heat of the moment it's easy to miscount the number of zeros.

**WARNING: DO THE NEXT STEPS EXACTLY! DON'T SEND AN ADDRESS TO BE CLICKED ON! DON'T DEVIATE FROM STEPS 3, 4, 5 IN SEQUENCE OR YOU'LL SCREW IT UP! THIS WARNING IS IN BOLD CAPS -- HEED IT!**

3. **Buyer of GBB sends payment request** (click on ". . ." to the left of the message text field) to seller, say for 40 GBB.  
-->Chat shows **payment request** from buyer for 40 GBB

4. Seller clicks on the payment request for 40 GBB and sends 40 GBB to smart wallet, binding it to the condition of receiving a payment of 1.0 GB (for example). **Make sure you bind it to the condition**, not simply send it without binding.  
-->Chat shows **payment request** from seller for 1.0 GB  
-->Chat shows **payment** from seller of 40 GBB

5. Buyer clicks on the payment request for 1.0 GB and sends payment for 1.0 GB.  
-->Chat shows **payment** from buyer of 1 GB

6. Done deal. Wait for confirms, and if both parties happy end off the chat. Now go to the smart wallet, and send the funds to your main wallet.

# Notes

1. The new smart wallet won't show up until the payments confirm after 5 minutes or so.

2. If you just bought blackbytes then the smart wallet won’t have any (white)bytes in it yet to cover the sending fee. Click the Receive tab, copy the address, then paste it into the Send screen of your regular bytes wallet.

3. You can see the details of the smart (wallet) contract by clicking the little eyeball to the right of the smart contract home screen.

4. Be careful to make payments from the correct wallet -- maybe you were looking in a smart wallet and forgot to get back into your main wallet.

5. You may get an error message if the funds you are trying to send haven't confirmed yet.

6. It is best not to send other payments in the 5-20 minutes while funding and waiting for execution of a smart contract. Additional payment notifications in the chat, and pending confirmations in the wallet home screen, can be confusing.

-----
**This "Trading blackbytes" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

