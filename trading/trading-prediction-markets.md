<!-- TITLE: Trading prediction markets -->
<!-- SUBTITLE: Prediction markets are exchange-traded markets created for the purpose of trading the outcome of events. This article concerns using the Byteball platform to do this. -->

# Definition of "prediction market"
Prediction markets (also known as predictive markets, information markets, decision markets, idea futures, event derivatives, or virtual markets) are [usually] exchange-traded markets created for the purpose of trading the outcome of events. The market prices can indicate what the crowd thinks the probability of the event is. A prediction market contract trades between 0 and 100%. It is a binary option that will expire at the price of 0 or 100%.

Research has suggested that prediction markets are at least as accurate as other institutions predicting the same events with a similar pool of participants.

Ref [Wikipedia](https://en.wikipedia.org/wiki/Prediction_market)

# The Slack #prediction_markets channel

The purpose of the [Slack](https://byteball.slack.com) #prediction_markets channel
is "Finding a counterpart for bets on future events". So post in there to find someone to bet with.

## Current available markets

### Sports-betting on upcoming soccer matches

See the wiki [sports betting](http://byteroll.com/trading/sports-betting) article for details

### Gambling on random numbers

See the wiki [gambling](http://byteroll.com/trading/gambling) article for details, including a video

### Betting on crypto exchange rates

Crypto exchange rates oracle:
JPQKPRI5FMTQRJF4ZZMYZYDQVRD55OTC

See wiki [oracle](https://byteroll.com/oracle) article for a step-by-step write-up  
Ref [Making P2P Great Again, Episode III: Prediction Markets](https://medium.com/byteball/making-p2p-great-again-episode-iii-prediction-markets-f40d49c0abab)

-----

Price oracle was extended (29 August) and now also posts total market cap of all crypto coins and BTC dominance percentage (as reported by coinmarketcap).

The new data feed names for smart contracts are:  
TOTAL_CAP - total market cap in billions  
BTC_PERCENTAGE - BTC percentage of total market cap

-----
**BinaryBalls**  

There is now (November) a commercial site, [BinaryBalls](https://binaryballs.com), that does binary options trading with Byteball bytes, using this oracle as a trustless referee.

### Betting on flight delays
Flight delay tracker for flight delays insurance  
GFK3RDAPQLLNCMQEVGGD2KCPZTLSG3HN  
Ref [Making P2P Great Again, Episode IV: P2P Insurance](https://medium.com/byteball/making-p2p-great-again-episode-iv-p2p-insurance-cbbd1e59d527)

There is a video of betting on a flight delay on the [oracle](https://byteroll.com/oracle) page.

-----


**This "Trading prediction markets" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)