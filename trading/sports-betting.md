<!-- TITLE: Sports betting -->
<!-- SUBTITLE: Make bets peer-to-peer or through the betting bot -->

# Article incomplete. See bot for latest details
## UFC added 2017-11-21

![Sports Oracle Many Sports 400 W](/uploads/slackjore/sports-oracle-many-sports-400-w.png "Sports Oracle Many Sports 400 W")

Thanks to contributions from @neversaynever, our sports oracle got a lot better.

Now it also supports MLB, NBA, NFL, and NHL.  You can browse the past and upcoming games and find the feed name for entering into your p2p betting contract.  And with the new contract forms, it takes very little typing to make a contract.  After the outcome of the requested game is known, you'll get notified by the oracle.  Try it.
# Major soccer matches
The data is sourced from football-data.org, [code](http://football-data.org/v1/competitions). 

The Sports Oracle in the wallet bot store has a "coming" feature. After opening a wallet chat window with it, send "coming" (without the quote marks). After a moment the oracle should respond with a list of matches in the next 7 days. Example from 2017-07-26:

> Next fixtures coming:
> 
> Chamois Niortais vs. Ajaccio: CHAMOISNIORTAIS_AJACCIO_2017-07-28
> 
> Valenciennes vs. Gazélec Ajaccio: VALENCIENNES_GAZÉLECAJACCIO_2017-07-28
> 
> . . . 

Select one of these matches for the bet.

## Before the end of the game
1. Using any communication channel, the punter/backer and "bookie"/layer agree the bet. Use one of the "coming" matches from the oracle's current list
2. In wallet chat, the backer sends the layer his receive address  
3. The layer left-clicks on it, clicks "offer a contract", carefully composes the contract, pays his stake and sends the contract    
4. The backer carefully checks the contract details are correct
5. If all OK, the backer then agrees the terms and pays his stake by clicking SEND 
6. The contract is now locked and the bet is irrevocable. 

## Guide for composing the contract
It is best to copy/paste both the oracle address and the data-feed item in order to avoid transcription errors. The contract will not accept an invalid oracle address, although it will accept an invalid data-feed item.

1. **ORACLE ADDRESS** is TKT4UESIKTTRALRRLWS4SENSTJX6ODCW  
2. **DATA-FEED NAME** is in the format HOMETEAM_AWAYTEAM_YYYY-MM-DD  
3. Remove common abbreviations from the team names such as FC, AS, etc  
4. Write team names in upper case  
5. Remove spaces from multi-word team names.  Manchester United becomes MANCHESTERUNITED  
6. Date of the match is in YYYY-MM-DD format  
7. **POSTED VALUE** is the name of the winning team formatted according to the same rules, or DRAW

## After the game
![Sports Betting 400 W](/uploads/slackjore/sports-betting-400-w.png "Sports Betting 400 W"){.align-right}
After the result is known, each peer can chat independently with the sports [oracle](https://byteroll.com/oracle) that will post the result of the game. This [Byteball link](byteball:Ar1O7dGgkkcABYNAbShlY2Pbx6LmUzoyRh6F14vM0vTZ@byteball.org/bb#0000) will open up the sports oracle chatbot in your wallet automatically even if it is closed -- try it right now! As an example, the Arsenal-Sunderland match on 16 May ~~will~~ [did] work. If the match isn't already in the database, it may take five minutes or more to get added.

You don't need to tell the bot the date of the match as it will find the most recent game between the two teams within the last 7 days.  When talking with the bot, type the names of the teams as they are commonly known (without FC, AS, etc).

The leagues covered by the oracle are:
* 444: **Campeonato Brasileiro da Série A** BSA (Brazil)  
* 445: **Premier League 2017/18** PL (England)  
* 446: **Championship 2017/18** ELC (England)  
* 447: **League One 2017/18** EL1 (England)  
* 448: **League Two 2017/18** EL2 (England)  
* 449: **Eredivisie 2017/18** DED (Netherlands)  
* 450: **Ligue 1 2017/18** FL1 (France)  
* 451: **Ligue 2 2017/18** FL2 (France)
* 452: **1. Bundesliga 2017/18** BL1 (Germany)  
* 453: **2. Bundesliga 2017/18** BL2 (Germany)  
* 455: **Primera Division 2017** PD (Spain)

The backer can unlock the contract if his prediction was correct.  Otherwise, the layer can unlock the contract after the contract expires. If something goes wrong, like the data-feed item is wrong or the oracle fails, the layer can always unlock the contract after it expires, fairly or not.

## Example
Here is a storage unit from the oracle showing a data-feed item from the first league listed (Brazil) [ATLÉTICOGOIANIENSE_ATLÉTICOMINEIRO_2017-07-16: ATLÉTICOMINEIRO](https://explorer.byteball.org/#rRlfUTtoOYH33bWkZ29CPLSQzMK2RoPafAEP0iTtWok=) 

## Source and screenshots

[Bitcointalk article 2017-05-20](https://bitcointalk.org/index.php?topic=1608859.msg19101028#msg19101028) 
# Verification lists
If you are being super-cautious, carefully copy the data-feed item from the actual received contract, and paste/search (Ctrl-F etc) for it here on this page. If **your exact search item** does not appear as your electronic search result, even though by eyeball it seems to be here, then the "bookie" who wrote the contract will win the bet by default, whatever the actual result of the match.

##  Soccer oracle list (editing incomplete for 2017/8 season)
**444 Campeonato Brasileiro da Série A**  
ATLÉTICOGOIANIENSE ATLÉTICOMINEIRO ATLÉTICOPARANAENSE AVAI BAHIA BOTAFOGO CHAPECOENSE CORINTHIANS CORITIBA CRUZEIRO FLAMENGO FLUMINENSE GRÉMIO PALMEIRAS PONTEPRETA SANTOS SAOPAULO SPORTRECIFE VASCODAGAMA VITÓRIA

**445 Premier League 2017/18** 1st matches 12 August  
ARSENAL BOURNEMOUTH BRIGHTONANDHOVEALBION BURNLEY CHELSEA CRYSTALPALACE EVERTON HUDDERSFIELDTOWN LEICESTERCITY LIVERPOOL MANCHESTERCITY MANCHESTERUNITED NEWCASTLEUNITED SOUTHAMPTON STOKECITY SWANSEACITY TOTTENHAMHOTSPUR WATFORD WESTBROMWICHALBION WESTHAMUNITED

**446 Championship 2017/18** 1st matches 4 August  
ASTONVILLA BARNSLEY BIRMINGHAMCITY BOLTONWANDERERS BRENTFORD BRISTOLCITY BURTONALBION CARDIFFCITY DERBYCOUNTY FULHAM HULLCITY IPSWICHTOWN LEEDSUNITED MIDDLESBROUGH MILLWALL NORWICHCITY NOTTINGHAMFOREST PRESTONNORTHEND QUEENSPARKRANGERS READING SHEFFIELDUNITED SHEFFIELDWEDNESDAY SUNDERLAND WOLVERHAMPTONWANDERERS

**447 League One 2017/18** 1st matches 5 August  
AFCWIMBLEDON BLACKBURNROVERS BLACKPOOL BRADFORDCITY BRISTOLROVERS BURY CHARLTONATHLETIC DONCASTERROVERS FLEETWOODTOWN GILLINGHAM MILTONKEYNESDONS NORTHAMPTONTOWN OLDHAMATHLETIC OXFORDUNITED PETERBOROUGHUNITED PLYMOUTHARGYLE PORTSMOUTH ROCHDALE ROTHERHAMUNITED SCUNTHORPEUNITED SHREWSBURYTOWN SOUTHENDUNITED WALSALL WIGANATHLETIC

**448 League Two 2017/18** 1st matches 5 August  
ACCRINGTONSTANLEY BARNET CAMBRIDGEUNITED CARLISLEUNITED CHELTENHAMTOWN CHESTERFIELD COLCHESTERUNITED COVENTRYCITY CRAWLEYTOWN CREWEALEXANDRA EXETERCITY FORESTGREENROVERS GRIMSBYTOWN LINCOLNCITY LUTONTOWN MANSFIELDTOWN MORECAMBE NEWPORTCOUNTY NOTTSCOUNTY PORTVALE STEVENAGE SWINDONTOWN WYCOMBEWANDERERS YEOVILTOWN


**449 Eredivisie 2017/18**


**450 Ligue 1 2017/18**


**451 Ligue 2 2017/18**


**452 1. Bundesliga 2017/18**


**453 2. Bundesliga 2017/18**


**455 Primera Division 2017** 1st matches 19 August  
ALAVÉS ATHLETICBILBAO ATLÉTICOMADRID BARCELONA CELTAVIGO DEPORTIVOLACORUÑA EIBAR ESPANYOL GETAFE GIRONA LASPALMAS LEGANÉS LEVANTE MÁLAGA REALBETIS REALMADRID REALSOCIEDAD SEVILLA VALENCIA VILLARREAL


-----
**This "Sports betting" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

