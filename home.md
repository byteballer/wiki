![Logo 48 Px](/uploads/logo-48-px.png "Logo 48 Px"){.pagelogo}<!-- TITLE: Byteball -->
<!-- SUBTITLE: Smart payments made simple -->
**Welcome to the world of Byteball. We hope you find what you are looking for. [Here](https://byteroll.com/all) are listed all the articles in this wiki. Any further questions, ask on our Slack channel. We will update the wiki as needed.**

Video in Hi-Def and with English subtitles.

[video](https://vimeo.com/221317149){.vimeo}

## Wiki purpose
This wiki contains **general** information, useful for people somewhat familiar with cryptocurrencies who want to become better informed about the Byteball platform. It is not a place for professionals to ensure their obscure speciality is properly explained.

## The *Byteball* brand
In the cryptocurrency field, *Byteball* functions as a brand name, as do *Bitcoin*, *Ripple*, *Ethereum*, *Dash* and all the 500+ others. The space is becoming increasingly commoditized, and as Bitcoin loses its dominance a crypto brand needs a unique selling point in order to stand out from the crowd. The Byteball USP at this point in time is its conditional-payments feature. No single crypto brand is going to be all things to all people, for buying a coffee, buying a house, immigrant workers sending money back home weekly, a store of value just in case, a simple contract, a complex contract, validating real estate ownership, and lots more. A crypto must select its best niche market and concentrate on that.  
Ref [Wikipedia article](https://en.wikipedia.org/wiki/Unique_selling_proposition)

## Byteball features
![Bind 400](/uploads/slackjore/bind-400.png "Bind 400"){.align-right}• The **killer feature** is the Smart/Conditional Payment. You set a condition for how the payee receives the money. If the condition is not met, you get your money back. This substitutes for trust between strangers because neither is able to scam the other. This smart-contract feature has many real-world peer-to-peer applications, including:
* no-fee crypto exchanges
* sports betting
* selling or buying insurance concerning negative events like a flight delay.

• **Textcoin**. One can send Bytes (Byteball funds) by email, even if the recipient is not in Byteball yet. The sender just writes an email address where he would normally write a Byteball address. When he hits "Send", his email app is opened with pre-filled text for the recipient. The sender can edit it before sending. The recipient receives an email with a link. Example:

Here is your link to receive 0.001 GB: https://byteball.org/openapp.html#textcoin?pact-volume-lazy-midnight-mix-cool-fiction-symbol-tag-fiction-coral-sibling

• **Private payments** can be made using blackbytes, a cash-like untraceable currency. Its transactions are not visible on the public database that shows all payments made with (white)bytes. Blackbytes are sent peer-to-peer instead in an encrypted chat session.

• **Chatbots** are fun and faciliate real-world transactions, including shopping with a merchant and paying with two clicks.

• There are the **usual features** like transactions cryptographically linked to each other in a decentralized, immutable (unchangeable), blockexplorer-like record; irrevocable, unstoppable payments with no third-party involved; storing savings untouchably outside the system; multi-signature security; yada yada yada. Tiny transaction fees though. Oh yes, no blockchain or miners. :)

## Most-read articles
[Airdrop](https://byteroll.com/airdrop): distribution information  
[Change address](https://byteroll.com/change-address): why your btc or bytes address may show 0  
[Wallet](https://byteroll.com/wallet): wallet details, menu items, etc  
[Trading blackbytes](https://byteroll.com/trading/trading-blackbytes): where and how to buy/sell blackbytes  
[Glossary](https://byteroll.com/glossary): common terms, with links to more information 

## Navigation
[All Pages](https://byteroll.com/all) will show all the top level articles/pages
[All Pages](https://byteroll.com/all) will also show directories, pages that include second-level articles that won't show up like the top-level ones. So the [Trading](https://byteroll.com/trading) page/directory includes links to  
[Sports betting](https://byteroll.com/trading/sports-betting),
[Trading blackbytes](https://byteroll.com/trading/trading-blackbytes), and 
[Trading prediction markets](https://byteroll.com/trading/trading-prediction-markets).

The Search box at the top works somewhat. Or you can always try a Google search, including the site with your search term(s). So if you're looking for articles here containing the word "witness", then search in Google with
> site:byteroll.com witness

## Things you can do to help
**• Huh? Help? What do you mean?** A wiki has content that is user-generated. It is a work in progress being created by volunteers, not a commercial site put together by the (usually paid) webmaster.

**• If it's a relatively small edit:** Post something in the Slack #byteball-wiki channel (see link below).

**• Otherwise, register an account:** If you need help, post something in the Slack #byteball-wiki or #helpdesk channel (see link below).

**• Read the editor guidelines page:** [Article guidelines](https://byteroll.com/ed/article-guidelines)

**• Learn to edit by mimicry:** You can start with the [Sandbox](https://byteroll.com/ed/sandbox) page. Click the Edit button in the top right. Type something, then click the "save changes" button and see the result. Try out adding a heading, or bold type, maybe. Just copy someone else's code and change the text to your own. 

**• Improve an existing article:** It's OK to improve an article that someone else wrote initially. You can get your feet wet by correcting tyypos and up tidying the grammars. Not everyone has English as a native language. Some articles need expanding too, with existing sections expanded and/or new sections added.

**• Write a new article:** See wanted articles list below.

 ### Articles needing attention 
 * [Error](https://byteroll.com/error) Add error messages (tell @slackjore please)  
 * [Blackbytes](https://byteroll.com/blackbytes) Up tidy the grammars  
 * [Trading](https://byteroll.com/trading) Section on external exchanges needs details re any exchange you have personally used
 * [Installation](https://byteroll.com/installation) Mac installation tutorials
 * [Market capitalization](https://byteroll.com/market-capitalization) Add more token examples
  
 ### Wanted articles

 * Got any ideas?

## External links
[Main official site](https://byteball.org)   
[White paper](https://byteball.org/Byteball.pdf)  
[Medium articles](https://medium.com/@Byteball)  
[Byteball Asset Manager for you to create a new asset](https://byteball.market)   


### Forums
[Slack channel invitation](https://slack.byteball.com) (invites temporarily suspended, Oct 17)   
[Bitcointalk thread](https://bitcointalk.org/index.php?topic=1608859.0)   
[Tony's posts on bitcointalk](https://bitcointalk.org/index.php?action=profile;u=412662;sa=showPosts;start=0)  
[Subreddit](https://reddit.com/r/byteball)  


## License
![Cc Licence](/uploads/slackjore/cc-licence.png "Cc Licence"){.align-center}  
This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/)

-----

This is the same license as Wikipedia uses. See the Byteroll article [license](https://byteroll.com/license) 

-----
**This home page main authors: @portabella and @slackjore at the** [Byteball slack](http://slack.byteball.org/)


