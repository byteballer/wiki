<!-- TITLE: Article guidelines -->
<!-- SUBTITLE: Points for article editors on style etc for wiki uniformity and ease of use -->
# Guidelines
## Creating a new article/page
**Is the topic already covered?:** Search carefully before spending hours researching and writing a new article. Your proposed topic entitled *Airdrop* may already exist in another article called *Air drop* or *Distribution* or even as a section of an article called *Features* or whatever.

The Search box at the top mostly only searches Byteroll article titles, not text within the body of the article. But you can search for a term -- say "blackbytes" -- in this wiki in Google like so:

> site:byteroll.com blackbytes

**New page:** You make a new page by (1) clicking on "+ CREATE" in the top right, and (2) entering in the proposed page path. For now most new pages will have a path "/new-page" and not "/some-existing-page/new-page". Use the title for the path, so an article on "simple elliptic curves" should have the path "/simple-elliptic-curves" or "/cryptography/simple-elliptic-curves" and not "/obscure-math-stuff". Do not use capital letters in the path.

**Title:** Capitalize first word only unless using proper names. So "Merchant use of Byteball" and not "Merchant Use of Byteball". Use simplest form of word in title if possible, so "Oracle" and not "Oracles". This is to prevent two articles being written on the same topic because the new editor searched for an *Oracle* article and missed an already-written (and poorly-titled) *Oracles* article.

## Editing a page
### Style

**Attribution:** As the final line of the body of the article, underneath a horizontal rule, write your username and contact details. For example, **This "Installation" page main author: @markcross at the** [Byteball slack](http://slack.byteball.org/). This is a wiki so anyone can edit it, but since we're just starting out let's be polite and ask permission before making any large changes. However, just go ahead and tweak the small stuff on an established page without any approval needed.

**Headings (H1, H2 etc):** Same capitalization as above. Note that H1, H2 headings will only appear in the PAGE CONTENTS sidebar if H1 is used.

**Line breaks:** Don't add unnecessary line breaks. The browser renders it wrong with different-sized-width screens, like desktop or mobile. Change your browser window size wider or narrower to see this when there are added line breaks. Compare this paragraph (no line breaks) to the next while using a narrow window.

Don't add unnecessary line breaks. The browser renders it wrong with different-sized-width  
screens, like desktop or mobile. Change your browser window size wider or narrower to see  
this when there are added line breaks. Compare this paragraph (line breaks) to the previous one.

### Additional commands
This means operations you can do in addition to those shown by the icons at the top of the edit box.

**How to make a newline:** Sometimes you do want a newline, a short line to be followed by another line at the usual spacing. If you simply press Enter it will look fine in the Edit screen but in Normal view will render as if you hadn't pressed Enter. And Enter Enter creates a new paragraph, as expected. Type Space Space Enter at the end of the first short line (and any subsequent ones) and that will work fine.

**How to make a table:** Don't use for layout as it doesn't work well on a mobile screen, so small data tables only.
Use [External: Markdown table generator](http://www.tablesgenerator.com/markdown_tables) to generate a table of the desired (small) size. Then copy/paste it into your edit box. You can add the tablecell data manually, or use the easier "file/paste table data" option at the external site.

**Linking to anchors:** You can provide a link to not only a byteroll page, but also a heading (H1, H2, or H3) on that page. Write "..../page#(exact-wording-in-heading)". Use all lower-case. For example, the link to this "Editing-a-page" section is [https://byteroll.com/ed/article-guidelines#editing-a-page](https://byteroll.com/ed/article-guidelines#editing-a-page). 


-----
**This "Article guidelines" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
