<!-- TITLE: Slackjore -->
<!-- SUBTITLE: A quick summary of Slackjore -->


# About me
Professionally: I'm 65+, retired and my time is mostly my own. My nature: archivist, librarian, teacher, empowerer, that sort of thing.

[Twitter](https://twitter.com/slackjore)  
[Vimeo](https://vimeo.com/slackjore)  
[Blog: Jorescratch](https://jorescratch.wordpress.com)

Needless to say, "Jore Bohne" (jawbone) is not on my birth certificate.
# My notes
# Video competition (snipped from videos page)
Competition with prizes for new videos showing Byteball in use. There are seven prizes, that will be percentages of the total prize money. In order, 1st = 50%, 20, 10, 5, 5, 5, 7th = 5%. Deadline for entry is next airdrop snapshot time. You can post video links in the Byteball Slack #video_competition channel. Links to all entries will be posted by Slackjore here in this section.

Ref [Google doc](https://docs.google.com/document/d/1FPY2s5-R2p1y2dMwojplL1F6XWOcvq8mxUqQvHYruEM/edit?usp=sharing)

The prize money (currently 0.5 GB) will be distributed by @yestur, who is holding it in address [FBSQSGYZ6BHZGCNA254IITDCTMWOCQZC](https://explorer.byteball.org/#FBSQSGYZ6BHZGCNA254IITDCTMWOCQZC). Anyone can contribute to this (no blackbytes, please). 

## Videos in competition for Sept 2017
[image]  
**Date uploaded:** 2017-xx-xx 
**Channel:** ... 
**Title:** ... 
**Length:** ...
**URL:** ...
**Notes:** ...

[image]  
**Date uploaded:** 2017-xx-xx 
**Channel:** ... 
**Title:** ... 
**Length:** ...
**URL:** ...
**Notes:** ...

. . . 

## Links
[New Bitcointalk thread](https://bitcointalk.org/index.php?topic=2099677.new#new)  
[Slack channel invitation](https://slack.byteball.com)

# snipped from airdrop page

The snapshot for the 11th round will be taken at the time of the first bitcoin block solved after the full moon of December, on December 3, 2017 at 15:47 UTC. If you linked your BTC address(es) correctly, proving your balance at that moment in time, you will get delivered to your full or light wallet all of these that apply:

* **BTC to bytes:** For every 1 BTC, 6.25 MB [0.00625 GB or 6,250,000 (white)bytes] 

* **BTC to blackbytes:** For every 1 BTC, 2.1111 x 6.25 million blackbytes (money supply of blackbytes is 2.1111 times as much as that of bytes)
 
* **Bytes to bytes:** For every 10 (white)bytes on **any** Byteball address, 1 new (white)byte 
 
* **Bytes to blackbytes:** For every 10 (white)bytes on a **linked** Byteball address: 2.1111 new blackbytes

If you have only bytes, and no linked address, you still get the bytes-to-bytes 10%.

add andreas vid to money article?

https://youtu.be/ONvg9SbauMg

## "Nuked" pages to rename, re-path, for any new articles being created

 ...


## Articles needing work
**/error** needs more useful content  


## Additional keyboard shortcuts
**Newline:** Type space space at the end of the line above  
`monospace` (see page source for syntax)


-----


# Witness
tonych [11:00 PM 2017-06-22] 
@markcross
> and it's those witnesses that wallet has choosen that will try to determine the validity of the transaction/message?
To make sure there is no misunderstanding, actually, No.
Witnesses are not sole validators.
All full nodes perform validation, witnesses are a small subset of full nodes.  They are no special in regards to validation.
Witnesses play other role.  Remember we are on a DAG, there is no strict order between units.   All full nodes look at witnesses in the recent history to establish the path of the Main Chain.  The Main Chain is chosen such that it goes through as many witness-authored units as possible.  Total order is then determined relative to the Main Chain, and total order resolves conflicts caused by double-spends -- the earlier version wins.  That's the role of the witnesses - to draw the Main Chain through them.
# from glossary
**WITNESS:** A witness is a highly reputable user with a real-world identity, who stamps each transaction seen. There are 12 witnesses involved in every transaction. In exchange for the work involved, a witness collects part of the transaction fee. This list varies very little from transaction to transaction. If 11 witnesses say no to a bad transaction and 1 says yes, that witness gets deemed unreliable and effectively fired. It would be unthinkable for all 12 to collude and allow a fraudulent transaction through. In this way the network is safeguarded.  
Ref [Byteball white paper PDF](https://byteball.org/Byteball.pdf)

# DAG-related terms needed for understanding of wp
**BEST PARENT:** *One way to build a main chain is to develop an algorithm that, given all parents of a unit, selects one of them as the “best parent”. The selection algorithm should be based only on knowledge available to the unit in question, i.e. on data contained in the unit itself and all its ancestors. Starting from any tip (a childless unit) of the DAG, we then travel backwards in history along the best parent links. Traveling this way, we build a main chain and eventually arrive at the genesis unit. Note that the main chain built starting from a specific unit will never change as new units are added. This is because on each step we are traveling from child to parent, and an existing unit can never acquire new parents.*

**EDGE:** A **link** connecting two units (vertexes) in a DAG. "Edge" and "vertex" are familiar enough with regard to a polygon, but "edge" is not at all intuitive to spot in a DAG, and for a while I wondered if it was some special kind of vertex.

**MAIN CHAIN** (Abbr. **MC**) *[W]e could choose a single chain along child-parent links within the DAG, and then relate all units to this chain. All the units will either lie directly on this chain, which we’ll call the main chain, or be reachable from it by a relatively small number of hops along the edges [links] of the graph. It’s like a highway with connectingside roads*

**MAIN CHAIN INDEX:** (Abbr. **MCI**)

**NON-SERIAL (UNIT):**

**SERIAL (UNIT):** *There is one more protocol rule that simplifies the definition of total order. We require, that if the same address posts more than one unit, it should include (directly or indirectly) all its previous units in every subsequent unit, i.e. there should be partial order between consecutive units from the same address. In other words, all units from the same author should be serial. If someone breaks this rule and posts two units such that there is no partial order between them (nonserial units), the two units are treated like double-spends even if they don’t try to spend the same output.*

**TIP:** Childless unit

**TOTAL ORDER** (Is this even a techni8cal term?) *Once we have a main chain (MC), we can establish a total order between two conflicting nonserial units. Let’s first index the units that lie directly on the main chain. The genesis unit has index 0, the next MC unit that is a child of genesis has index 1, and so on traveling forward along the MC we assign indexes to units that lie on the MC. For units that do not lie on the MC, we can find an MC index where this unit is first included (directly or indirectly). In such a way, we can assign an MC index (MCI) to every unit.*


## in more detail, how this done
Is this true? 1. ordinary user authors a storage unit, including regular fees

2. Unit is transmitted to all hubs, with priority to whichever one is stated in his wallet as the default

3. Hub broadcasts unit to be picked up by all 12 witnesses, and it's ok if up to 5 of them are offline/not working etc

4. Each witness who gets it validates/rejects it, and re-broadcasts it but with his stamp of approval attached as a child unit, a witness-approval-stamp unit

5. When a unit is observed to have a majority of witness approvals, 7 say, it gets the FINAL (confirmed) stamp



### dag-level nitty gritty, but NOT copypasta
Here's PART of the copypasta version to make nondev-readable from the white paper abstract:

> Total order is established by selecting a single chain on the DAG (the main chain) that is attracted  to units signed by known users called witnesses. 

> A unit whose hash is included earlier on the main chain is deemed earlier on the total order. Users choose the witnesses by naming the user-trusted witnesses in every storage unit.

> Witnesses are reputable users with real-world identities, and users who name them expect them to never try to double-spend. As long as the majority of witnesses behave as expected, all double-spend attempts are detected in time and marked as such. As witnesses-authored units accumulate after a user’s unit, there are deterministic (not probabilistic) criteria when the total order of the user’s unit is considered final.


## Statistics page, on hold for now
### GByte price
See widget on wiki front page in the navigation bar, powered by [Coinmarketcap](https://byteroll.com/coinmarketcap.com). If your have a narrow screen, and the nav bar doesn't show, click the link and scroll down.
### funding
# Funding issue
see jorescratch.wordpress.com

# Colors gray etc

slackjore 
[8:10 PM] 
Change of subject . . .

[8:11] 
If we've finished arguing about a slogan for now, how about color? (edited)

[8:12] 
Probably treading on some toes here, but, anyway . . .

[8:14] 
I just looked at the Byteball sub-reddit. It's grey. (I've spent decades in both the UK and the US and my spelling's gone to hell with color/colour, the single/double consonants like labeling/labelling, the -ise and -ize, I use both and don't care any more that I mix them up). (edited)

[8:14] 
Back to the sub-reddit. It looks like something respectful and sombre, full of gravitas. (edited)

[8:14] 
Like a funeral home. Or old men in three-piece suits, grey ones, with grey faces; monotone. (edited)

[8:18] 
Is this appropriate for Byteball's future? It sort of fits with the conservative banking idea of asset-issuance -- even though big bank use of that is likely to be decades in the future if ever -- but MY high-street bank tries to be fairly bright and modern without trying to be hip. And cryptofans who might be interested in trying out smart contracts aren't likely to be 75 years old and impressed by classical architecture (long vertical columns). (edited)

slackjore 
[8:29 PM] 
See https://www.entrepreneur.com/article/233843 for instance. I know little about color choice in marketing, beyond noting my own responses sometimes.
Entrepreneur
The Psychology of Color in Marketing and Branding
A comprehensive look at the role color plays in marketing and how it impacts customer engagement. (66kB)
April 13th, 2016 at 1:00 PM
(edited)

slackjore 
[9:44 PM] 
The wallet itself is fine, by the way. The user can even pick his own colors. :slightly_smiling_face: (edited)


### light
deleted

# Attest
For article on attested assets

tonych [9:22 AM 2017-06-27 slack] 
> I am not 100% certain what the address would be but I think that could be a device address or a wallet address.
A wallet address.
After being attested, the address gets access to assets that are restricted to attested users.
The choice of trusted attestors is up to the asset issuer.
The attestor would certify that this of that address is associated with a KYCed user (with or without disclosing the real names) or with a user who satisfies the criteria of accredited investor under US law.

# videos
# YouTube
**Date uploaded:** 2016-12-23
**Channel:** Marc De Mesel
**Title:** Byteball: Scalable, Private, Free but HURRY!!
**Length:** 9:03
**URL:** https://www.youtube.com/watch?v=bPicDP-ehfU
**Notes:** 3209 views at 2017-07-01

**Date uploaded:** 2017-04-13
**Channel:** Future Blockchains
**Title:** What is Byteball - blockchain
**Length:** 3:48
**URL:** https://www.youtube.com/watch?v=hhZGU1W_Hsw
**Notes:** Talking head throughout; 1216 views at 2017-07-01

**Date uploaded:** 2017-04-26
**Channel:** DashMeister
**Title:** Byteball: The most amazing crypto for free
**Length:** 5:03
**URL:** https://www.youtube.com/watch?v=IFP0rPgMu_Q
**Notes:** Narrated; shots from coinmarketcap and byteball.org; 3041 views at 2017-07-01

**Date uploaded:** 2017-06-07
**Channel:** DashMeister
**Title:** How to get free Byteball step-by-step (transition bot)
**Length:** 3:14
**URL:** https://www.youtube.com/watch?v=igJka8q69RA
**Notes:** Narrated; shots of byteball.org and transition bot; 746 views at 2017-07-01


