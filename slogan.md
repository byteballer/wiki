<!-- TITLE: Slogan -->
<!-- SUBTITLE: Edited mostly from Slack posts by Slackjore -->

# Smart payments made simple
What's a slogan? It's not just some cute phrase someone thought of one day, like "Byte me!"

**A slogan** is a memorable motto or phrase used in a clan, political, commercial, religious, and other context as a repetitive expression of an idea or purpose, with the goal of persuading members of the public or a more defined target group. The Oxford Dictionary of English defines a slogan as "a short and striking or memorable phrase used in advertising." A slogan usually has the attributes of being memorable, very concise and appealing to the audience. These attributes are necessary in a slogan, as it is only a short phrase. **Therefore, it is necessary for slogans to be memorable, as well as concise in what the organisation or brand is trying to say and appealing to who the organisation or brand is trying to reach.**  
Ref [Wikipedia article: Slogan](https://en.wikipedia.org/wiki/Slogan)

Note the final sentence. Exactly WHO are we trying to reach?

## Byteball's primary public/audience
Our primary target right now must be cryptofans, people who are interested in cryptocurrencies, who probably already have one or more crypto wallets, who have received/sent crypto transactions, and who are aware of the world of altcoins beyond Bitcoin.

Why? Because we speak a language that is different to what "the general public" uses, even if both are nominally English. The penetration into normal society of King Crypto -- Bitcoin -- is just about negligible. How many bricks-and-mortar stores in your neighborhood even accept Bitcoin, let alone any other crypto? Yes, this will change in the future, but wishing it were happening now is not the same as the actuality of it happening now.

So our marketing time and efforts must go in the direction of who we can reach with our message, and that is limited to people already in the crypto space.

**Our primary public/audience are existing cryptofans.**

## What message?
Right now, Byteball is just another altcoin (among 500+) to many. How to distinguish it from the others?

**Positioning refers to the place that a brand occupies in the mind of the customer and how it is distinguished from products from competitors.** In order to position products or brands, companies may emphasize the distinguishing features of their brand (what it is, what it does and how, etc.) or they may try to create a suitable image (inexpensive or premium, utilitarian or luxurious, entry-level or high-end, etc.) through [marketing]. Once a brand has achieved a strong position, it can become difficult to reposition it. Positioning is one of the most powerful marketing concepts.  
Ref [Wikipedia article: Positioning](https://en.wikipedia.org/wiki/Positioning_(marketing))

In the cryptocurrency field, *Byteball* functions as a brand name, as do *Bitcoin*, *Ripple*, *Ethereum*, *Dash* and all the 500+ others. The space is becoming increasingly commoditized, and as Bitcoin loses its dominance a crypto brand needs a **unique selling point** in order to stand out from the crowd. The Byteball USP at this point in time is its conditional-payments feature.  
Ref [Wikipedia article: Unique_selling_proposition](https://en.wikipedia.org/wiki/Unique_selling_proposition)

### Killer feature
Byteball's **killer feature** right now is the Smart/Conditional Payment. What's the **first** bit of copy on Tony's site under the headings?

> **Why use Byteball?**
> 
>Byteball allows you to do something that traditional currencies can't: conditional payments. You set a condition when the payee receives the money. If the condition is not met, you get your money back.

Factually, Byteball is the first cryptocurrency with conditional payments. But you can't use the words "conditional payments" in a slogan as no-one knows what they are. Hence we use "smart payments" instead. It's true that "smart payments" can mean almost anything, but that doesn't matter at all. Why? Because no other cryptocurrency has taken the "smart payments" branding position, so **we** get to explain what *smart payments* are.

### "But Byteball has so much more!"
The opening words of the white paper are, *Byteball is a decentralized system that allows tamper proof storage of arbitrary data, including data that represents transferrable value such as currencies, property titles, debt, shares, etc. Storage units are linked to each other [cryptographically].*

One could say those benefits are only tenuously described as "smart payments", which is true. But those attributes are not yet available to consumers. No single crypto brand is going to be all things to all people, for buying a coffee, buying a house, immigrant workers sending money back home weekly, a store of value just in case, a simple contract, a complex contract, validating real estate ownership, and lots more. A crypto must select its best niche market and concentrate its marketing on that. All the other development and roll-out can continue, but the marketing emphasis is limited to one single theme: conditional payments.

**Our best message target is the conditional-payments niche in the cryptofans market.**

## What words for the message?
We have to position Byteball against (i.e., in relation to, not in opposition to) the leader in this conditional-payments category. Let's see how some copycat cryptos with savvy marketing people might do it:

> **Byteclub**, the powerful smart-payments crypto  
**Chicbyte**, the smart-payments solution for the smart woman  
**Firstbyte**, smart-payments for your 5-year-old  
**Bytenip**, the smart-payments app for cats
> 
> For men, women, young kids not yet old enough to download an app themselves, and pets. Well, OK, maybe not the last one.

But what about Byteball? Oh! Oh! There are **zero** competitors (at present) in the conditional-payments category. We are the leader, the first by default. So we can say whatever we want.

After considerable discussion in the Slack #marketing channel on 20 June 2017 (look it up in the archives if you wish), we settled on Cryptkeeper's **Smart Payments Made Simple**.

## The future
"Smart payments made simple" looks good for a long time. It uses no crypto-specific words -- although explaining how *smart payments* equals *conditional payments* still needs to occur -- so will transfer seamlessly from a public very familiar with cryptocurrencies to one not so.

## References

Some good references on positioning and branding:

[Positioning: The Battle for Your Mind](https://www.amazon.com/Positioning-Battle-Your-Al-Ries/dp/0071373586) (Ries and Trout, 1981)  
[22 Immutable Laws of Marketing](https://www.amazon.com/22-Immutable-Laws-Marketing-Violate/dp/0887306667) (Ries and Trout, 1994)  
[22 Immutable Laws of Branding](https://www.amazon.com/22-Immutable-Laws-Branding/dp/0060007737) (Ries and Ries, 2002)

-----
**This "Slogan" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)