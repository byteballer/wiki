<!-- TITLE: Polls -->
<!-- SUBTITLE: This is to record Byteball poll results from Twitter etc. -->

# Twitter 2018-01-25
**Poll for #Byteball wallet holders, have you made a simple payment to another wallet? (91 votes)**[[1](#references)]

1. (53 votes) 58% Yes, I've sent Bytes  
2. (14 votes) 16% Yes, I've sent others too  
3. (11 votes) 12% No, I've 0 Bytes for fees  
4. (13 votes) 14% No, another problem

## Notes
**Purpose of poll:** To get some idea of what fraction of wallet-owners had done the simplest task  
**Follow up:** (1) Send #3 people maybe 20KB so they can make 20+ tx; (2) Ask #4 people for details.[[2](references)]

# References
1. https://twitter.com/slackjore/status/956134649973149696
2. https://twitter.com/slackjore/status/956504398095704065


-----
**This "Asset" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)