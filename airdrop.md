<!-- TITLE: Airdrop -->
<!-- SUBTITLE: How and when the Byteball currencies of bytes and blackbytes get distributed -->

# Install a wallet
Normal users should choose a light/quick wallet, not the full/slow option. The full one takes a long time to sync, and you cannot change from full to light but have to start from scratch. Be warned.

See normal installation videos etc at the wiki article [Installation](https://byteroll.com/installation) 

## Complexity
See the [Wallet](https://byteroll.com/wallet) article for variations on multiple wallets and multiple devices. Generally, the more complex the arrangement you create, the more likely it is to fail, like sending one device's address from another device. Keep it as simple as you can.
# After installing a wallet
After installing, chat with the Transition Bot to participate in the next distribution round. [This link](byteball:A2WMb6JEIrMhxVk+I0gIIW1vmM3ToKoLkNF8TqUV5UvX@byteball.org/bb#0000) will open automatically in your wallet when you click it.

## Byteball address (disambiguation)
* A wallet address is all upper case, looks like K7RMH5EFPZW67JTS5B5GA6PDZA4MYX4LY  
* A unit address, like a transaction you sent, looks like Ip241kkFRkZnaVF61Z6+/JW3YELOaHn9C6PAjeMp8fs=  
* And a bitcoin address looks like 13AM4VW2dhxYgXeQepoHkHSQuy6NgaEb94

## Proving ownership 
The Transition Bot will ask you to send your Byteball address, then prove ownership of a regular Bitcoin address you provide by either sending a small transaction from it, or by signing a message using its private key. Workable (free!) signing solutions are:
* Electrum  
* Trezor  
* Nano Ledger S
* Mycelium (on Android)

Note that the bitcoin address can have zero bitcoins in it. The bot only needs a signature from the correct private key. Fractional bitcoin amounts like 0.027 BTC are fine too.
### sign
### Signing a message
* Click on "sign a message" in your Bitcoin wallet menu
* In the "message" field, paste your Byteball address
* In the "address" field, paste your Bitcoin address
* Click "Sign" (not "Verify")
* Copy the resulting "signature", and paste it for the transition bot

### BTC on exchanges
Generally it won't work to try and link a Byteball address to bitcoins held on an exchange. You need to know the private key to sign a message for the Transition bot. There apparently is an exception:

> cryptkeeper [2017-06-06 8:07 AM]  
@xxxx I stand corrected: cryptox.pl will pay out distributed bytes and blackbytes for a fee of 5%

## P2PKH addresses
If you try and sign a Bitcoin address beginning with 3... (instead of 1...), the transition bot will say "Only P2PKH addresses are supported." You can still use your P2SH address (3...), but you will have to prove ownership by the microtransaction method and not the signing method. This is a Bitcoin limitation, not a Byteball limitation.

P2PKH = Pay to Public Key Hash and P2PSH = Pay to Script Hash. Further explanations are outside the scope of this article, but are easy enough to find online.

## Multiple addresses
You are allowed to link more than one Bitcoin address to a single Byteball address. 

But you are not allowed to link more than one Byteball address to a single Bitcoin address. If you link both by transaction, neither will work. If you link both by signing a message, the first will deactivate but the replacement will work. 

These are OK, for example:
| Byteball address | Bitcoin address |
|------------------|-----------------|
| CDE... linked to | 1j4y...         |
| CDE... linked to | 17g6...         |
| PQR... linked to | 1kmr...         |
| . . .  linked to | . . .           |

### Many Bitcoin addresses
It may be easiest to consolidate all your wallet bitcoins into one Bitcoin address. Choose any one of your Bitcoin wallet addresses as a destination, and send all bitcoins in the wallet to that address. After the transaction has confirmed, now you can sign a message for the transition bot using that single Bitcoin address.

### Rinse, repeat
If you want to link different Bitcoin addresses to different Byteball addresses, your dialog with the bot should consist of repeated series of these 3 steps:  
* Byteball address  
* Bitcoin address  
* signed message
# Distribution
All the Byteball (native) currency units were created at the outset. About 65% have been distributed so far, in ten rounds, starting December 25, 2016. A snapshot recording which wallets will get "bytten" has been being taken at the first bitcoin block after every full moon. The bytes distributions usually take place maybe 12 hours after the snapshot. The blackbytes distributions usually take several **days**, starting with the highest amounts and working down to the lowest, so be patient. Tony will announce when the distribution is complete.

## Total supply
Total supply of bytes = 10^15 = 1,000,000,000,000,000  
Total supply of blackbytes = 2.1111 x 10^15 = 2,111,100,000,000,000


### rules

## Current rules and rates 

From Tony https://bitcointalk.org/index.php?topic=1608859.msg24965637#msg24965637 (2017-11-21)

> So far, bytes were mainly distributed via airdrops to holders of BTC and Bytes, and a relatively small portion was distributed through the cashback program.  We started this free distribution with the purpose of getting the currency as wide spread as possible, and while the purpose stays the same, methods can change.  The airdrop method worked well to get the initial user base, but it also proved to be the least efficient in terms of user acquisition cost.

> Over the next few months, we'll add more methods of free distribution.  One of them is already being developed, I expect it to be ready in the first half of December. 

> The new methods are designed to grow our user base faster than the money supply.

> While the new methods are still untested, it is early to drop the airdrops.  That's why the next round of distribution to Bytes and BTC holders is tentatively planned for the full moon of March 2, 2018.  The rules are the same as in the previous round:

* **BTC to bytes:** For every 1 BTC, 6.25 MB [0.00625 GB or 6,250,000 (white)bytes] 

* **BTC to blackbytes:** For every 1 BTC, 2.1111 x 6.25 million blackbytes (money supply of blackbytes is 2.1111 times as much as that of bytes)
 
* **Bytes to bytes:** For every 10 (white)bytes on **any** Byteball address, 1 new (white)byte 
 
* **Bytes to blackbytes:** For every 10 (white)bytes on a **linked** Byteball address: 2.1111 new blackbytes

If you have only bytes, and no linked address, you still get the bytes-to-bytes 10%.

> This round can be postponed or even canceled depending on performance of other distribution methods.

# Verify your balances
## Bitcoins
If you spend money from a bitcoin address, the "change" by default goes into a different address (explained more at [Change address](https://byteroll.com/change-address)). Check before the airdrop that your bitcoins are in the linked address, and move them back there if necessary. Allow enough time for bitcoin transaction delays.

## Bytes
Similarly, if you've been using your byteball wallet even for small amounts, a big chunk of your bytes may have moved to different addresses. Send them back to your linked address in time for the airdrop.

### check
 ## The transition bot will tell you your linked balances
 The bot will tell you the current linked balances for both your bitcoins and bytes. Just say hi to it. This [Transition Bot](byteball:A2WMb6JEIrMhxVk+I0gIIW1vmM3ToKoLkNF8TqUV5UvX@byteball.org/bb#0000) link will open automatically in your wallet when you click it. 

### Online check
You can also track linking progress at [Byteball main site: transition pages](https://transition.byteball.org).

### Bot down sometimes
Sometimes the bot is down for some hours. During the monthly distribution period it's out of action for several days, and may simply repeat back whatever you say to it. Don't worry -- it will wake up again.

### Changed hubs
If you change hubs, say hi to the transition bot before the next distribution, so that the distribution bot knows where to send the blackbytes.


# After


# Wrong amount
Here are steps to take after the airdrop if you think you didn't receive the bytes/blackbytes you should have. When you ask for help in Slack this is what someone else has to do for you, so it is preferable to do it yourself instead.

0. Make sure you are using the latest version of the wallet, currently 2.1.

1. Be aware of the rules above, especially concerning

* when the distributions start, how long they take, and when we know they are finished
* blackbytes and linked addresses
* how [change addresses](https://byteroll.com/change-address) work.

2. Check the address(es) at transition.byteball.org to see what is linked and the bitcoin balance, and/or explorer.byteball.org to see the current Byteball address balance.

3. Check the **date/time** of any transaction that moved your coins. Was it before the snapshot?

At this point, most issues should have been resolved. 
## Too soon
Some people move their coins to an exchange seconds or minutes after the exact time of the full moon but before the next bitcoin block when the snapshot is taken.  

## Wallet and explorer differ
Does explorer.byteball.org show some new bytes have arrived, but they don't show in your wallet? 

**Wallet not connected properly?** Your internet connection needs to allow communications to get to and from the wallet: an easy check is do the chatbots work? If the chatbots don't work, maybe showing a Socket Closed error, this lack of proper connection must get fixed.
* Other internet things work, like a refreshed/reloaded browser opens a website, right?  
* Firewall problem?  
* Tor turned on by mistake? (**Check** this at Settings > TOR, don't just assume it isn't)
* Something else wrong?

**Wallet not sync'd?** If have a full wallet it needs to sync up to the date of a transaction to show it. Then, transfer your coins to a light wallet and when you're sure the coins are in the light wallet dump the full wallet.

**Weird wallet?** If you have done something weird like "copied" a wallet from one device to another, or are running more than one instance on a single computer, then you may or may not be able to access these coins. 


-----

# History
The first drop distributed exactly 10% of the bytes. The figures 1.4152, 2.9876 and 0.1319 have been rounded off.

| # | Snapshot (UTC) | Block time | BTC to GB | BTC to GBB | B to B | B to BB |
|---|----------------|----------|-----------|------------|--------|---------|
| 1 | 25/12/16 00:00 | 00:02:59 | 1.4152    | 2.9876     | 1      | 2.1111  |
| 2 | 11/02/17 00:33 | 00:44:34 | 0.0625    | 0.1319     | 0.1    | 0.21111 |
| 3 | 12/03/17 14:54 | 15:02:25 | 0.0625    | 0.1319     | 0.1    | 0.21111 |
| 4 | 11/04/17 06:08 | 06:22:12 | 0.0625    | 0.1319     | 0.1    | 0.21111 |
| 5 | 10/05/17 21:42 | 21:46:16 | 0.0625    | 0.1319     | 0.1    | 0.21111 |
| 6 | 09/06/17 13:10 | 13:17:27 | 0.0625    | 0.1319     | 0.2    | 0.42222 |
| 7 | 09/07/17 04:07 | 04:20:40 | 0.0625    | 0.1319     | 0.2    | 0.42222 |
| 8 | 07/08/17 18:10 | 18:11:03 | 0.0625    | 0.1319     | 0.2    | 0.42222 |
| 9 | 06/09/17 07:02 | 07:09:14 | 0.00625   | 0.0132     | 0.1    | 0.21111 |
|  | October skipped  |    |      |    |  |
| 10 | 04/11/17 05:23 | 05:24:16  | 0.00625   | 0.0132     | 0.1    | 0.21111 |
|  | December to February skipped  |    |      |    |  |
|11 | ? 02/03/18 ? |
|≥12 | unknown |

See [@0xB10C graph of airdrop amounts](https://pbs.twimg.com/media/DJNQM2vXcAECClL.jpg)  
and [for blackbytes](https://pbs.twimg.com/media/DJNQmBCWsAEMDOy.jpg)


-----
**This "Airdrop/Distribution" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
