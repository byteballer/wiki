<!-- TITLE: Grassroots -->
<!-- SUBTITLE: A quick summary of Grassroots -->

# How to support Byteball – the ultimate Guide
See https://docs.google.com/document/d/1PGvFwFKkyBjqIW1DwtEyTnOpCW_d7Dv70mjwPcqglsQ/edit

**Doc maintained by @btctry at the** [Byteball slack](http://slack.byteball.org/)


# Byteball community marketing spreadsheet - simple daily tasks everyone can do to help out
See https://docs.google.com/spreadsheets/d/1ZP93Tg9WmPuMhaiybxRXvBNZt0orccEMeZa39Q0zK34/edit#gid=0

**Doc maintained by @blokchain at the** [Byteball slack](http://slack.byteball.org/)



