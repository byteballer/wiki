<!-- TITLE: Timestamp -->
<!-- SUBTITLE: Different smart-contract uses for the timestamp oracle -->

# Oracle
The oracle address is I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT and timestamps are in milliseconds since Jan 1, 1970 UTC.

# Notes
## Tied-up funds
The "offer a contract" options tie up the contract-writer's payment amount until the specified future time without possibility of recovery.

The bound-payment options allow either commitment as above, or the writer's voiding the contract and recovering his funds at any time before the payee collects them.

## Times

There are three times that may be independently set by the contract-writer:  
1. The time after which, if the peer doesn't agree to and fund the contract, the writer can reclaim his funds ("offer a contract" option only)
2. The time after which the writer/peer can unlock the contract
3. The time after which the peer/writer can unlock the contract.

Note that after both (2) and (3) have taken place, either party will be able to spend the funds. 

## Who peer pays ("offer a contract" option ONLY)

Be careful in selecting who the peer pays, either to "me" or "the contract": 
* With a bet, where the winner takes all, one would usually write "the contract"
* For a simple future payment, where the recipient must contribute a token amount, it doesn't matter
* But where the contract requires significant funds from both parties if it is executed, its proper non-execution must allow each party to get his funds back. 

**Note that the peer MUST pay, even the token amount, for the contract to be valid.** 

## Smart contract
Both the "offer a contract" and the bound-payment option create a new smart contract. The writer can see this contract immediately, but the payee only after the oracle posts the specified time. Then the payee can unlock the contract; the writer can unlock the contract after the take-back time. Note that it will be possible for both parties to unlock the contract after the later of those two times.

Using the timestamp oracle like this must be done in paired wallet chat. It cannot be done using some random address pasted into a send-payment screen.

## Conversion

Use website like https://www.epochconverter.com/ to convert between Unix Epoch Time (like 1498989599806) and human-readable date (like GMT: Sunday, July 2, 2017 9:59:59.806 AM).

### Milliseconds

Note timestamp is in milliseconds, with 13 digits. So if you are converting from date to epoch time remember to add “000” to the end if needed or the contract will fail. For example, six months from July 2, 2017 10:00 AM GMT is January 2, 2018, 10:00 AM GMT. A converter gives 1514887200, so to use the oracle you would change this to 1514887200000.

# A. Simple future payment using "offer a contract"
This is a birthday present for a friend, 1.7 GBB that can be collected about 4 months from today (July 2) on Nov 5, 2017.

**Left-click address (PQR...) sent by peer in wallet chat. Select "Offer a contract".**
## 1. PAYMENT TO THE CONTRACT 
**I PAY:** 1.7 GBB (can use any asset with sufficient funds)  
**PEER PAYS:** .01 MB (peer must pay something; here it is a token amount)  
**PEER PAYS TO:** this contract (peer gets payment back anyway)  
**IF THE PEER DOESN'T PAY, I CAN TAKE MY MONEY BACK AFTER:** 4 hours (leave as default, not important here as long as peer pays in time)

## 2. WHO CAN UNLOCK THE CONTRACT? 
* the peer, *if an event is posted by an oracle:*  
**ORACLE ADDRESS:** I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT  
**DATA FEED NAME:** timestamp (copy this name exactly)  
**POSTED VALUE:** >1509840000000 (MUST be 13 digits; this epoch time is Nov 5, 2017 00:00:00.000, about 125 days from today)

* me  *after contract expires in*  
155 days (this particular time allows the peer 30 days to unlock the contract before the writer can reclaim the funds) 

# B. Simple future bound payment
This is a birthday present for a friend, 3 zwibs that can be collected about 4 months from today (July 2) on Nov 5, 2017.

**Left-click address (PQR...) sent by peer in wallet chat. Select "Pay to this address".**

## Choose main wallet / sub-wallet
**AVAILABLE BALANCE:** 7 of ZW1b... (select asset to send)  
**TO:** PQR... (address is not user-changeable)  
Click link "Bind the payment to a condition"  
**AMOUNT:** 3 of ZW1b

## THE PEER RECEIVES THIS PAYMENT IF
An event is posted by an oracle (select from drop-down menu)  
**ORACLE ADDRESS:** I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT  
**DATA FEED NAME:** timestamp (copy this name exactly)  
**EXPECTED VALUE:** >1509840000000 (MUST be 13 digits; don't forget the ">" (means "greater than"); this epoch time is Nov 5, 2017 00:00:00.000, about 125 days from today)

**THE EXPECTED VALUE WILL BE POSTED:** either way  
**IF THE CONDITION IS NOT MET, I CAN TAKE THIS MONEY BACK AFTER:** 3720 hours (= 155 days converted to hours. This particular time allows the peer 30 days to unlock the contract before the writer can reclaim the funds) 

Click Send.

# Differences between A and B above
* Both A and B require the recipient to provide a receive address. However, with A the recipient must ratify this particular contract by paying a token amount; while with B the writer does not need the peer's explicit permission this time.


* By setting the time close to now in the "if the condition is not met" field, B allows the writer to void the contract and recover his funds at any time before the expected payment date. This may or may not be desirable.


-----

# Request to benefactor using "offer a contract"
This is a request to a rich aunt for 2 GB as a wedding present, that can be collected about 4 months from today (July 2) on Nov 5, 2017. It is useful because once funded it commits her to the payment, but prevents me from accessing the funds ahead of time.

**Left-click address (PQR...) sent by peer in wallet chat. Select "Offer a contract".**
## 1. PAYMENT TO THE CONTRACT 
**I PAY:** .01 MB (I must pay something; here it is a token amount)   
**PEER PAYS:** 2 GB (peer must pay the exact amount; it is not user-changeable in the contract she will receive)  
**PEER PAYS TO:** this contract (just in case something happens to me in the meantime)  
**IF THE PEER DOESN'T PAY, I CAN TAKE MY MONEY BACK AFTER:** 4 hours (leave as default, not important here as long as peer pays in time)

## 2. WHO CAN UNLOCK THE CONTRACT? 
* me *if an event is posted by an oracle:*  
**ORACLE ADDRESS:** I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT  
**DATA FEED NAME:** timestamp (copy this name exactly)  
**POSTED VALUE:** >1509840000000 (MUST be 13 digits; this epoch time is Nov 5, 2017 00:00:00.000, about 125 days from today)

* the peer  *after contract expires in*  
150 days (this particular time allows me 25 days to unlock the contract before the benefactor can reclaim the funds) 

# Exercising an option using "offer a contract"
Normally one buys an option to buy/sell something at a particular price at a particular future time. One pays for the option, but neither party commits the funds now for the possible future transaction. However, with this smart contract the funds from the contract-writer are tied up in the contract until the peer exercises the option or the contract expires, although the contract-writer can void (and violate) the contract at any time before the peer funds the contract.

In the following contract, in exchange for a pre-contract payment to me of 1 MB, I grant the peer the option to buy 15 zwibs from me for 10 MB, starting in 24 hours from now and valid for 29 days only. Any wallet asset could have been used, and asset volatility may be significant.

## Details of figures used in this contract  
* Current date/time is 2017-07-02 1:40pm UTC  
* "30 days" from now is, say, 2017-08-01 23:59:59.999 UTC  
* 2017-08-01 23:59:59.999 UTC is from now 30 days 10 hrs 20 mins = 30 + 620/1440 days = 30.43 days = 730.3 hours (the take-back-if-no-peer-payment figure can only be set to the nearest .1 hours, or 6 minutes)  
* Epoch time for 2017-07-03 1:40pm UTC is 1499089200000 
* 2017-08-01 23:59:59.999 UTC is from now 30 days 10 hrs 20 mins = 30 + 620/1440 days = 30.43 days (the contract expiry figure can only be set to the nearest .01 days, about 15 minutes)

**Left-click address (PQR...) sent by peer in wallet chat. Select "Offer a contract".**
## 1. PAYMENT TO THE CONTRACT 
**I PAY:** 15 zwibs (can use any asset with sufficient funds)  
**PEER PAYS:** 10 MB (can use any asset with sufficient funds)  
**PEER PAYS TO:** me (NOT the contract)  
**IF THE PEER DOESN'T PAY, I CAN TAKE MY MONEY BACK AFTER:** 730.3 hours 

## 2. WHO CAN UNLOCK THE CONTRACT? 
* the peer, *if an event is posted by an oracle:*  
**ORACLE ADDRESS:** I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT  
**DATA FEED NAME:** timestamp (copy this name exactly)  
**POSTED VALUE:** >1499089200000 (MUST be 13 digits)

* me  *after contract expires in*  
30.43 days 

# Dead-man contract using a bound payment
**Not working Jan 2018: can't put ">" before the timestamp right now**


This is a simple contract that will execute unless terminated by the drawer. The idea is that one transfers an asset by default if unable to stop the transaction (maybe because one is dead).

Here I wish to allow the payee to unlock 50 GB any time after 3 months from now. The 3 months is a trade-off between one week -- meaning I would have to remember to reissue this contract every week -- and 6 months or more -- meaning if I died next week the payee would have to wait six months or more to access the funds.

Note that I can revoke/reissue this contract at any time.


## Details of figures used in this contract  
* Current date/time is 2017-07-02 5:00pm UTC  
* "3 months" from now is, say, 2017-10-01 00:00:00.000 UTC  
* Epoch time for 2017-10-01 00:00:00.000 UTC is 1506816000000

**Left-click address (PQR...) sent at some prior time by peer in wallet chat. Select "Pay to this address".**
## Choose main wallet / sub-wallet
**AVAILABLE BALANCE:** XXX GB (select asset to send)  
**TO:** PQR... (address is not user-changeable)  
Click link "Bind the payment to a condition"  
**AMOUNT:** 50 GB

## THE PEER RECEIVES THIS PAYMENT IF
An event is posted by an oracle (select from drop-down menu)  
**ORACLE ADDRESS:** I2ADHGP4HL6J37NQAD73J7E5SKFIXJOT  
**DATA FEED NAME:** timestamp (copy this name exactly)  
**EXPECTED VALUE:** >1506816000000 (MUST be 13 digits; don't forget the ">" (means "greater than"); this epoch time is 2017-10-01 00:00:00.000 UTC, about 90 days from today)

**THE EXPECTED VALUE WILL BE POSTED:** either way  
**IF THE CONDITION IS NOT MET, I CAN TAKE THIS MONEY BACK AFTER:** .1 hours (allows the writer to revoke/reissue the contract any time after 6 minutes from when the payment confirms)

Click Send.

-----
**This "Timestamp" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)