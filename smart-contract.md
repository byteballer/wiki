<!-- TITLE: Smart Contract -->
<!-- SUBTITLE: About smart contracts, the basis of conditional payments ("smart payments"), Byteball's killer feature. And "the competition". Not too technical -->
# Contracts. Ethereum, right?
**Byteball** does **simple**, human-readable contracts to perform simple actions.

**Ethereum** does **complex**, programmer-readable contracts to perform complex actions.

No overlap.
## But Rootstock?
Rootstock is a centralized smart-contract platform planned to be layered on top of Bitcoin. RSK is just released on Bitcoin testnet.

Ref [Reddit thread 2017-05-30](https://www.reddit.com/r/Bitcoin/comments/6e7mdz/rsk_testnet_is_open/?st=j3bxt1g7&sh=63cbb552)

One post: RSK is EVM on BTC (that is, Ethereum Virtual Machine infrastructure, but linked to and secured by bitcoin's blockchain instead, using sidechains.)

RSK is now supported on Trezor. Ref [Online article 2017-05-30](http://gk2.sk/how-to-use-trezor-with-rootstock-rsk-using-myetherwallet/)
# Offering smart contracts

When you want to create a new smart contract with a user, your sequence is usually as follows:

1. You ask the user to send his payment address (it will be included in the contract)  
2. You define a new contract using the user's address and your address as parties of the contract  
3. You pay your share to the contract  
4. At the same time, you send a specially formatted payment request (different from the payment request above) to the user to request his share. You start waiting for the user's payment  
5. The user views the contract definition in his wallet and agrees to pay  
6. You receive the payment notification and wait for it to get confirmed  
7. After the payment is confirmed, the contract is fully funded

Ref [Github: Writing chatbots for Byteball](https://github.com/byteball/byteballcore/wiki/Writing-chatbots-for-Byteball) (this non-code bit is copy/pasted)

See wiki [oracle](https://byteroll.com/oracle) article for examples and videos

-----
 **This "Smart Contract" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
