<!-- TITLE: Gambling -->
<!-- SUBTITLE: Peer-to-peer random numbers game -->
# Random numbers game
Play a random numbers game with a peer, with verifable randomness and decentralized trust.

The oracle **FOPUBEUPBC6YLIQDLKL6EW775BMV7YOH** (which also monitors the Bitcoin blockchain) posts *pseudorandom* numbers derived from the Bitcoin block hash.  A new number is posted every time a new Bitcoin block is mined and gets one further confirmation.  The random numbers are posted under data feed name randomXXXXXX where XXXXXX is the Bitcoin block number.  

The numbers are between 1 and 100,000.  You can bet on a future random number being less than or greater than a specific value, for example, there is 25% chance that the number will be below 25,000 and 75% chance that it'll be above 25,000.  

The above contract was between my Mac and my phone, it was created before block 460852.  This is the data feed that was posted by the oracle after this block was mined: 

https://explorer.byteball.org/#Ns//o52EKR32ykuHfUfcm3ailHtta6TvM5kB07vbpVA=  
bitcoin_hash: 0000000000000000019dd457cf8e233701dab44ac855492d575a4366556213e0  
bitcoin_height: 460852  
bitcoin_merkle: GW9cQW5m0KLIi2suUlkzRP2yurg8jLEax3uHiun7kik=  
random460852: 34789  
 
34789 is more than 25000, so the Mac won.  

Join #gambling channel on our [Byteball slack](http://slack.byteball.org/) to find a partner for this p2p random numbers game.

-----

Info here from a post by @tonych on the [Bitcointalk Byteball thread](https://bitcointalk.org/index.php?topic=1608859.msg18499180#msg18499180)

-----

## Video showing this

[video](https://vimeo.com/221655112){.vimeo}


-----
**This (copy/paste) page main author: @tonych at the** [Byteball slack](http://slack.byteball.org/)
