<!-- TITLE: Features -->
<!-- SUBTITLE: User benefits, current and future -->
**Note for editors:** A feature here is from the user perspective. Under-the-hood WOWZAs are only features to developers.
# Byteball features (current)
![Bind 400](/uploads/slackjore/bind-400.png "Bind 400"){.align-right}• The **killer feature** is the Smart/Conditional Payment. You set a condition for how the payee receives the money. If the condition is not met, you get your money back. This substitutes for trust between strangers because neither is able to scam the other. 

• This smart-contract feature has many real-world peer-to-peer applications, including:
* no-fee crypto exchanges
* sports betting
* selling or buying insurance concerning negative events like a flight delay.

• Private payments can be made using blackbytes, a cash-like untraceable currency. Its transactions are not visible on the public database that shows all payments made with (white)bytes. Blackbytes are sent peer-to-peer instead in an encrypted chat session.

• Chatbots are fun and faciliate real-world transactions, including shopping with a merchant and paying with two clicks.

• There are the usual features like transactions cryptographically linked to each other in a decentralized, immutable (unchangeable), blockexplorer-like record; irrevocable, unstoppable payments with no third-party involved; storing savings untouchably outside the system; multi-signature security; yada yada yada. Tiny transaction fees though. Oh yes, and no blockchain or miners. :)
## Create new assets
As of 2 June, you can now create new assets. You can sell these tokens using a smart-contract in a chat session with someone, or if the asset allows it simply send some to any Byteball address without any prior arrangement. And the owner of that address can do the same. This capability to create and send new assets is a big deal. 

See wiki article [Asset](https://byteroll.com/asset)
# Byteball features (future)
The opening paragraph of the [Byteball white paper](https://byteball.org/Byteball.pdf) says: 
> Byteball is a decentralized system that allows tamper proof storage of arbitrary data, including data that represents transferrable value such as currencies, property titles, debt, shares, etc. Storage units are linked to each other [cryptographically].
> 
At present, Spring 2017, none of this is available for general use, apart from regular payments using (white)bytes and blackbytes.



-----
**This "Features" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/) 
