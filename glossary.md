<!-- TITLE: Glossary -->
<!-- SUBTITLE: Byteball terms, not too technical, but assuming some familiarity with cryptocurrencies -->
# Notes to editors 
1. Give sources if possible, or links to byteroll articles that should include sources.
2. Don't add unnecessary line breaks. The browser renders it wrong with differently-sized-width screens, like desktop or mobile. Change your browser window size wider or narrower to see this when there are added line breaks.
# Entries
**ADDRESS:** (Disambiguation)  
* A device address is all upper case, starts with 0, looks like 0VCPO8MYDRN2E6N5JDRHRLTVEVZSZYARM
* A wallet address is all upper case, looks like K7RMH5EFPZW67JTS5B5GA6PDZA4MYX4LY  
* A unit address, like a transaction you sent, looks like Ip241kkFRkZnaVF61Z6+/JW3YELOaHn9C6PAjeMp8fs=  
* A bitcoin address looks 13AM4VW2dhxYgXeQepoHkHSQuy6NgaEb94

**AIR-DROP:** The most recent snapshot for distribution of new bytes to wallets linked to proven BTC balances occurred on 4 November 2017 at 05:23 UTC. Track linking progress at [Byteball main site: transition pages](https://transition.byteball.org).  
* For each 1 BTC linked --> 6.25MB (0.00625GB)  
* For each 1 BTC linked --> 2.1111 x 6.25 million blackbytes (money supply of blackbytes is 2.1111 times as much as that of bytes)
* For every 10 (white)bytes on any address --> 1 new (white)byte
* For every 10 (white)bytes on a **linked** address --> 2.1111 new blackbytes.
  
See wiki [Airdrop](https://byteroll.com/airdrop) article

**ASSET:** Users can issue new assets and define rules that govern their transferability. For example: ICOs, shares, bonds, fiat-pegged coins, loyalty points, minutes of airtime, assets in online games, whatever you can imagine. You can do this in Byteball right now.  
See wiki article [Asset](http://byteroll.com/asset) for instructions

**ATOMIC EXCHANGE:** When two parties sign a single unit that executes both legs of the exchange, the two transactions either happen simultaneously or don't happen at all. It is no longer necessary to trust any centralized exchanges. Note this uses a definition of "atomic" related to databases, and has nothing to do with the usual "extremely small" senses.  
Ref [Official Byteball main site, front page, other features](https://byteball.org)

**AUTHOR:** (In explorer) Authors are addresses that signed the transaction, similar to bitcoin sending address.

**BBIP:** Byteball Improvement Proposal. Based on the Bitcoin BIP system. Very new, not yet off the ground.  
Ref [Github: BBIPs](https://github.com/byteball/BBIPs)

**BIND:** See CONDITIONAL PAYMENTS

**BLACKBYTE:** One of the two native Byteball currencies (also see BYTE). When you want complete privacy pay in blackbytes, a cash-like untraceable currency whose transactions are not visible on the public database. They are sent peer-to-peer instead.  
Ref [Official Byteball main site, front page](https://byteball.org)

**BOT:** See CHATBOT

**BOT STORE:** The section in the Byteball wallet, available from the CHAT tab, that contains about 20 bots you can add to your contacts.

**BYTE:** One of the two native currencies of the Byteball network (also see BLACKBYTE). It is also a basic general computing term: *A sequence of adjacent bits, usually eight, operated on as a unit, e.g. 0 1 0 1 1 1 0 1. One byte can store one character, e.g. 'A' or 'x' or '$'.* There are 10^15 total bytes in the system. The unit traded on exchanges is the "GByte", 10^9 bytes.

**BYTEBALL (NAME):** (1) Once a unit is broadcast into the network, and other users start building their units on top of it (referencing it as parent), the number of secondary revisions required to edit this unit hence grows like a snowball.  That’s why we call this design *Byteball* (our snowflakes are bytes of data). Ref [Byteball white paper, p5](https://byteball.org/Byteball.pdf)  
(2) *Byteball* (upper-case "B") is the name of the platform, the protocol; *byteball* (lower-case "b") is sometimes (mis)used very loosely to represent the currency, as in "Hey, I see a byteball [1 GB] is now worth $250!" or "Use blackbytes for private transactions and byteballs for the open ones".

**BYTEBALL COMMUNITY FUND:** The Byteball Community Fund's goal is to support and encourage the Byteball crypto platform. Fundraising is done by voluntary donations from investors, stakeholders and enthusiasts.  
See the wiki [Byteball Community Fund](https://byteroll.com/byteball-community-fund) article

**CASHBACK:** We want to get this coin into the hands of as many people as possible. In addition to the regular monthly airdrop, we are going to partner with several categories of companies, like merchants, payment processors, and "Bitcoin debit card" companies. We will offer 10% cashback, paid in Bytes, for all qualifying purchases (no matter how the purchases are paid).  
See the wiki [Cashback](https://byteroll.com/cashback) article

**CHAT:** The wallet incorporates a chat function, allowing the user to exchange end-to-end-encrypted messages using AES with a peer or bot when their devices are paired. It's end-to-end encrypted using AES. The hubs only forward the encrypted messages; they can't see anything.  To deliver your message, your wallet connects to the recipient's hub (if it's different from your home hub).  
See Tony's [2017-09-13 bitcointalk post](https://bitcointalk.org/index.php?action=profile;u=412662;sa=showPosts;start=0)

**CHATBOT:** You can pair your wallet with a bot, which emulates the experience of chatting with a live person. The bot will have very limited responses, but enough to get the job done. A merchant bot could allow you to select from a range of pizzas, for example, then pay for your order with two clicks while still in that chat session.  
See the wiki [Chatbot](https://byteroll.com/chatbot) article for a list

**CHILDREN:** (In explorer) Children and parents are pointers to later and earlier units in the DAG (see the arrows between units).

**CONDITIONAL PAYMENTS:** This is the **killer feature** of Byteball, and the "smart payments" in the slogan. You can choose to bind a payment to a condition. If/when that condition is satisfied the payment is unlocked and only the recipient can collect it. If the condition fails the payment can only be collected by the issuer.
See the wiki [Trading blackbytes](https://byteroll.com/trading/trading-blackbytes) article for an example.

**CONFIRMED:** A Byteball payment is confirmed/stable once it has become sufficiently buried by later transactions to be unalterable, similar to confirmations on a blockchain. It usually takes about five minutes.

**DAG:** Directed Acyclic Graph, the radically-different immutable data structure used in the Byteball platform instead of the usual cryptocurrency blockchain. No mining, no proof-of-work, no proof-of-stake. DAGs are also known by Git users. 
Ref [Byteball white paper PDF](https://byteball.org/Byteball.pdf)

**DISTRIBUTION:** See AIRDROP

**DOUBLE-SPEND:** In case of doublespend, the version that comes earlier on the main chain wins.  Therefore, if your node is well-connected and you see a few other txs piling up on top of the new unconfirmed tx, and the time since its arrival is significantly larger than the typical network latency, then you can be reasonably sure that even if a doublespend appears later it will be sorted later, hence voided.  
Ref [Slack comment](https://byteball.slack.com/archives/C30NVUJAD/p1507881016000333)  

**FULLY FUNDED:** The state of a SMART CONTRACT when the agreed payments from both parties have been locked in the contract until certain conditions are satisfied.  
See the wiki [Smart contract](https://byteroll.com/smart-contract) article

**GENESIS UNIT:**: The Byteball analog to Bitcoin's genesis block, including the creation of the entire global supply of its native currencies: 1,000,000 GBytes and 2,111,100 GBBytes.  

**GRANTS:** Byteball Grants Program. We’ll pay for work that improves the ecosystem. We want the contributors to both come up with new ideas and realize them.  
Ref [Medium article](https://medium.com/byteball/byteball-grants-program-906a71b93d3c)  
Ref [Slack #grants channel](https://byteball.slack.com/messages/C5XRQJQLS/)

**HUB:** This is a node for the Byteball network that serves as a relay, plus it facilitates the exchange of end-to-end encrypted messages among devices connected to the Byteball network. The hub does not hold any private keys and cannot send payments itself, nor can it read the messages. Users set their hub address in their wallet settings. The default hub is wss://byteball.org/bb but users can change it.  
Ref [Github readme.md](https://github.com/byteball/byteball-hub)  
See wiki [Hub](https://byteroll.com/hub) article

**LINKED:** Concerning airdrops, it refers to (white)bytes at one of your wallet ID addresses linked to bitcoins, where the link has been verified by the transition bot. All such pairs are visible on the [transition pages](http://transition.byteball.org).

**MOVED:** In your wallet history, shows funds moving from one address in your wallet to a different address in the same wallet. Either you deliberately sent them there, or they moved as change. See wiki article [Change address](https://byteroll.com/change-address)

**MULTI SIGNATURE:** For security, you can require that your funds be spendable only when several signatures are provided, e.g. from your laptop and from your phone. For shared control of funds, signatures from different people may be required.  
Ref [Official Byteball main site, front page, other features](https://byteball.org)

**NODE:** Imagine a fishing net: the nodes would be the knots holding the lines of rope together. Every device in the Byteball network is technically a node, whether a light client/wallet, a full wallet, a relay or a hub. Informally, node is used to mean full wallet.  
See wiki article [Node](https://byteroll.com/node) for different roles

**ORACLE:** A trusted third party that monitors specific external events and registers selected data-feed items to the Byteball database. An example is a list of cryptocurrency exchange rates updated every ten minutes. When dealing with untrusted counterparties, you can lock the funds on an address that is spendable either by you or by the counterparty, depending on the oracle's data collection and registration.  
Ref [Official Byteball main site, front page, other features](https://byteball.org)  
See wiki article [Oracle](https://byteroll.com/oracle) 

**P2P INSURANCE:** Insurance against a negative event provided by another peer instead of a faceless company. An example is flight delays insurance.  
Ref [Making P2P Great Again, Episode IV: P2P Insurance](https://medium.com/byteball/making-p2p-great-again-episode-iv-p2p-insurance-cbbd1e59d527)

**PAIR:** To link with another Byteball wallet, either remote or face-to-face, use a pairing code generated by either wallet. In your wallet, Chat > add a new device > invite the other device OR accept invitation. The code can then be sent by pasting its characters into a non-wallet messaging app, or by QR code.

**PARENTS:** (In explorer) Children and parents are pointers to later and earlier units in the DAG (see the arrows between units).

**POLL:** See VOTE

**PREDICTION MARKETS:** Prediction markets are (usually) exchange-traded markets created for the purpose of trading the outcome of events. The purpose of the [Slack](https://byteball.slack.com) #prediction_markets channel is "Finding a counterpart for bets on future events".  
See wiki article [Trading prediction markets](https://byteroll.com/trading/trading-prediction-markets)

**RECOVER:** To recover from seed (which is not the preferred option because blackbytes are not included) you need to use a full wallet. Also see RESTORE

**REGULATED ASSETS:** Regulated institutions can issue assets that are compatible with KYC/AML requirements. Every transfer of such asset is to be cosigned by the issuer, and if there is anything that contradicts the regulations, the issuer won't cosign.  
Ref [Official Byteball main site, front page, other features](https://byteball.org)

**RELAY:** This is a node for the Byteball network that stores the entire database and forwards new storage units to peers. The relay does not hold any private keys and cannot send payments itself.  
Ref [Github readme.md](https://github.com/byteball/byteball-relay)  
See wiki article [Node](https://byteroll.com/node) for different roles

**RESTORE:** To restore from a full backup which includes everything, you can use every wallet type. The restore function is right under the backup function in the wallet settings. Also see RECOVER

**SINGLE-ADDRESS WALLET:** These wallets have only one address and the change always returns to the same address, supporting applications that require a stable identity. For example, you can run a manual oracle right from your wallet without having to run a node on a server. This allows you to run a PREDICTION MARKET for a future event, enabling users to make contracts (bets) referencing your address as an oracle, and when the outcome of the event is known you post its result from your wallet.  
See the wiki [Wallet](https://byteroll.com/wallet) article

**SMART CONTRACT/WALLET:** See CONDITIONAL PAYMENTS  
See the wiki [Smart contract](https://byteroll.com/smart-contract) article

**STABLE:** See CONFIRMED

**STORAGE UNIT:** Byteball is a decentralized system that allows tamper proof storage of arbitrary data, including data that represents transferrable value such as currencies, property titles, debt, shares, etc. Storage units are linked to each other [cryptographically].  
Ref [Byteball white paper](https://byteball.org/Byteball.pdf)

**TEXTCOIN:** Sending Byteball funds from one's wallet through a text app, such as an email, Telegram, WhatsApp etc.  
See the wiki [Textcoin](https://byteroll.com/textcoin) article  

**TRANSACTION FEE:** The fee you pay is identical to the size of the data you want stored. So a storage unit that takes up 18,000 bytes in the distributed Byteball database will cost 18,000 (white)bytes to send there. Currently a usual transaction fee is maybe 500 bytes, with a blackbytes fee being maybe 1000 bytes or so. If 1GB = $750, then 1MB = $0.75, and 1KB (1000 bytes) = $0.00075. So that's less than 1/10 cent US.

**TRANSITION BOT:** After installing the wallet, chat with the Transition Bot to participate in the next distribution round. [This link](byteball:A2WMb6JEIrMhxVk+I0gIIW1vmM3ToKoLkNF8TqUV5UvX@byteball.org/bb#0000) will open automatically in your wallet when you click it.  
See the wiki article [Airdrop](https://byteroll.com/airdrop)

**UNIT** (In explorer) Unit is hash of data unit, similar to bitcoin transaction ID, but unit can have more than just a transaction.

**VOTE:** (In wallet Poll bot) A vote is a small transaction that pays to yourself and is signed by your most funded addresses; the weight of the vote is the combined balance of the signing addresses.  Note that there is a privacy concern because by making a vote you associate a number of your addresses. 

**WALLET:** A node/device on the Byteball network that allows the user to own private keys, own funds, and create transactions. A full wallet downloads the whole Byteball database, a light wallet doesn't. Note that a Bitcoin wallet doesn't have an address as such, and just acts as a container for bitcoin keypairs. While a Byteball wallet contains addresses, which act as sources and destinations for sending (storage) units, which include keypairs for funds in (white)bytes/blackbytes. Note also that a Byteball device on your computer or mobile may contain more than one wallet. 
See wiki article [Node](https://byteroll.com/node) for different roles, and  
see wiki article [Wallet](https://byteroll.com/wallet) for lots more details

**WITNESS:** A witness is a highly reputable user with a real-world identity, who stamps each transaction seen. There are 12 witnesses involved in every transaction. In exchange for the work involved, a witness collects part of the transaction fee. This list varies very little from transaction to transaction. If 11 witnesses say no to a bad transaction and 1 says yes, that witness gets deemed unreliable and effectively fired. It would be unthinkable for all 12 to collude and allow a fraudulent transaction through. In this way the network is safeguarded.  
Ref [Byteball white paper PDF](https://byteball.org/Byteball.pdf)

**ZWIB:** The first Byteball new asset traded on Slack. The name comes from the unit address, *ZW1b...*  
See the wiki [Asset](https://byteroll.com/asset) article

-----
**This "Glossary" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
