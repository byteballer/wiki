<!-- TITLE: Tangos -->
<!-- SUBTITLE: A new asset/token on the Byteball platform, designed to be easily spread around. -->
Practise receiving them, sending them to others, making textcoins with them, exchanging them with other assets in smart contracts, etc. It doesn't matter if you mess up somehow as they are worthless.
# It takes two to Tango
So spread them around! They have no monetary value, since there are enough for 8 billion people to have 125,000 each. You can always get more.

There isn't much you can do with a wallet on a single device. But if you have separate (not multisig) wallets on two or more devices, maybe a laptop and a cellphone, you can pair them and execute smart contracts between them, which is excellent practice.

Each transaction will cost a regular transaction fee of 500-1000 bytes. If 1 GB = $1000, 1 MB = $1, 1 KB (1000 bytes) = 1/10 cent.

## Change addresses
Be aware of [change address](https://byteroll.com/change-address)es and how they work. The usual time you would first notice this is when you send some of your nice new Tangos to a friend and then cannot send any more anywhere for 5-10 minutes.

# Smart contract
Here's how to use Tangos in a smart contract. Let's say you are exchanging 10,000 Tangos for 75 X's (KB, Zingos, whatever) with a peer.

## Pairing
* One party gives the other a pairing code, either written out in full or via QR code. Generate this in byteball wallet chat by clicking on "+ Add a new device" then "Invite the other device". It will look something like <code>AhHPXCpCSTzD1CF53ELGTAsZ6MCA8Ogvk+koyibfPt/2v@byteball.org/bb#GxVBizZ2DSEf</code>. The other party in byteball wallet chat clicks on "+ Add a new device" then "Accept invitation from the other device" and pastes the pairing code into the almost-invisible line above the 'pair' button.

## After pairing

1. In chat, peer sends you a receive address.  
2. You left-click the address, and click "Pay to this address".  
3. From the drop-down menu, select Tangos. Click "bind this payment to a condition". Use "I receive another payment" as the condition. Adjust the "take-back" time if you wish. Fill in the amount of 75 X's. Click "Bind payment".  
4. Fill in your payment amount of 10,000 Tangos and click Send. Or click Cancel if you were just trying it out.  
5. Peer reads the contract and if it's what was agreed, clicks Pay/Send. Done deal, both payments occur at once.

## Notes
* See the new smart wallet by selecting it right at the top of the screen to the right of the main wallet's name. 
* If you just received Tangos, then the smart wallet won’t have any (white)bytes in it yet to cover the sending fee. Click the Receive tab, copy the address, then paste it into the Send screen of your regular bytes wallet. Send maybe 5000 bytes to cover any possible transaction fee, then after you've moved the Tangos you can send the remaining bytes back to your main wallet.
* You can see the details of the smart (wallet) contract by clicking the little eyeball to the right of the smart contract home screen.
* Be careful to make payments from the correct wallet -- maybe you were looking in a smart wallet and forgot to get back into your main wallet.
* You may get an error message if the funds you are trying to send haven't confirmed yet.
* It is best not to send other payments in the 5-20 minutes while funding and waiting for execution of a smart contract. Additional payment notifications in the chat, and pending confirmations in the wallet home screen, can be confusing.
# New tokens in your wallet
(Copied from [Asset](https://byteroll.com/asset))

You started off with 0 bytes in your wallet. Later, you probably got some bytes, and maybe some blackbytes. At present, maybe some Tangos and/or something else. If you click the Send tab, you'll see you a drop-down menu, and you can spend from any of your asset types.

Note that these listed asset/token types remain, even if the balance of one particular asset/token is zero. So every time someone receives some Bongos, the Bongo asset/token type gets added to their wallet asset/token list, indelibly so for now. After a few of these, if you don't really want them, they act as spam.

So, please don't send your new assets/tokens to someone without asking them first if they want them.

# Definition
[Source](https://explorer.byteball.org/#0Qki2BWSJ80dMN58Dq1rrJugaYyFndxkZloLJby+olU=)  
cap: 1,000,000,000,000,000  
is_private: false  
is_transferrable: true  
auto_destroy: true  
fixed_denominations: false  
issued_by_definer_only: true  
cosigned_by_definer: false  
spender_attested: false

# Registry publication
[Source](https://explorer.byteball.org/#ammQoP5B+hpm5wX2EVjro6WGLVDePxUoxXAMhIKq5yE=)  
asset: 0Qki2BWSJ80dMN58Dq1rrJugaYyFndxkZloLJby+olU=  
decimals: 0  
name: Tangos by Jore Bohne  
shortName: Tangos  
issuer: Jore Bohne  
ticker: TANGO  
description: A token of no monetary value to practise with, to be spread around, not worth hoarding because there are so many of them.


-----
**This "Tangos" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)