<!-- TITLE: Cashback -->
<!-- SUBTITLE: A quick summary of Cashback -->

# Cashback API
from https://bitcointalk.org/index.php?topic=1608859.msg21130417#msg21130417 post 13043

Cashback API for merchants is ready https://github.com/byteball/byteball/wiki/Cashback-API  
A few merchants are already integrating.
# Announcement
from https://bitcointalk.org/index.php?topic=1608859.msg20686283#msg20686283 post 12180

After August 7 we'll reach more than 50% distributed coins and will start a new phase of our distribution.

It was stated from day one that the purpose of this distribution is to get this coin into the hands of as many people as possible.  Both coins (bytes and blackbytes) are meant to be used as currencies, and this is only possible when there are many users and apps to interact with.  We already have a sizable community, a number of unique apps, and we are the only crypto project to have an app distribution platform (the Bot Store), but there is still a lot of room to grow in terms of user count, number of apps, and willingness of users to use these apps.

Up until now, we were distributing only to holders of BTC and Bytes, i.e. we were rewarding holding.  Now we are adding actual users into the mix, i.e. we are going to reward transactions.

To get you an idea of how we are going to do it, we are going to partner with several categories of companies:
- merchants
- payment processors
- "Bitcoin debit card" companies 
 
and offer 10% cashback, paid in Bytes, for all qualifying purchases (no matter how the purchases are paid).  The cashback will be funded from the undistributed pool.  For example, a customer who bought for $100 receives $10 cashback in Bytes, paid to his Byteball address, at the current exchange rate.  For merchants, this is something that would drive sales and they would put effort into promoting the offer.  In competitive industries, a 10% cashback is a very powerful tool to lure customers.  For customers, this is a 10% discount (which matters a lot in some industries).  For Byteball, it is new users who will have to get involved into the system in order to receive the cashback.

A few companies have already expressed interest (not disclosing the names while it is a work in progress).

A few extensions of this offer:
- 20% cashback if the purchase is paid in bytes or blackbytes.  This would incentivize merchants to start accepting bytes and blackbytes, and the infrastructure will stay after the distribution ends.
- merchants can offer additional cashback to their customers.  Merchants fund it themselves by buying bytes from the market, and for every 1% funded by the merchant we add 1% more from the undistributed pool.

If you see similarity with existing loyalty points schemes, it is similar indeed.

At a minimum, we receive many new users who learn about Byteball from their merchants, plus working payment integrations.  And the users are not just crypto fans, it may be their first crypto coin for many users.  With the most user friendly wallet in the industry, we are in the best position to expand beyond the crypto village.

We can continue adding 10-20% to existing byte balances to incentivize keeping the received bytes before more infrastructure is built, rather than cashing out immediately.  

Two negative sides:
- the scheme is less transparent than plain adding on top of existing balances, and some share of fraud is inevitable.  Merchants might try to deceive us to receive coins for themselves by reporting nonexistent sales or selling to themselves.  This is mitigated by good choice of trustworthy merchants and our ability to disconnect any merchant at any time on suspicion of fraud.  Their customers can also try to find ways to abuse the system, again we'll require the merchants to prevent that by excluding some types of purchases, monitoring customers, enforcing caps, etc.  Additionally, if the merchant funds part of the cashback himself, he has skin in the game to counter the customer fraud.
- these new users are not holders for the most part, they are more likely to sell.  Not a big problem, the point is they already know about Byteball and it's easy for them to get back.  The new users are new to crypto, many of them won't use exchanges, and somebody will have to create new easier-to-use channels to fiat, which is positive for liquidity.  Also, 10%-20% monthly distributions discourage fast selling while the distribution is ongoing.  And lastly, the merchants who fund 50% of the cashback would partially balance the markets by buying coins.

On balance, I'm sure that these negatives are tolerable when we are going to achieve a vast expansion of our user base and acceptance at merchants.

For the current distribution round [August 7], nothing changes.

For the next distribution on September 6, we are changing the ratios in favor of Byte holders and slowing down the distribution to have more time to build out the cashback program:

BTC to Bytes: every 160 BTC gives you 1 GB from the distribution (or 0.00625 GB per BTC).
Bytes to Bytes: every 10 GB of existing balance gives you additional 1 GB from the distribution (in other words, +10% to existing balances).

Similar ratios for blackbytes.

For those who receive their first bytes from cashbacks, Sep 6 will be the first distribution when they receive +10%.

I'll make announcements as we add merchants in the cashback program.
# Application form
Ref [Sept 12, 2017 Medium article](https://medium.com/byteball/byteball-cashback-program-9c717b8d3173), along with further description.
# Merchants
[Premiumize](https://www.premiumize.me): Premium downloads, cloud torrents, and VPN.  They also accept payments in Bytes.

[NODEshare](http://nodeshare.in): Masternode as a service.  They also accept payments in Bytes and offer merchant match for large orders.

[Osteria il Trullo restaurant in Milan](https://www.tripadvisor.com/Restaurant_Review-g187849-d1084589-Reviews-Il_Trullo-Milan_Lombardy.html) (owner @hjxi at the [Byteball slack](http://slack.byteball.org/)) PLUS 12 local businesses in Milan using a central distribution point (ref https://byteball.org/milan)

[Dr. Terence Lee (Fertility Care of Orange County)](http://fcare.com/): Dr. Lee is a long time cryptocurrency enthusiast and is famous for the first Bitcoin baby  
Ref http://money.cnn.com/2013/06/10/technology/innovation/bitcoin-baby/index.html

[JoJo Savard](http://jojosavard.com/contact/): a Canadian television psychic

[BTC Germany](https://www.btcgermany.de/): a fast growing cryptocurrency blog

[Yopey](https://www.airbnb.jp/rooms/10998033): an airbnb host in Tokyo 

[Marigona Dent](https://www.facebook.com/marigonadent): a dental clinic in Kosovo 

[Kanzlei Hellinger](https://hellinger.eu): a German law firm offering tax and legal support, including in cryptocurrency matters. They will also accept Bytes for payment.

[LP](http://lp-kyoto.jp/): a hair salon in Kyoto, Japan 

[Yamauchi-Dou](https://store.shopping.yahoo.co.jp/yamauchidou/): a store in Japan

[Stardelta Audio Mastering](https://www.stardeltamastering.com/): a Grammy-winning music studio in Devon, England

[HJC's Online Shop](https://hjcs.myshopify.com/blogs/news/cashback-program-is-here): online store in Canada

[DoYouCrypto](https://doyoucrypto.com): an online store for crypto fans

## Central distribution points
This system is being piloted by Osteria il Trullo restaurant above, owner @hjxi at Slack. He's the Byteball expert there. There are four Byteball sticker types that stores in the scheme can display, depending on their degree of involvement:

**With valid receipt, get 10% in digital money:** A customer of this store can take his purchase receipt to the distribution center and get 10% cashback in bytes. This store does not offer a discount.

**Bytes distribution point:** A customer from a nearby recruited business presents his purchase receipt here by the end of the next working day. The staff give the customer his 10% cashback free bytes, and help with downloading a wallet if needed.

**Bytes distribution point + 10-20% cashback:** (1) Staff here perform the distribution center functions. (2) Customer will get 10% cashback bytes on the spot, 20% if purchase is made using bytes (from a wallet).

**Bytes accepted here:** In this store, you can pay in bytes. This is a general Byteball sticker, not particularly related to the cashback program.

Note that a merchant in the first group need know little about Byteball. His only involvement is displaying the sticker and directing customers to the distribution center. His incentive to join the scheme is the potential increase in patronage from customers who want the free 10% cashback.

# Merchant services
## Payment gateways

A payment gateway is a merchant service provided by an e-commerce application service provider that authorizes direct payments processing for e-businesses, online retailers, bricks and clicks, or traditional brick and mortar. The payment gateway may be provided by a bank to its customers, but can be provided by a specialised financial service provider as a separate service, such as a payment service provider.  
[Ref Wikipedia](https://en.wikipedia.org/wiki/Payment_gateway)

### Byteball for Web Merchants

As a payment service provider, Byteball for Web Merchants facilitates a payment transaction in Bytes by the transfer of information between a merchant payment portal (such as a website, mobile phone) and its front end payment processor. Multi-currency input (BTC, USD, EUR) to Bytes output.  
Website: https://byteball-for-merchants.com/  
Github repo: https://github.com/ByteFan/byteball-for-web-merchant/

## Plug-ins
### Wordpress *WooBytes Gateway*
Main features:  
* powered by Byteball for Merchants payment gateway  
* fully integrates Byteball cashback program  
* multi-currency (Bytes, BTC, EUR, USD, AUD, BRL, CAD, CHF, CLP, CNY, DKK, GBP, HKD, INR, ISK, JPY, KRW, NZD, PLN, RUB, SEK, SGD, THB, TWD)  
* anonymous: no api key needed  
* extremely simplified setup  
* includes a Woocommerce integrated optional debugging tool  
* compatible with other Woocommerce gateways.

https://wordpress.org/plugins/woobytes-gateway/

# Textcoin

Lastly, our own cashback program now supports [textcoins](https://byteroll.com/textcoin). Merchants don't have to update their order forms to ask users about their Byteball address: instead they can give us the user's email address and we send a textcoin.

Updated API description: https://github.com/byteball/byteball/wiki/Cashback-API

-----

**This Cashback page main author: @tonych at the** [Byteball slack](http://slack.byteball.org/)  
**Payment gateways & central distribution points added by @slackjore at the** [Byteball slack](http://slack.byteball.org/)