<!-- TITLE: Family Tokens -->
<!-- SUBTITLE: A token economy system updated for the crypto age-->

# Overview
[video](https://vimeo.com/255212862){.vimeo}

A token economy system is one of the fastest and most effective ways to get kids to follow the rules. Similar to a traditional reward system, kids earn tokens throughout the day. Then, tokens can be exchanged for bigger rewards.[[1](#references)]

Traditional ways of doing this include stickers on wall-charts and tokens such as special marbles kept in jars. Our system uses Byteball wallets on the family members' personal cellphones. Let's pretend the Smart family is Mom, Dad, Bobby, Jane, and Rusty the dog. Jane Smart has a separate wallet for Rusty.

# Set-up
## Create the tokens
Mom uses some tokens from the fun-coins faucet, or preferably creates 1000 personalised "Credits by Mom Smart" (or whatever)[[2](#references)], stores them in the "Treasury" wallet, and is mostly in charge of distributing them.

## Everyone has a wallet
It is assumed each of the humans has a smartphone already. If not, they can be bought very cheaply. In ASDA (big UK supermarket), one running Android 7.0 goes for £35, or one running Android 5.1 for £20. Both work perfectly for Byteball, including cameras for scanning QR codes. Use single-address wallets to reduce confusion.

* **Mom:** Personal "mom" plain wallet; Treasury 1of2 multisig wallet  
* **Dad:** Personal "dad" plain wallet; Treasury 1of2 multisig wallet  
* **Bobby:** Personal "Bobby" plain wallet  
* **Jane:** Personal "Jane" plain wallet; "Rusty" plain wallet.

## Address list
Each person has the following named addresses in their wallet address lists: Treasury, Mom, Dad, Bobby, Jane, Rusty. This makes it easy to move the tokens around.

### Pairing
It is not essential, but there are benefits if everyone pairs phones with each other. Apart from the encrypted chat, one can then use smart contracts if desired.

One party gives the other a pairing code, either written out in full or via QR code. Generate this in Byteball wallet chat by clicking on "+ Add a new device" then "Invite the other device". It will look something like `AhHPXCpCSTzD1CF53ELGTAsZ6MCA8Ogvk+koyibfPt/2v@byteball.org/bb#GxVBizZ2DSEf`. The other party in Byteball wallet chat clicks on "+ Add a new device" then "Accept invitation from the other device" and pastes the pairing code into the almost-invisible line above the "pair" button.

## Transaction fees
Every transaction that moves "Credits" from one wallet to another will cost maybe 1000 bytes. Make sure everyone has enough. If the kids waste their bytes, they can buy more with regular money. But note that "Credits" cannot be bought for regular money.

# How it works
The exact system of what behaviours earn how many tokens and what rewards these tokens can be exchanged for (family trips to the movies etc) will need to be worked out by the family concerned.

See, for example, the Very Well Family website for some ideas.

## Individual and group tokens
* Bobby mows the lawn, and Jane does the laundry, each receiving tokens per the agreed list.  
* The kids get tokens for being friendly to each other, i.e. not fighting, for a whole day. They get the same number to each wallet. The group tokens encourage peer pressure: "If we fight we don't get the 20 credits today, so just chill out, all right?"

## Individual and group rewards
* The kids spend their tokens on individual rewards: Bobby wants to stay up late to watch a TV show, and Jane wants to try some of Mom's new make-up.  
* The kids cash them in to go on a family outing to the theme park.

## Tokens from Treasury
When tokens are earned, per the family system, a parent sends them from Treasury to the kid's wallet. This can include use of a [personal oracle](https://byteroll.com/oracle#personal-oracle), if you wish.

## Tokens to Treasury
When tokens are exchanged for rewards, the kid sends the appropriate number to the Treasury wallet.

## Explorer
Since all the relevant wallet addresses are single-address, and these addresses are known, everyone can see all the relevant token transactions.

# The dog?
The dog is included for family harmony.

# See also
* [Very Well Family website](https://www.verywellfamily.com/create-a-token-economy-system-to-improve-child-behavior-1094888)  
* https://byteball.market To create your own family token in 5 minutes

# References
1. https://www.verywellfamily.com/create-a-token-economy-system-to-improve-child-behavior-1094888
2. https://byteball.market

-----
**This "Family tokens" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
