<!-- TITLE: Videos -->
<!-- SUBTITLE: Some general (non-technical) videos in English about Byteball -->

Format is intentional to allow viewing on mobile screens.


# YouTube
![Coininterview 80 Hi](/uploads/videos/coininterview-80-hi.jpg "Coininterview 80 Hi"){.align-right}
**Date uploaded:** 2016-10-14
**Channel:** Coin Interview
**Title:** Coin Interview Episode 27: Anton Churyumov of Byteball .org
**Length:** 58:16
**URL:** https://www.youtube.com/watch?v=zjT7wQNg_s4
**Notes:** Talking heads throughout but mainly Tony; 3940 views at 2017-11-23

![Marcdemesel 80 Hi](/uploads/videos/marcdemesel-80-hi.jpg "Marcdemesel 80 Hi"){.align-right}
**Date uploaded:** 2016-12-23
**Channel:** Marc De Mesel
**Title:** Byteball: Scalable, Private, Free but HURRY!!
**Length:** 9:03
**URL:** https://www.youtube.com/watch?v=bPicDP-ehfU
**Notes:** Talking head throughout; 4446 views at 2017-11-23

![Dashmeister Amazing 80 Hi](/uploads/videos/dashmeister-amazing-80-hi.jpg "Dashmeister Amazing 80 Hi"){.align-right}
**Date uploaded:** 2017-04-26
**Channel:** DashMeister
**Title:** Byteball: The most amazing crypto for free
**Length:** 5:03
**URL:** https://www.youtube.com/watch?v=IFP0rPgMu_Q
**Notes:** Narrated; shots from coinmarketcap and Byteball main page; 8708 views at 2017-11-23

![Cryptojack Bb 1 80 Hi](/uploads/videos/cryptojack-bb-1-80-hi.jpg "Cryptojack Bb 1 80 Hi"){.align-right}
**Date uploaded:** 2017-06-03
**Channel:** Crypto Jack
**Title:** BYTEBALL- What it is -Part 1. ( Features)
**Length:** 3:56
**URL:** https://www.youtube.com/watch?v=dNLLB-qJMi4
**Notes:** Talking head throughout; 697 views at 2017-11-23

![Cryptojack Bb 2 80 Hi](/uploads/videos/cryptojack-bb-2-80-hi.jpg "Cryptojack Bb 2 80 Hi"){.align-right}
**Date uploaded:** 2017-06-03
**Channel:** Crypto Jack
**Title:** BYTEBALL-What it is-Part 2. (Smart contracts)
**Length:** 5:52
**URL:** https://www.youtube.com/watch?v=inQCSG2zbl0
**Notes:** Talking head throughout; 298 views at 2017-11-23

![Cryptojack Bb 3 80 Hi](/uploads/videos/cryptojack-bb-3-80-hi.jpg "Cryptojack Bb 3 80 Hi"){.align-right}
**Date uploaded:** 2017-06-03
**Channel:** Crypto Jack
**Title:** BYTEBALL-What it is. ( part 3)
**Length:** 5:03
**URL:** https://www.youtube.com/watch?v=LkcvQB5ntRk
**Notes:** Talking head throughout; 272 views at 2017-11-23

![Cryptojack Vs 80 Hi](/uploads/videos/cryptojack-vs-80-hi.jpg "Cryptojack Vs 80 Hi"){.align-right}
**Date uploaded:** 2017-06-04
**Channel:** Crypto Jack
**Title:** BYTEBALL vs ETHEREUM & BITCOIN
**Length:** 3:23
**URL:** https://www.youtube.com/watch?v=wLWVqDezA_g
**Notes:** Talking head throughout; 1524 views at 2017-11-23

![Yestur 80 Hi](/uploads/videos/yestur-80-hi.jpg "Yestur 80 Hi"){.align-right}
**Date uploaded:** 2017-06-05
**Channel:** Yestur Junak
**Title:** How to Get free Byteball by linking bitcoins for airdrop distribution
**Length:** 4:12
**URL:** https://www.youtube.com/watch?v=e-FzQRNODAA
**Notes:** Narrated; shows details of transition bot and signing a message in Electrum; 1713 views at 2017-11-23

![Dashmeister Transition 80 Hi](/uploads/videos/dashmeister-transition-80-hi.jpg "Dashmeister Transition 80 Hi"){.align-right}
**Date uploaded:** 2017-06-07
**Channel:** DashMeister
**Title:** How to get free Byteball step-by-step (transition bot)
**Length:** 3:14
**URL:** https://www.youtube.com/watch?v=igJka8q69RA
**Notes:** Narrated; shots of Byteball main page and transition bot; 4856 views at 2017-11-23

![Wekkel 80 Hi](/uploads/videos/wekkel-80-hi.jpg "Wekkel 80 Hi"){.align-right}
**Date uploaded:** 2017-06-11
**Channel:** Wekkel Ekkel
**Title:** Byteball - Making Byteball Payments
**Length:** 6:21
**URL:** https://www.youtube.com/watch?v=6VI_-JIDyUI
**Notes:** Music; no narration but some onscreen explanations; handheld video of monitor showing byte and blackbyte payments; 568 views at 2017-11-23

![Heymonkey Videoscribe 80 Hi](/uploads/videos/heymonkey-videoscribe-80-hi.jpg "Heymonkey Videoscribe 80 Hi"){.align-right}
**Date uploaded:** 2017-06-26
**Channel:** h3ym0nk3y
**Title:** Join Byteball
**Length:** 1:48
**URL:** https://www.youtube.com/watch?v=fUd5LM9C6AM
**Notes:** Music; no narration; VideoScribe presentation of basic Byteball features; 463 views at 2017-11-23

![Heymonkey Bb 80 Pxh](/uploads/videos/heymonkey-bb-80-pxh.jpg "Heymonkey Bb 80 Pxh"){.align-right}
**Date uploaded:** 2017-06-29
**Channel:** h3ym0nk3y
**Title:** byteball
**Length:** 0:24
**URL:** https://www.youtube.com/watch?v=IiXBiwVCFOI
**Notes:** No sound; nifty rotating graphic of 3D "Byteball" made of 0s and 1s; 113 views at 2017-11-23

![Mike 80 Hi](/uploads/videos/mike-80-hi.jpg "Mike 80 Hi"){.align-right}
**Date uploaded:** 2017-07-02
**Channel:** Mike    
**Title:** How to digitally sign your Bitcoin to get free Byteball!
**Length:** 8:31
**URL:** https://www.youtube.com/watch?v=0zTuzZNzEd0
**Notes:** Narrated; shows chatting with transition bot and signing Electrum message; 3564 views at 2017-11-23

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2017-12-02 **Channel:** Marc De Mesel **Title:** Interview with Byteball founder Tony 😎❤ Part 1: Why DAG Better Than Blockchain? **Length:** 23:31 **URL:** https://www.youtube.com/watch?v=M-gmFnh24Lc **Notes:** Talking heads throughout; background noise; 1042 views at 2017-12-07

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2017-12-05 **Channel:** Marc De Mesel **Title:** Interview with Byteball founder Tony 😎❤ Part 2: How Private are Blackbytes? Spend Crypto with Cashback! **Length:** 19:47 **URL:** https://www.youtube.com/watch?v=PAB6K85QmnQ **Notes:** Talking heads throughout; background noise; 313 views at 2017-12-07

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2017-12-06 **Channel:** Marc De Mesel **Title:** Interview with Byteball founder Tony 😎❤ Part 3: What Crypto Can & Fiat Not: Conditional Payments **Length:** 15:33 **URL:** https://www.youtube.com/watch?v=71QadbG-sSM **Notes:** Talking heads throughout; background noise; 215 views at 2017-12-07

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2017-12-13 **Channel:** Marc De Mesel **Title:** Byteball Wallet Tips & Tricks 😉 👌👍 **Length:** 12:19 **URL:** https://www.youtube.com/watch?v=oGxOQAS54vk **Notes:** Talking heads throughout; with gorgeous beach and girl; 274 views at 2017-12-13

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2017-12-16 **Channel:** Marc De Mesel **Title:** Part 2 - Byteball Wallet Tips & Tricks - Caveats & Cows 😉😚 **Length:** 7:40 **URL:** https://www.youtube.com/watch?v=OUzIfBhRDnA **Notes:** Talking head throughout; with gorgeous beach and cattle [sic]; 146 views at 2017-12-16

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2017-12-22 **Channel:** A.C. **Title:** The ground is shifting **Length:** 0:27 **URL:** https://www.youtube.com/watch?v=VIOBsmLA6jo **Notes:** Text onscreen; upbeat music; 46 views at 2017-12-22

![(image)](/uploads/videos/mdm1-80-hi.jpg "MDM1 80 Hi"){.align-right} **Date uploaded:** 2018-01-21 **Channel:** The Billion Dollar Secret **Title:** Exclusive Interview with the Byteball Founder Tony Churyumoff (Part 1) 🔒 Legends of Crypto **Length:** 31:05 **URL:** https://www.youtube.com/watch?v=tB0AUpiF9B4 **Notes:** Talking heads throughout; 610 views at 2018-01-23
# Vimeo
![Slackjore 1 80 Hi](/uploads/videos/slackjore-1-80-hi.jpg "Slackjore 1 80 Hi"){.align-right}
**Date uploaded:** 2017-06-12
**Channel:** Jore Bohne
**Title:** Byteball: 1. Smart payments made simple
**Length:** 2:13
**URL:** https://vimeo.com/221317149
**Notes:** Narrated with subtitles; shows live wallet chat of buying zwibs with MB; 2278 views at 2017-11-23

![Slackjore 2 80 Hi](/uploads/videos/slackjore-2-80-hi.jpg "Slackjore 2 80 Hi"){.align-right}
**Date uploaded:** 2017-06-14
**Channel:** Jore Bohne
**Title:** Byteball: 2. Betting on random numbers
**Length:** 4:03
**URL:** https://vimeo.com/221655112
**Notes:** Narrated with subtitles; shows live betting and using oracle feed; 59 views at 2017-11-23

![Slackjore 3 80 Hi](/uploads/videos/slackjore-3-80-hi.jpg "Slackjore 3 80 Hi"){.align-right}
**Date uploaded:** 2017-06-17
**Channel:** Jore Bohne
**Title:** Byteball: 3. Betting on flight delays
**Length:** 4:10
**URL:** https://vimeo.com/222006250
**Notes:** Narrated with subtitles; shows live betting, using flightstats data and oracle feed; 120 views at 2017-11-23

![Slackjore 4 80 Hi](/uploads/videos/slackjore-4-80-hi.jpg "Slackjore 4 80 Hi"){.align-right}
**Date uploaded:** 2017-06-20
**Channel:** Jore Bohne
**Title:** Byteball: 4. The wiki
**Length:** 2:48
**URL:** https://vimeo.com/222427604
**Notes:** Narrated with subtitles; shows live views of various wiki articles; 28 views at 2017-11-23

![Slackjore 5 80 Hi](/uploads/videos/slackjore-5-80-hi.jpg "Slackjore 5 80 Hi"){.align-right}
**Date uploaded:** 2017-06-23
**Channel:** Jore Bohne
**Title:** Byteball: 5. Forums
**Length:** 5:36
**URL:** https://vimeo.com/222903592
**Notes:** Narrated with subtitles; shows live views of our sub-Reddit, Bitcointalk thread and Slack channels; 21 views at 2017-11-23

![Slackjore 6 80 Hi](/uploads/videos/slackjore-6-80-hi.jpg "Slackjore 6 80 Hi"){.align-right}
**Date uploaded:** 2017-07-04
**Channel:** Jore Bohne
**Title:** Byteball: 6. Making a dead-man contract
**Length:** 4:41
**URL:** https://vimeo.com/224224884
**Notes:** Talking heads throughout; noisy; 992 views at 2017-12-06


-----
**This "Videos" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)