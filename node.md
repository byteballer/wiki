<!-- TITLE: Node -->
<!-- SUBTITLE: A general summary, not too technical -->
## Roles of different types of nodes

| Capability                        | Hub | Relay | Full W | Light W |
|-----------------------------------|-----|-------|--------|---------|
| Performs full validation          | y   | y     | y      |         |
| Relays units                      | y   | y     | y      |         |
| Stores and forwards e2ee messages | y   |       |        |         |
| Owns funds, owns private keys     |     |       | y      | y       |
| Creates transactions              |     |       | y      | y       |
| Accepts incoming connections      | y   | y     | y/n    |         |
| Serves light clients              | y   |       |        |         |

Abbreviations are used so the table is viewable on most mobile screens:  
e2ee = end-to-end encrypted  
W = wallet 

Ref [Google docs spreadsheet](https://docs.google.com/spreadsheets/d/11sZXCJEebtIrcOeMWTvjqbEua-WURsIxCl5EYxscctI/edit#gid=0)

### Hub
See wiki article [Hub](https://byteroll.com/hub)


-----
**This "Node" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)  
**Data from @tonych at the** [Byteball slack](http://slack.byteball.org/)

