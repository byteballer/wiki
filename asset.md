<!-- TITLE: Asset -->
<!-- SUBTITLE: New assets/tokens created on the Byteball platform, this article not too technical -->
**"Asset" in financial community = "token" in non-financial community.**
# From white paper
> Users can issue new assets and define rules that govern their transferability. The rules can include spending restrictions such as a requirement for each transfer to be cosigned by the issuer of the asset, which is one way for financial institutions to comply with existing regulations. Users can also issue assets whose transfers are not published to the database, and therefore not visible to third parties. Instead,the information about the transfer is exchanged privately between users, and only a hash of the transaction and a spend proof (to prevent double-spends) are published to the database.

Ref [White paper abstract](https://byteball.org/Byteball.pdf)

# Byteball Asset Manager
1. Order your new asset from https://byteball.market.
2. Its name will be the address of the unit that sent it, e.g. ZW1b.... No custom names or fancy rules yet^.
3. Let's say you are exchanging 10 Zwibs for 10 KB with a peer. In chat, peer sends you a receive address.
4. You left-click the address, and click "Pay to this address".
5. From the drop-down menu, select Zwibs. Click "bind this payment to a condition". Click "I receive another payment" as the condition. Fill in the amount of 10 KB. Click "Bind payment".
6. Fill in your payment amount of 10 Zwibs and click Send. Or click Cancel if you were just trying it out.
7. Peer reads the contract and if it's what was agreed, clicks Pay/Send. Done deal, both payments occur at once. 

^Custom names can now be set [2018-01-07] that will show in the wallet.

## Name registries
### @pmiklos
The registry is operated by Peter Miklos @pmiklos who also runs Byteball Asset manager at https://byteball.market/ and a similar bot.

Here is the [registration policy](https://docs.google.com/document/d/e/2PACX-1vQnpiwTipnBgrhSJcELOYYAOa3mTLZbmLmOebbtHFJFrfgHtlsNNZ9MPEGafvtuTnVAyfWukwu_hYSB/pub).

One of its [recent registration records](https://explorer.byteball.org/#IYeNVOvVeP8X9hDx3LgPu48MCTVbeo8S/NJM0vIAY14=)

Once a name is registered, it becomes visible  in the wallets updated to a recent version (2.0+).

Contact Peter to assign human readable names to your assets.

## Auto-destroy
If you check this box, it means that any new asset/token sent to its defining address become unspendable, like sending something to a black hole.

## Other attributes
Some of the attributes don't make sense to set differently to the default if a 3rd party is used for asset creation.

If `Issued by definer only` was false that would mean anyone could issue new assets, but then how much would the original requestor get? And who would be the issuer?

If `Co-signed by definer` was true, Byteball market would have to sign all transactions of that asset.

`Spender attested` could be true, but that would require listing all accepted attestors. Currently the byteball.market owner doesn't know anyone doing attestation on the Byteball platform, so he didn't find it important to support yet.

## New assets

* Clicking the link will take you to the unit page, where you can see how the asset/token is defined.

Some examples:

| Asset | Contact | Notes |
|-------|-------|-------|
| [Zwibs](https://explorer.byteball.org/#ZWlbZhl6C9IsV3biDC+ngzsLq58mQ0hbwWyLBLpFLQc=) | @slackjore | first new asset on platform   |
| [CK1](https://explorer.byteball.org/#3kc7H8A2oiWr9mv7AcWJeLCA0Cp8c3BLK04kYQ+5pfU=)  | @cryptkeeper     | community asset with membership-like features like special discounts at webshops   |
| [Scamcups](https://explorer.byteball.org/#LScaMCupXYOxp7hJpo6XfHqyAdHVtcvpU9uaLrAV+oI=) | @chainsaw | first asset with **great** random name in hash (LScaMCup...) |

## Titan Coin
See [Wiki article Chatbot § TitanCoin ICO](https://byteroll.com/chatbot#TitanCoin-ICO) for details
## Just do it
You can simply send your new (unrestricted) assets/tokens to **any** Byteball address, with no chat or contract needed. Copy the address, open the send tab, find the asset/token in your drop-down box, paste the address, enter the amount (in whole numbers), and click Send. Perfect for confusing people who aren't expecting to receive 25 Zingos in their wallet!

Caution: Be careful to send the right asset/token. Don't send someone 4 GB by mistake when you mean to send 4 Zingos. Not joking. Double-check.

### Textcoin
With wallet version 2.1, you can send any public asset -- including your newly-created one -- by [textcoin](https://byteroll.com/textcoin).
## Transaction fees
Usual transaction fees apply. So sending 9 Zingos somewhere will cost 500 bytes, or whatever. At 1GB = $600, 1MB = $0.60, 1KB = $0.0006, and 500 bytes = $0.0003 so you won't go broke.

## Change address
See the wiki [change address](https://byteroll.com/change-address) article if you're not sure how these work and apply here.

## New assets/tokens in your wallet
You started off with 0 bytes in your wallet. Later, you probably got some bytes, and maybe some blackbytes. At present, maybe some Zwibs and/or something else. If you click the Send tab, you'll see you a drop-down menu, and you can spend from any of your asset types.

Note that these listed asset/token types remain, even if the balance of one particular asset/token is zero. So every time someone receives some Zingos, the Zingo asset/token type gets added to their wallet asset/token list, indelibly so for now. After a few of these, if you don't really want them, they act as spam.

So, please don't send your new assets/tokens to someone without asking them first if they want them. :)

## Directory
There is an [asset/token directory](https://byteball.market/#!/assets) where you can view details of the new assets/tokens.

# ICO bot
[TitanCoin](https://byteroll.com/chatbot#titancoin_ico) is the first use of our ICO bot which was published on github just recently https://github.com/byteball/ico-bot. The bot is designed to help entrepreneurs raise funds and distribute their tokens, easily and securely. The current version (2018-01-07) now accepts BTC and ETH in addition to Bytes. The tokens are issued immediately after the payment is confirmed.

# The ICO craze
[video](https://www.youtube.com/watch?v=yfjgcI8xX3A){.youtube}

This must-watch 9-minute video from Andreas Antonopoulos explains how ICOs will disrupt financial technology all across the spectrum; how almost all ICOs currently are bad; how regulators will try but won't be able to keep with new innovations and their rules will be unenforceable anyway; how society has to wade through hundreds of losing imvestments in order to finally learn what is a good ICO investment.

# Regulation
Be aware that fundraising via new assets/tokens on cryptocurrency platforms, sometimes issued as ICOs (Initial Coin Offerings), are increasingly being regulated in various nations. For a general overview of this by country, see https://en.wikipedia.org/wiki/Initial_coin_offering#Regulation.

## SEC
On July 25, 2017, the US SEC (Securities and Exchange Commission) [issued a report](https://www.sec.gov/news/press-release/2017-131) stating that offers and sales of digital assets by "virtual" organizations are subject to the requirements of the federal securities laws. The key points from this are that:

1) Digital assets can be securities  
2) That determination will depend on the facts and circumstances of each case (see Howey)  
3) Digital assets that are deemed securities are subject to U.S. securities laws.

The third point is extremely broad because securities laws are very extensive.

### Howey Test

Excerpted from [Findlaw.com "What is the Howey Test?](http://consumer.findlaw.com/securities-law/what-is-the-howey-test.html):

> Whether a particular investment transaction involves the offer or sale of a security – regardless of the terminology or technology used – will depend on the facts and circumstances, including the economic realities of the transaction.
> 
> Under the Howey Test, a transaction is an investment contract (and thus subject to securities registration requirements) if:  
> 1.	It is an investment of money  
> 2.	There is an expectation of profits from the investment  
> 3.	The investment of money is in a common enterprise  
> 4.	Any profit comes from the efforts of a promoter or third party

Refer to the linked article for further explanation of these four points. It might be a good idea to not use the word "investment" in describing your asset/token.

### Jurisdiction
Does this apply to *your* token/asset? Depends. The US lately has been assuming a "World Police" function, whether sanctioned by international law or not. If in doubt, consult a competent lawyer.
	
## China
On September 4, 2017, the People's Bank of China [declared](http://www.pbc.gov.cn/goutongjiaoliu/113456/113469/3374222/index.html) all ICOs (Initial Coin Offerings) illegal. [A report](https://www.cointelegraph.com/news/china-ban-on-ico-is-temporary-licensing-to-be-introduced-official) from September 10 said this may be "only temporary, until local financial regulators introduce necessary regulatory frameworks and policies for both ICO investors and projects." How this applies to any new asset issued on the Byteball platform is not clear.

# Bitcointalk copypasta
Ref [https://bitcointalk.org](https://bitcointalk.org/index.php?topic=1608859.0)

5/29 from tonych:
> Developer guide for issuing new assets on Byteball:  
> https://github.com/byteball/byteballcore/wiki/Issuing-assets-on-Byteball

5/30 response:  
> It's the same purpose as ethereum tokens, except the feature is built natively in Byteball and you don't need to write a smart-contract to manage the assets you emitted. 

5/30 from tonych (post #8739):

> [Examples] ICOs, shares, bonds, fiat-pegged coins, loyalty points, minutes of airtime, assets in online games, whatever you can imagine.

> [How traded and where] They can be immediately traded P2P via smart contracts (same as blackbytes).
Also we have a trustless exchange already prepared https://github.com/byteball/byteball-exchange but not started yet.


-----
**This "Asset" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
