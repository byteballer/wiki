<!-- TITLE: Trading -->
<!-- SUBTITLE: Buying GBytes on exchanges; exchanging GB and GBB or bytes and blackbytes; betting on external events using oracles; and more. -->
# Betting on external events
## Prediction markets
The Byteball platform currently supports:  
* [Sports-betting](https://byteroll.com/trading/sports-betting) on upcoming soccer matches  
* Gambling on random numbers  
* Betting on crypto exchange rates  
* Betting on flight delays.

See the wiki [Trading prediction-markets](https://byteroll.com/trading/trading-prediction-markets) article for details.
# Buying GB on exchanges
## External exchanges
You can buy GBytes on various external exchanges. Some allow fiat but others are BTC/altcoins only.

[Bittrex](https://bittrex.com) Crypto only; recently changed (lower) withdrawal limits. It's a good idea to check wallet status for particular cryptos here first: [Bittrex status](https://bittrex.com/Status). 

[Cryptochangex](https://cryptochangex.com) New, announced 2017-11-05, UK company. Owned by s1lverbox, well-known trader on bitcointalk and Byteball Slack. 

[Cryptox](https://cryptox.pl) Crypto only. Rather small, but generally OK. GBYTE/BCC (Bitcoin Cash) pair added Jan 2018. If needed, contact admin at admin@cryptox.pl or @grzem in the Byteball Slack. 

[Cryptopia](https://cryptopia.co.nz) Crypto only; site is more than a simple exchange

[Changelly](https://changelly.com/exchange/btc/gbyte) Accepts Visa/MC and cryptos. The exchange used by the Byteball-Altcoin Exchange Bot (see the wiki article [Chatbot](https://byteroll.com/chatbot))

[Bitsquare](https://bitsquare.io) Decentralized! National currencies and cryptos


-----

[YDX](http://www.iotaexchange.com/#details) It allows you to trade certain coins in a semi-automated way via Slack, and supports GB and even GBB.

### Exchange volume
* Bittrex 80-98%  
* Cryptox 20-2%  
* Cryptopia 0-2%  

Use [Coinmarketcap.com](https://coinmarketcap.com/currencies/byteball/#markets) for latest figures

### Exchange fees
| Exchange/Fee |  Maker       | Taker | Withdraw |
|-------------------|---------|-------|----------|
| bittrex.com       | 0.25%   | 0.25% | 2 MB     |
| cryptox.pl        | (-0.1%) | 0.20% | 2 KB     |

## *Byte-BTC Exchange*
There is an **in-wallet chatbot** that provides a book for users, but there are few usable  asks/bids. For example, at the time of writing, the exchange rate per [Coingecko](https://www.coingecko.com/en/coins/byteball) is .1292 BTC/GB. 


The bot gives the trades below. Some have been removed.

*  At 2 BTC/GB sell vol. 0.998 GB
*
*  (13 asks removed)
*  
*  At 0.15 BTC/GB sell vol. 0.474946421 GB
*  At 0.1494999 BTC/GB sell vol. 3.313512837 GB
*  At 0.1494 BTC/GB sell vol. 1.00008582 GB
*
*
*  At 0.129874 BTC/GB buy vol. 0.038421855 GB
*  At 0.12 BTC/GB buy vol. 0.959016667 GB
*  At 0.1005 BTC/GB buy vol. 0.496517413 GB
*  
*  (9 bids removed)
*  
*  At 0.00876118 BTC/GB buy vol. 23.693612048 GB

You send it your Receive address. You then pay between 0.0005 and 0.2 BTC to a bitcoin address it provides. After 2 confirmations you will receive the appropriate number of bytes.


### Change order
You can change your order with 'set price'. Enter 'help' for a list of all available commands.

### bytebtc
## Peer-to-peer Byte-BTC exchange
**This one is complex. It is hard for the buyer to verify from reading it that the contract will perform as the buyer expects: maybe the seller is dishonest and made a subtle change in the details.** 

Let's say you want to sell Gbytes to a peer who's paying in BTC. You've agreed the amounts, say .121 BTC for 1 GByte. Let's say your bitcoin receive address is 1MJ7xew1X13okNYKRu7qA3uN4hpRH1Tfpq. You and the buyer pair wallets. The buyer sends you in wallet chat his Byteball receive address, say "5GU...". 

You left-click that address, choose "Pay to this address", and in the next screen enter the amount (here 1 GB), the address "5GU...", then just below this address click "Bind the payment to a condition". This opens the next screen. 

Under THE PEER RECEIVES THIS PAYMENT IF click the little down arrow, and click "An event is posted by an oracle". This opens the next screen. 

Under ORACLE ADDRESS paste
> FOPUBEUPBC6YLIQDLKL6EW775BMV7YOH

Under DATA FEED NAME paste
> bitcoin_merkle

Under EXPECTED VALUE paste your bitcoin address and the exact amount you are expecting, in the form "address:amount":
> 1MJ7xew1X13okNYKRu7qA3uN4hpRH1Tfpq:0.121 (in this example)

Under THE EXPECTED VALUE WILL BE POSTED click the little down arrow, and click "in merkle tree".

Change the "4" hours to some other figure if you wish. Click SEND PAYMENT. This will return you to the first payment screen and allow you to double-check the amount. Finally click SEND.

The wallet chat will show that your payment (for 1GB here) has been sent. Once this payment has confirmed, after maybe 5-10 minutes, the buyer will automatically get a copy of the smart contract/wallet. The buyer can see it by exiting the wallet chat, going to the home screen, clicking on "smart wallet", and then in the smart wallet home screen clicking on the little eyeball on the right.

The buyer must read the contract carefully, to ensure that it specifies the correct bitcoin address and the correct amount. Otherwise a dishonest seller would be able to receive both the bitcoin payment (sent to a different address that had been passed over) and reclaim the bytes after 4 hours as the expected bitcoin_merkle would not get generated. Also the correct oracle address and data feed name.

After two confirmations, the buyer needs to get the "merkle proof" (just a long string of data) of this transaction. A merkle proof can be obtained (after two or more confirmations) from the BTC oracle at byteball:A7C96Bhg4Gpb2Upw/Ky/YfGG8BKe5DjTiBuJFGAX50N1@byteball.org/bb#0000.

The buyer then copies and pastes in this proof to unlock the bytes payment from the smart contract.
# Slack trading GB and BTC
Long-time Bitcointalk trader S1lverbox will buy GB for BTC at market rates, as of 5/30/2017. No Slack #trading reviews yet from regular users.
# Slack trading GB and GBB
You can't buy blackbytes at an exchange like Bittrex, but you can find sellers and buyers in the Slack Byteball #trading_blackbyte channel. How to do the trade is covered in this wiki article:

[Trading blackbytes](https://byteroll.com/trading/trading-blackbytes)

# Merchant chatbots
There are no known mainnet ones yet.


-----
**This "Trading" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)


