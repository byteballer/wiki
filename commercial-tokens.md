<!-- TITLE: Commercial Tokens -->
<!-- SUBTITLE: Like social tokens, commercial tokens on the Byteball platform are intended to be backed by real-world goods or services. These are discount/rebate tokens being honoured by businesses, rather than personal tokens being honoured by individuals. But unlike social tokens, commercial tokens in a different form are already in widespread use. -->

# Who would use commercial tokens on Byteball?
[video](https://vimeo.com/255626555){.vimeo}

The current commercial coupon market seems to be fairly mature and work quite well. Generally, it would seem like a bridge too far for Byteball to get anywhere.

However, there is one way to get a foot in the door. A man and his wife, both Byteball fans, maybe run a small beauty salon in their town. They use [family tokens](https://byteroll.com/family-tokens) with their younger kids, while their teenage son has started a thriving [social tokens](https://byteroll.com/social-tokens) community at the local high school. Being very familiar with issuing, trading and redeeming tokens, and having a captive audience while receiving beauty treatments, it is fairly easy to introduce their customers to the various Byteball token systems. Some people are interested and get free [textcoin](https://byteroll.com/textcoin) discount "coupons", some are not.

## Grass roots

It is always going to start with one Byteball fan **using** the platform with their friends and family, and not just sitting on their GBytes wishing the price would go up more.

# Coupon promotion
In marketing, a coupon is a ticket or document that can be redeemed for a financial discount or rebate when purchasing a product.

Customarily, coupons are issued by manufacturers of consumer packaged goods or by retailers, to be used in retail stores as a part of sales promotions. They are often widely distributed through mail, coupon envelopes, magazines, newspapers, the Internet (social media, email newsletter), directly from the retailer, and mobile devices such as cell phones. Since only price conscious consumers are likely to spend the time to claim the savings, coupons function as a form of price discrimination, enabling retailers to offer a lower price only to those consumers who would otherwise go elsewhere. In addition, coupons can also be targeted selectively to regional markets in which price competition is great.[[1](#references)]

# Types and uses
There are different types of values applied to coupons such as discounts, free shipping, buy-one get-one, trade-in for redemption, first-time customer coupons, free trial offer, launch offers, festival offers, and free giveaways. Similarly, there are different uses of coupons which include: to incentive a purchase, to reduce the price of a particular item or items, provide a free sample, or to help allow marketers better-understand the demographics of their customer.

# See also

# References
1. https://en.wikipedia.org/wiki/Coupon


-----
**This "Commercial tokens" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
