<!-- TITLE: Slack -->
<!-- SUBTITLE: Some of the Byteball channels at Slack.com -->

# Archive
Slack channels only keep a few weeks of posts, depending on volume. But older messages are available from  
https://byteball.slackarchive.io
# Our Slack channels
## #byteball_wiki
for matters concerning this wiki. The wiki editors/admins mostly hang out on Slack, and can be pinged easily
## #general
for general matters

### on changing the name/logo
ref https://byteball.slack.com/archives/C30NVUJAD/p1514929559000560

> **tonych** [2018-01-02 9:45 PM] 
> *So [Tony] if you want to help us out here: do you have any intention of ever changing the name or logo?*
I'm ok with the name and the logo. Maybe the logo could still be improved but the concept should stay the same: a circle.  Simplicity, minimalism, no embellishments.
> I saw suggestions to change the name to Bytes.  Bytes is already the name of the first coin.  And Byteball - the platform.

## #helpdesk
tends to be for newbies with simple questions, most of which should be able to be answered by giving a reference to a page in this wiki

## #ideas
for brainstorming new stuff. "Sticky" ideas might rate their own section or page here. . . 
### change
### Change the unit from GB to MB
You can choose the unit in your wallet as KB, MB or GB. Plus . . . 
> **tonych** [2017-05-24 10:48 PM] 
I don't think it would be a good move now.  We are still traded mostly against bitcoin and the price of MB (0.00013 BTC) with its many zeros wouldn't be easier to read than the price of GB 0.13 BTC

> **tonych** [2018-01-05 See thread started 23:39 UTC by @neo]  
Guys don't waste your energy on the units issue. @blokchain I saw your excellent post on reddit, and you had far more important points to care about than this one.  We are not trying to appeal to stupid "investors" who don't understand that the choice of unit is arbitrary.  They are easy to be fooled but we don't exploit them.  Everybody is free to use the unit of his choice, it's easy to change in the wallet, and the default is bytes, which already shows amounts as integers (to your point @Phyzy that you prefer integers).

### slogan
Lots of "ooh, I thought of a nice slogan, it's ... . " posts. [Rationale for current slogan here](https://byteroll.com/slogan).



## #marketing
for questions, announcements, ideas, co-ordination on marketing matters

## #prediction_markets
for discussion about, and finding partners for, betting, trying out smart-contracts etc
## #tech
where the developers hang out

## #trading
for Byteball trading generally, discussion about troubles with external exchanges dealing with GBytes, etc

## #trading_blackbyte
limited to trading blackbytes and related discussions. There's a bookbot that lists asks and bids, with commands that are available displayed by posting "otc"

## #zwib
for all things zwib, including getting some and trading them for other new assets