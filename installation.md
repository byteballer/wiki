<!-- TITLE: Installation -->
<!-- SUBTITLE: Beginners' guide to installing the wallet and linking to BTC addresses before next airdrop -->

# Installation videos
Installation is pretty straightforward.

[Windows](https://byteroll.com/installation#windows)

[Mac OS X](https://byteroll.com/installation#mac)

[Linux & Linking your bitcoin address to your Byteball Wallet for all](https://byteroll.com/installation#linux)

Airdrop linking - Confused?

If you are on Windows or Mac *install Electrum and Byteball* on your machine, then proceed to the "Making bitcoin addresses visible in Electrum" video

# Multisignature Wallet Generation
[All platforms](https://byteroll.com/installation#multisig)

## Windows

[Basic overview of the complete installation process](https://www.youtube.com/watch?v=bBuLgihIpNA){.youtube}

## Mac OS X
coming . . . 

## Linux

### #1 Installing Electrum

[video](https://youtu.be/ijMtwx12p1o){.youtube}


So from the [Electrum Ubuntu/Debian install](https://electrum.org/#download)
I would recommend the following line:

Note that for Ubuntu 16.04 an additional dependency appears to be necessary

```text
sudo apt-get install python-qt4 python-pip python-setuptools
```

### #2 Making bitcoin addresses visible in Electrum - Tutorial for ALL PLATFORMS

[Making Electrum bitcoin addresses visible](https://youtu.be/Cvv0YMLtcv4){.youtube}


### Part 3 Signing - Tutorial for ALL PLATFORMS


[Signing the wallet address with your bitcoin wallet](https://youtu.be/GGsw_2srqmg){.youtube}

## Multisig
[The basics of how to create a multisignature wallet](https://youtu.be/V7rQMyolFuQ){.youtube}


-----
**This "Installation" page main author**: markcross on the [byteball slack](http://slack.byteball.org/)
